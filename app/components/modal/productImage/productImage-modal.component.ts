import { Component,ViewContainerRef } from "@angular/core";
import { ModalDialogParams, ModalDialogService } from "nativescript-angular/directives/dialogs";

@Component({
    selector: "productImage-modal",
    templateUrl: "components/modal/productImage/productImage-modal.component.html",
})
export class ProductImageModalComponent {

    public constructor(
    	private params: ModalDialogParams,
    	private modal:ModalDialogService,
    	private vcRef:ViewContainerRef) {}

    public close(res: string) {
        this.params.closeCallback(res);
    }

}