import { Component } from "@angular/core";
import { AuthService } from '../../services/auth.service'
import { EmailValidator } from '@angular/forms';
import { ValidationService } from '../../services/validation.service'
import { RouterExtensions } from "nativescript-angular/router";
import * as app from 'application';
import { isAndroid } from 'platform';

@Component({
    selector: 'login-component',
    templateUrl: 'components/login/login.component.html',
})

export class LoginComponent {
    email = ''
    password = ''
    error_email = ''
    error_password = ''
    error_login = ''

    constructor(private authService: AuthService, private routerExtensions: RouterExtensions, ) {
        
    }

    loginWithEmailAndPassword(args) {
        if (this.validateLoginCredentials()) {
            this.authService.loginWithEmailAndPassword(this.email, this.password,
                () => {
                    /* 
                        Hide keyboard when user is succesfully loged
                        https://bradmartin.net/2016/10/21/dismiss-the-android-softkeyboard-programmatically-in-a-nativescript-app/
                    */
                    this.hideKeyboard() //
                    this.routerExtensions.navigate(["/account"]);
                },
                error => {
                    this.error_login = error
                }
            )
        }
    }

    validateLoginCredentials() {
        //Reset errors
        this.error_email = this.error_password = this.error_login = ''

        if (!ValidationService.isValidEmail(this.email)) {
            this.error_email = 'Invalid email'
        }

        if (!ValidationService.isValidPassword(this.password)) {
            this.error_password = 'Password must be atleast 6 characters long'
        }

        return !this.error_email && !this.error_password
    }

    private hideKeyboard() {
        if (isAndroid) {
            try {
                let activity = app.android.foregroundActivity;
                let Context = app.android.currentContext;
                let inputManager = Context.getSystemService(android.content.Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), android.view.inputmethod.InputMethodManager.HIDE_NOT_ALWAYS);
            } catch (err) {
                console.log(err);
            }
        }
    }
}

