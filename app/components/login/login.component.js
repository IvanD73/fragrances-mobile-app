"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_service_1 = require("../../services/auth.service");
var validation_service_1 = require("../../services/validation.service");
var router_1 = require("nativescript-angular/router");
var app = require("application");
var platform_1 = require("platform");
var LoginComponent = (function () {
    function LoginComponent(authService, routerExtensions) {
        this.authService = authService;
        this.routerExtensions = routerExtensions;
        this.email = '';
        this.password = '';
        this.error_email = '';
        this.error_password = '';
        this.error_login = '';
    }
    LoginComponent.prototype.loginWithEmailAndPassword = function (args) {
        var _this = this;
        if (this.validateLoginCredentials()) {
            this.authService.loginWithEmailAndPassword(this.email, this.password, function () {
                /*
                    Hide keyboard when user is succesfully loged
                    https://bradmartin.net/2016/10/21/dismiss-the-android-softkeyboard-programmatically-in-a-nativescript-app/
                */
                _this.hideKeyboard(); //
                _this.routerExtensions.navigate(["/account"]);
            }, function (error) {
                _this.error_login = error;
            });
        }
    };
    LoginComponent.prototype.validateLoginCredentials = function () {
        //Reset errors
        this.error_email = this.error_password = this.error_login = '';
        if (!validation_service_1.ValidationService.isValidEmail(this.email)) {
            this.error_email = 'Invalid email';
        }
        if (!validation_service_1.ValidationService.isValidPassword(this.password)) {
            this.error_password = 'Password must be atleast 6 characters long';
        }
        return !this.error_email && !this.error_password;
    };
    LoginComponent.prototype.hideKeyboard = function () {
        if (platform_1.isAndroid) {
            try {
                var activity = app.android.foregroundActivity;
                var Context = app.android.currentContext;
                var inputManager = Context.getSystemService(android.content.Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), android.view.inputmethod.InputMethodManager.HIDE_NOT_ALWAYS);
            }
            catch (err) {
                console.log(err);
            }
        }
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login-component',
            templateUrl: 'components/login/login.component.html',
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService, router_1.RouterExtensions])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTBDO0FBQzFDLDREQUF5RDtBQUV6RCx3RUFBcUU7QUFDckUsc0RBQStEO0FBQy9ELGlDQUFtQztBQUNuQyxxQ0FBcUM7QUFPckM7SUFPSSx3QkFBb0IsV0FBd0IsRUFBVSxnQkFBa0M7UUFBcEUsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBTnhGLFVBQUssR0FBRyxFQUFFLENBQUE7UUFDVixhQUFRLEdBQUcsRUFBRSxDQUFBO1FBQ2IsZ0JBQVcsR0FBRyxFQUFFLENBQUE7UUFDaEIsbUJBQWMsR0FBRyxFQUFFLENBQUE7UUFDbkIsZ0JBQVcsR0FBRyxFQUFFLENBQUE7SUFJaEIsQ0FBQztJQUVELGtEQUF5QixHQUF6QixVQUEwQixJQUFJO1FBQTlCLGlCQWdCQztRQWZHLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFDaEU7Z0JBQ0k7OztrQkFHRTtnQkFDRixLQUFJLENBQUMsWUFBWSxFQUFFLENBQUEsQ0FBQyxFQUFFO2dCQUN0QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNqRCxDQUFDLEVBQ0QsVUFBQSxLQUFLO2dCQUNELEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFBO1lBQzVCLENBQUMsQ0FDSixDQUFBO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCxpREFBd0IsR0FBeEI7UUFDSSxjQUFjO1FBQ2QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFBO1FBRTlELEVBQUUsQ0FBQyxDQUFDLENBQUMsc0NBQWlCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxlQUFlLENBQUE7UUFDdEMsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLENBQUMsc0NBQWlCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLGNBQWMsR0FBRyw0Q0FBNEMsQ0FBQTtRQUN0RSxDQUFDO1FBRUQsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUE7SUFDcEQsQ0FBQztJQUVPLHFDQUFZLEdBQXBCO1FBQ0ksRUFBRSxDQUFDLENBQUMsb0JBQVMsQ0FBQyxDQUFDLENBQUM7WUFDWixJQUFJLENBQUM7Z0JBQ0QsSUFBSSxRQUFRLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQztnQkFDOUMsSUFBSSxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUM7Z0JBQ3pDLElBQUksWUFBWSxHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2dCQUMxRixZQUFZLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFDLGNBQWMsRUFBRSxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ25KLENBQUM7WUFBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDckIsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBdkRRLGNBQWM7UUFMMUIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsV0FBVyxFQUFFLHVDQUF1QztTQUN2RCxDQUFDO3lDQVNtQywwQkFBVyxFQUE0Qix5QkFBZ0I7T0FQL0UsY0FBYyxDQXdEMUI7SUFBRCxxQkFBQztDQUFBLEFBeERELElBd0RDO0FBeERZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnXHJcbmltcG9ydCB7IEVtYWlsVmFsaWRhdG9yIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBWYWxpZGF0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3ZhbGlkYXRpb24uc2VydmljZSdcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0ICogYXMgYXBwIGZyb20gJ2FwcGxpY2F0aW9uJztcclxuaW1wb3J0IHsgaXNBbmRyb2lkIH0gZnJvbSAncGxhdGZvcm0nO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2xvZ2luLWNvbXBvbmVudCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50Lmh0bWwnLFxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIExvZ2luQ29tcG9uZW50IHtcclxuICAgIGVtYWlsID0gJydcclxuICAgIHBhc3N3b3JkID0gJydcclxuICAgIGVycm9yX2VtYWlsID0gJydcclxuICAgIGVycm9yX3Bhc3N3b3JkID0gJydcclxuICAgIGVycm9yX2xvZ2luID0gJydcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCApIHtcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBsb2dpbldpdGhFbWFpbEFuZFBhc3N3b3JkKGFyZ3MpIHtcclxuICAgICAgICBpZiAodGhpcy52YWxpZGF0ZUxvZ2luQ3JlZGVudGlhbHMoKSkge1xyXG4gICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLmxvZ2luV2l0aEVtYWlsQW5kUGFzc3dvcmQodGhpcy5lbWFpbCwgdGhpcy5wYXNzd29yZCxcclxuICAgICAgICAgICAgICAgICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAvKiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgSGlkZSBrZXlib2FyZCB3aGVuIHVzZXIgaXMgc3VjY2VzZnVsbHkgbG9nZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgaHR0cHM6Ly9icmFkbWFydGluLm5ldC8yMDE2LzEwLzIxL2Rpc21pc3MtdGhlLWFuZHJvaWQtc29mdGtleWJvYXJkLXByb2dyYW1tYXRpY2FsbHktaW4tYS1uYXRpdmVzY3JpcHQtYXBwL1xyXG4gICAgICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5oaWRlS2V5Ym9hcmQoKSAvL1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvYWNjb3VudFwiXSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZXJyb3JfbG9naW4gPSBlcnJvclxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlTG9naW5DcmVkZW50aWFscygpIHtcclxuICAgICAgICAvL1Jlc2V0IGVycm9yc1xyXG4gICAgICAgIHRoaXMuZXJyb3JfZW1haWwgPSB0aGlzLmVycm9yX3Bhc3N3b3JkID0gdGhpcy5lcnJvcl9sb2dpbiA9ICcnXHJcblxyXG4gICAgICAgIGlmICghVmFsaWRhdGlvblNlcnZpY2UuaXNWYWxpZEVtYWlsKHRoaXMuZW1haWwpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JfZW1haWwgPSAnSW52YWxpZCBlbWFpbCdcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghVmFsaWRhdGlvblNlcnZpY2UuaXNWYWxpZFBhc3N3b3JkKHRoaXMucGFzc3dvcmQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JfcGFzc3dvcmQgPSAnUGFzc3dvcmQgbXVzdCBiZSBhdGxlYXN0IDYgY2hhcmFjdGVycyBsb25nJ1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuICF0aGlzLmVycm9yX2VtYWlsICYmICF0aGlzLmVycm9yX3Bhc3N3b3JkXHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBoaWRlS2V5Ym9hcmQoKSB7XHJcbiAgICAgICAgaWYgKGlzQW5kcm9pZCkge1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgbGV0IGFjdGl2aXR5ID0gYXBwLmFuZHJvaWQuZm9yZWdyb3VuZEFjdGl2aXR5O1xyXG4gICAgICAgICAgICAgICAgbGV0IENvbnRleHQgPSBhcHAuYW5kcm9pZC5jdXJyZW50Q29udGV4dDtcclxuICAgICAgICAgICAgICAgIGxldCBpbnB1dE1hbmFnZXIgPSBDb250ZXh0LmdldFN5c3RlbVNlcnZpY2UoYW5kcm9pZC5jb250ZW50LkNvbnRleHQuSU5QVVRfTUVUSE9EX1NFUlZJQ0UpO1xyXG4gICAgICAgICAgICAgICAgaW5wdXRNYW5hZ2VyLmhpZGVTb2Z0SW5wdXRGcm9tV2luZG93KGFjdGl2aXR5LmdldEN1cnJlbnRGb2N1cygpLmdldFdpbmRvd1Rva2VuKCksIGFuZHJvaWQudmlldy5pbnB1dG1ldGhvZC5JbnB1dE1ldGhvZE1hbmFnZXIuSElERV9OT1RfQUxXQVlTKTtcclxuICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4iXX0=