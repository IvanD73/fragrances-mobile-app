import {Component} from "@angular/core";
import {AuthService} from '../../services/auth.service'

@Component({
    selector: 'drawer-content',
    templateUrl: 'components/drawer/drawer.component.html',
})
export class DrawerComponent {
    isLogged = false
    constructor(private authService: AuthService) {

    }

    ngOnInit(){
        this.isLogged = this.authService.isLogged()
    }
}
