"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_component_1 = require("./app.component");
var router_1 = require("nativescript-angular/router");
var nativescript_ngx_fonticon_1 = require("nativescript-ngx-fonticon");
var nativescript_ngx_slides_1 = require("nativescript-ngx-slides");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var angular_1 = require("nativescript-checkbox/angular");
var angular_2 = require("nativescript-radiobutton/angular");
var http_1 = require("nativescript-angular/http");
//Routes
var app_routes_1 = require("./app.routes");
//Services
var products_services_1 = require("./services/products.services");
var filter_service_1 = require("./services/filter.service");
var auth_service_1 = require("./services/auth.service");
var auth_guard_service_1 = require("./services/auth-guard.service");
var validation_service_1 = require("./services/validation.service");
var account_service_1 = require("./services/account.service");
//Components
var drawer_component_1 = require("./components/drawer/drawer.component");
var search_modal_component_1 = require("./components/modal/search/search-modal.component");
var filter_modal_component_1 = require("./components/modal/filter/filter-modal.component");
var address_modal_component_1 = require("./components/modal/address/address-modal.component");
var productImage_modal_component_1 = require("./components/modal/productImage/productImage-modal.component");
var login_component_1 = require("./components/login/login.component");
var logout_component_1 = require("./components/logout/logout.component");
var register_component_1 = require("./components/register/register.component");
var slider_component_1 = require("./pages/home/slider/slider.component");
var boxes_component_1 = require("./pages/home/boxes/boxes.component");
//Pages
var home_page_1 = require("./pages/home/home.page");
var account_dashboard_page_1 = require("./pages/account/dashboard/account-dashboard.page");
var account_edit_page_1 = require("./pages/account/edit/account-edit.page");
var account_addresses_page_1 = require("./pages/account/addresses/account-addresses.page");
var account_orders_page_1 = require("./pages/account/orders/account-orders.page");
var settings_page_1 = require("./pages/settings/settings.page");
var checkout_page_1 = require("./pages/checkout/checkout.page");
var categories_page_1 = require("./pages/categories/categories.page");
var categoryList_component_1 = require("./pages/categories/category-list/categoryList.component");
var subcategoryList_component_1 = require("./pages/categories/subcategory-list/subcategoryList.component");
var category_products_component_1 = require("./pages/categories/category-products/category-products.component");
var product_page_1 = require("./pages/product/product.page");
var angular_3 = require("nativescript-pro-ui/sidedrawer/angular");
var nativescript_angular_1 = require("nativescript-angular");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [app_component_1.AppComponent],
            declarations: [
                app_component_1.AppComponent,
                home_page_1.HomePage,
                slider_component_1.SliderComponent,
                boxes_component_1.BoxesComponent,
                account_dashboard_page_1.AccountDashboardPage,
                account_edit_page_1.AccountEditPage,
                account_addresses_page_1.AccountAddressesPage,
                account_orders_page_1.AccountOrdersPage,
                login_component_1.LoginComponent,
                logout_component_1.LogoutComponent,
                register_component_1.RegisterComponent,
                settings_page_1.SettingsPage,
                checkout_page_1.CheckoutPage,
                categories_page_1.CategoriesPage,
                categoryList_component_1.CategoryList,
                subcategoryList_component_1.SubcategoryList,
                category_products_component_1.CategoryProducts,
                product_page_1.ProductPage,
                drawer_component_1.DrawerComponent,
                angular_3.SIDEDRAWER_DIRECTIVES,
                search_modal_component_1.SearchModalComponent,
                filter_modal_component_1.FilterModalComponent,
                address_modal_component_1.AddressModalComponent,
                productImage_modal_component_1.ProductImageModalComponent,
            ],
            entryComponents: [
                search_modal_component_1.SearchModalComponent,
                filter_modal_component_1.FilterModalComponent,
                address_modal_component_1.AddressModalComponent,
                productImage_modal_component_1.ProductImageModalComponent,
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                router_1.NativeScriptRouterModule,
                router_1.NativeScriptRouterModule.forRoot(app_routes_1.routes),
                nativescript_angular_1.NativeScriptFormsModule,
                nativescript_ngx_fonticon_1.TNSFontIconModule.forRoot({
                    'fa': './assets/font-awesome.css',
                    'ion': './assets/ionicons.css'
                }),
                nativescript_ngx_slides_1.SlidesModule,
                angular_1.TNSCheckBoxModule,
                angular_2.RadioButtonModule,
                http_1.NativeScriptHttpModule
            ],
            providers: [
                products_services_1.ProductsServices,
                modal_dialog_1.ModalDialogService,
                auth_service_1.AuthService,
                auth_guard_service_1.AuthGuardService,
                validation_service_1.ValidationService,
account_addresses_service_1.AccountAddressesService,
                filter_service_1.FilterService
            ],
            schemas: [core_1.NO_ERRORS_SCHEMA],
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsZ0ZBQThFO0FBQzlFLGlEQUErQztBQUMvQyxzREFBdUU7QUFDdkUsdUVBQWtGO0FBQ2xGLG1FQUF1RDtBQUN2RCxrRUFBdUU7QUFDdkUseURBQWtFO0FBQ2xFLDREQUFvRTtBQUNwRSxrREFBbUU7QUFFbkUsUUFBUTtBQUNSLDJDQUFvQztBQUVwQyxVQUFVO0FBQ1Ysa0VBQWdFO0FBQ2hFLDREQUEwRDtBQUMxRCx3REFBcUQ7QUFDckQsb0VBQWdFO0FBQ2hFLG9FQUFpRTtBQUNqRSxrRkFBOEU7QUFFOUUsWUFBWTtBQUNaLHlFQUFzRTtBQUN0RSwyRkFBeUY7QUFDekYsMkZBQXdGO0FBQ3hGLDhGQUEyRjtBQUMzRiw2R0FBMEc7QUFDMUcsc0VBQW9FO0FBQ3BFLHlFQUFzRTtBQUN0RSwrRUFBNkU7QUFFN0UsT0FBTztBQUNQLG9EQUFrRDtBQUNsRCwyRkFBd0Y7QUFDeEYsNEVBQXdFO0FBQ3hFLDJGQUF1RjtBQUN2RixrRkFBNkU7QUFDN0UsZ0VBQThEO0FBQzlELGdFQUE4RDtBQUU5RCxzRUFBb0U7QUFDcEUsa0dBQXVGO0FBQ3ZGLDJHQUFnRztBQUNoRyxnSEFBb0c7QUFFcEcsNkRBQTJEO0FBQzNELGtFQUErRTtBQUMvRSw2REFBK0Q7QUE0RC9EO0lBQUE7SUFBd0IsQ0FBQztJQUFaLFNBQVM7UUExRHJCLGVBQVEsQ0FBQztZQUNSLFNBQVMsRUFBRSxDQUFDLDRCQUFZLENBQUM7WUFDekIsWUFBWSxFQUFFO2dCQUNaLDRCQUFZO2dCQUNaLG9CQUFRO2dCQUNSLDZDQUFvQjtnQkFDcEIsbUNBQWU7Z0JBQ2YsNkNBQW9CO2dCQUNwQix1Q0FBaUI7Z0JBQ2pCLGdDQUFjO2dCQUNkLGtDQUFlO2dCQUNmLHNDQUFpQjtnQkFDakIsNEJBQVk7Z0JBQ1osNEJBQVk7Z0JBQ1osZ0NBQWM7Z0JBQ2QscUNBQVk7Z0JBQ1osMkNBQWU7Z0JBQ2YsOENBQWdCO2dCQUNoQiwwQkFBVztnQkFDWCxrQ0FBZTtnQkFDZiwrQkFBcUI7Z0JBQ3JCLDZDQUFvQjtnQkFDcEIsNkNBQW9CO2dCQUNwQiwrQ0FBcUI7Z0JBQ3JCLHlEQUEwQjthQUMzQjtZQUNELGVBQWUsRUFBRTtnQkFDYiw2Q0FBb0I7Z0JBQ3BCLDZDQUFvQjtnQkFDcEIsK0NBQXFCO2dCQUNyQix5REFBMEI7YUFDN0I7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsd0NBQWtCO2dCQUNsQixpQ0FBd0I7Z0JBQ3hCLGlDQUF3QixDQUFDLE9BQU8sQ0FBQyxtQkFBTSxDQUFDO2dCQUN4Qyw4Q0FBdUI7Z0JBQ3ZCLDZDQUFpQixDQUFDLE9BQU8sQ0FBQztvQkFDeEIsSUFBSSxFQUFFLDJCQUEyQjtvQkFDakMsS0FBSyxFQUFFLHVCQUF1QjtpQkFDL0IsQ0FBQztnQkFDRixzQ0FBWTtnQkFDWiwyQkFBaUI7Z0JBQ2pCLDJCQUFpQjtnQkFDakIsNkJBQXNCO2FBQ3pCO1lBQ0QsU0FBUyxFQUFFO2dCQUNQLG9DQUFnQjtnQkFDaEIsaUNBQWtCO2dCQUNsQiwwQkFBVztnQkFDWCxxQ0FBZ0I7Z0JBQ2hCLHNDQUFpQjtnQkFDakIsbURBQXVCO2dCQUN2Qiw4QkFBYTthQUNoQjtZQUNELE9BQU8sRUFBRSxDQUFDLHVCQUFnQixDQUFDO1NBQzVCLENBQUM7T0FFVyxTQUFTLENBQUc7SUFBRCxnQkFBQztDQUFBLEFBQXpCLElBQXlCO0FBQVosOEJBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9uYXRpdmVzY3JpcHQubW9kdWxlXCI7XHJcbmltcG9ydCB7IEFwcENvbXBvbmVudCB9IGZyb20gXCIuL2FwcC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBUTlNGb250SWNvbk1vZHVsZSwgVE5TRm9udEljb25TZXJ2aWNlIH0gZnJvbSAnbmF0aXZlc2NyaXB0LW5neC1mb250aWNvbic7XHJcbmltcG9ydCB7IFNsaWRlc01vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1uZ3gtc2xpZGVzJztcclxuaW1wb3J0IHsgTW9kYWxEaWFsb2dTZXJ2aWNlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL21vZGFsLWRpYWxvZ1wiO1xyXG5pbXBvcnQgeyBUTlNDaGVja0JveE1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1jaGVja2JveC9hbmd1bGFyJztcclxuaW1wb3J0IHsgUmFkaW9CdXR0b25Nb2R1bGUgfSBmcm9tICduYXRpdmVzY3JpcHQtcmFkaW9idXR0b24vYW5ndWxhcidcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0SHR0cE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9odHRwXCI7XHJcblxyXG4vL1JvdXRlc1xyXG5pbXBvcnQge3JvdXRlc30gZnJvbSAnLi9hcHAucm91dGVzJztcclxuXHJcbi8vU2VydmljZXNcclxuaW1wb3J0IHsgUHJvZHVjdHNTZXJ2aWNlcyB9IGZyb20gJy4vc2VydmljZXMvcHJvZHVjdHMuc2VydmljZXMnO1xyXG5pbXBvcnQgeyBGaWx0ZXJTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9maWx0ZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnXHJcbmltcG9ydCB7IEF1dGhHdWFyZFNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2F1dGgtZ3VhcmQuc2VydmljZSdcclxuaW1wb3J0IHsgVmFsaWRhdGlvblNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL3ZhbGlkYXRpb24uc2VydmljZSdcclxuaW1wb3J0IHsgQWNjb3VudEFkZHJlc3Nlc1NlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2FjY291bnQtYWRkcmVzc2VzLnNlcnZpY2UnXHJcblxyXG4vL0NvbXBvbmVudHNcclxuaW1wb3J0IHsgRHJhd2VyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2RyYXdlci9kcmF3ZXIuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBTZWFyY2hNb2RhbENvbXBvbmVudCB9ICBmcm9tICcuL2NvbXBvbmVudHMvbW9kYWwvc2VhcmNoL3NlYXJjaC1tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGaWx0ZXJNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9tb2RhbC9maWx0ZXIvZmlsdGVyLW1vZGFsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEFkZHJlc3NNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9tb2RhbC9hZGRyZXNzL2FkZHJlc3MtbW9kYWwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUHJvZHVjdEltYWdlTW9kYWxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbW9kYWwvcHJvZHVjdEltYWdlL3Byb2R1Y3RJbWFnZS1tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMb2dpbkNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBMb2dvdXRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbG9nb3V0L2xvZ291dC5jb21wb25lbnQnXHJcbmltcG9ydCB7IFJlZ2lzdGVyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudCc7XHJcblxyXG4vL1BhZ2VzXHJcbmltcG9ydCB7IEhvbWVQYWdlIH0gZnJvbSBcIi4vcGFnZXMvaG9tZS9ob21lLnBhZ2VcIjtcclxuaW1wb3J0IHsgQWNjb3VudERhc2hib2FyZFBhZ2UgfSBmcm9tICcuL3BhZ2VzL2FjY291bnQvZGFzaGJvYXJkL2FjY291bnQtZGFzaGJvYXJkLnBhZ2UnO1xyXG5pbXBvcnQgeyBBY2NvdW50RWRpdFBhZ2UgfSBmcm9tICcuL3BhZ2VzL2FjY291bnQvZWRpdC9hY2NvdW50LWVkaXQucGFnZSdcclxuaW1wb3J0IHsgQWNjb3VudEFkZHJlc3Nlc1BhZ2UgfSBmcm9tICcuL3BhZ2VzL2FjY291bnQvYWRkcmVzc2VzL2FjY291bnQtYWRkcmVzc2VzLnBhZ2UnXHJcbmltcG9ydCB7IEFjY291bnRPcmRlcnNQYWdlfSBmcm9tICcuL3BhZ2VzL2FjY291bnQvb3JkZXJzL2FjY291bnQtb3JkZXJzLnBhZ2UnXHJcbmltcG9ydCB7IFNldHRpbmdzUGFnZSB9IGZyb20gXCIuL3BhZ2VzL3NldHRpbmdzL3NldHRpbmdzLnBhZ2VcIjtcclxuaW1wb3J0IHsgQ2hlY2tvdXRQYWdlIH0gZnJvbSAnLi9wYWdlcy9jaGVja291dC9jaGVja291dC5wYWdlJztcclxuXHJcbmltcG9ydCB7IENhdGVnb3JpZXNQYWdlIH0gZnJvbSAnLi9wYWdlcy9jYXRlZ29yaWVzL2NhdGVnb3JpZXMucGFnZSc7XHJcbmltcG9ydCB7IENhdGVnb3J5TGlzdCB9IGZyb20gJy4vcGFnZXMvY2F0ZWdvcmllcy9jYXRlZ29yeS1saXN0L2NhdGVnb3J5TGlzdC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTdWJjYXRlZ29yeUxpc3QgfSBmcm9tICcuL3BhZ2VzL2NhdGVnb3JpZXMvc3ViY2F0ZWdvcnktbGlzdC9zdWJjYXRlZ29yeUxpc3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2F0ZWdvcnlQcm9kdWN0cyB9IGZyb20gJy4vcGFnZXMvY2F0ZWdvcmllcy9jYXRlZ29yeS1wcm9kdWN0cy9jYXRlZ29yeS1wcm9kdWN0cy5jb21wb25lbnQnO1xyXG5cclxuaW1wb3J0IHsgUHJvZHVjdFBhZ2UgfSBmcm9tICcuL3BhZ2VzL3Byb2R1Y3QvcHJvZHVjdC5wYWdlJztcclxuaW1wb3J0IHsgU0lERURSQVdFUl9ESVJFQ1RJVkVTIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1wcm8tdWkvc2lkZWRyYXdlci9hbmd1bGFyXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyXCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGJvb3RzdHJhcDogW0FwcENvbXBvbmVudF0sXHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICBBcHBDb21wb25lbnQsXHJcbiAgICBIb21lUGFnZSxcclxuICAgIEFjY291bnREYXNoYm9hcmRQYWdlLFxyXG4gICAgQWNjb3VudEVkaXRQYWdlLFxyXG4gICAgQWNjb3VudEFkZHJlc3Nlc1BhZ2UsXHJcbiAgICBBY2NvdW50T3JkZXJzUGFnZSxcclxuICAgIExvZ2luQ29tcG9uZW50LFxyXG4gICAgTG9nb3V0Q29tcG9uZW50LFxyXG4gICAgUmVnaXN0ZXJDb21wb25lbnQsXHJcbiAgICBTZXR0aW5nc1BhZ2UsXHJcbiAgICBDaGVja291dFBhZ2UsXHJcbiAgICBDYXRlZ29yaWVzUGFnZSxcclxuICAgIENhdGVnb3J5TGlzdCxcclxuICAgIFN1YmNhdGVnb3J5TGlzdCxcclxuICAgIENhdGVnb3J5UHJvZHVjdHMsXHJcbiAgICBQcm9kdWN0UGFnZSxcclxuICAgIERyYXdlckNvbXBvbmVudCxcclxuICAgIFNJREVEUkFXRVJfRElSRUNUSVZFUyxcclxuICAgIFNlYXJjaE1vZGFsQ29tcG9uZW50LFxyXG4gICAgRmlsdGVyTW9kYWxDb21wb25lbnQsXHJcbiAgICBBZGRyZXNzTW9kYWxDb21wb25lbnQsXHJcbiAgICBQcm9kdWN0SW1hZ2VNb2RhbENvbXBvbmVudCxcclxuICBdLFxyXG4gIGVudHJ5Q29tcG9uZW50czogW1xyXG4gICAgICBTZWFyY2hNb2RhbENvbXBvbmVudCxcclxuICAgICAgRmlsdGVyTW9kYWxDb21wb25lbnQsXHJcbiAgICAgIEFkZHJlc3NNb2RhbENvbXBvbmVudCxcclxuICAgICAgUHJvZHVjdEltYWdlTW9kYWxDb21wb25lbnQsXHJcbiAgXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSAsXHJcbiAgICAgIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSxcclxuICAgICAgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvclJvb3Qocm91dGVzKSxcclxuICAgICAgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUsICBcclxuICAgICAgVE5TRm9udEljb25Nb2R1bGUuZm9yUm9vdCh7XHJcbiAgICAgICAgJ2ZhJzogJy4vYXNzZXRzL2ZvbnQtYXdlc29tZS5jc3MnLFxyXG4gICAgICAgICdpb24nOiAnLi9hc3NldHMvaW9uaWNvbnMuY3NzJ1xyXG4gICAgICB9KSxcclxuICAgICAgU2xpZGVzTW9kdWxlLFxyXG4gICAgICBUTlNDaGVja0JveE1vZHVsZSxcclxuICAgICAgUmFkaW9CdXR0b25Nb2R1bGUsXHJcbiAgICAgIE5hdGl2ZVNjcmlwdEh0dHBNb2R1bGVcclxuICBdLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAgICBQcm9kdWN0c1NlcnZpY2VzLFxyXG4gICAgICBNb2RhbERpYWxvZ1NlcnZpY2UsXHJcbiAgICAgIEF1dGhTZXJ2aWNlLFxyXG4gICAgICBBdXRoR3VhcmRTZXJ2aWNlLFxyXG4gICAgICBWYWxpZGF0aW9uU2VydmljZSxcclxuICAgICAgQWNjb3VudEFkZHJlc3Nlc1NlcnZpY2UsXHJcbiAgICAgIEZpbHRlclNlcnZpY2VcclxuICBdLFxyXG4gIHNjaGVtYXM6IFtOT19FUlJPUlNfU0NIRU1BXSxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBcHBNb2R1bGUge31cclxuIl19

