import { HomePage } from './pages/home/home.page';
import { AccountDashboardPage } from './pages/account/dashboard/account-dashboard.page';
import { AccountEditPage } from './pages/account/edit/account-edit.page'
import { AccountAddressesPage } from './pages/account/addresses/account-addresses.page'
import { AccountOrdersPage} from './pages/account/orders/account-orders.page'
import { SettingsPage } from './pages/settings/settings.page';
import { CheckoutPage } from './pages/checkout/checkout.page';
import { CategoriesPage } from './pages/categories/categories.page';
import { SubcategoryList } from './pages/categories/subcategory-list/subcategoryList.component';
import { CategoryProducts } from './pages/categories/category-products/category-products.component';
import { ProductPage } from './pages/product/product.page';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component'
import { RegisterComponent } from './components/register/register.component';
import { AuthGuardService } from './services/auth-guard.service'


export const routes = [
    { path: "", redirectTo: "home", pathMatch: "full" },
    { path: "home", component: HomePage },
    // { path: 'account', component: AccountPage, canActivate: [AuthGuardService]},
    {
        path: 'account', canActivateChild: [AuthGuardService], children: [
            {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
            {path: 'dashboard', component: AccountDashboardPage},
            {path: 'edit', component: AccountEditPage},
            {path: 'addresses', component: AccountAddressesPage},
            {path: 'orders', component: AccountOrdersPage},
        ]
    },
    { path: 'logout', component: LogoutComponent },
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "settings", component: SettingsPage },
    { path: "checkout", component: CheckoutPage },
    { path: "categories", component: CategoriesPage },
    { path: "category/subcategories/:id", component: SubcategoryList, pathMatch: "full" },
    { path: "category/:id", component: CategoryProducts, pathMatch: "full" },
    { path: "product/:id", component: ProductPage }
];