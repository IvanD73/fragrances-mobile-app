import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions  } from "@angular/http";
import { Observable as RxObservable } from "rxjs/Observable";
import {URLSearchParams} from '@angular/http'

import "rxjs/add/operator/map";
import "rxjs/add/operator/do";

export class BaseService{
    protected serverUrl = "https://fragrances.bg/index.php?route=mobileapi";
    
    constructor(private http: Http){
        
    }

    protected executeGetRequest( endpoint, data:any = {}){
        let headers = this.createRequestHeader();
        
        let url = '';
        if(!this.isEmptyObject(data)){
            let params = this.objToParams(data)

            if(this.serverUrl.indexOf('?') !== -1){
                url = this.serverUrl + endpoint + '&' + params
            }else{
                url = this.serverUrl + endpoint + '?' + params
            }
        }else{
            url = this.serverUrl + endpoint
        }
        
        return this.http.get(url, { headers: headers })
            .map(res => res.json());
    }

    protected objToParams(data){
        return JSON.stringify(data)
        /*
        let params = new URLSearchParams();
        for(let key in data){
            params.set(key, data[key]) 
        }
        return params.toString()
        */
    }

    protected executePostRequest(endpoint, data:any = {}){
        let options = this.createRequestOptions()
        let url = this.serverUrl + endpoint
        let body = this.objToParams(data)
        
        return this.http.post(url, body, options)
            .map(res => res.json());
    }

    private createRequestHeader() {
        let headers = new Headers();
        // set headers here e.g.
        headers.append("AuthKey", "my-key");    
        headers.append("AuthToken", "my-token");
        headers.append('Content-Type', 'application/x-www-form-urlencoded')
        headers.append('Content-Type', 'application/json');
        return headers;
    }

    private createRequestOptions() {
        let headers = this.createRequestHeader()
        let options = new RequestOptions({ headers: headers });
        return options;
    }

    private isEmptyObject(obj) {
        for(var prop in obj) {
           if (obj.hasOwnProperty(prop)) {
              return false;
           }
        }
        return true;
    }
}