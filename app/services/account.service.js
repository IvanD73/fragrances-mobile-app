"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var base_service_1 = require("../services/base.service");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var auth_service_1 = require("../services/auth.service");
var AccountService = (function (_super) {
    __extends(AccountService, _super);
    function AccountService(http, authService) {
        var _this = _super.call(this, http) || this;
        _this.authService = authService;
        _this.token = '';
        //Set token
        _this.token = _this.authService.getToken();
        return _this;
    }
    AccountService.prototype.getAddresses = function (success, error) {
        this.executePostRequest('/account/addresses', { token: this.token })
            .subscribe(function (response) {
            if (response.error) {
                error(response.message);
            }
            else {
                success(response.addresses);
            }
        }, function (err) {
            error(err);
        });
    };
    AccountService.prototype.getOrders = function (success, error, start, limit) {
        if (start === void 0) { start = 0; }
        if (limit === void 0) { limit = 10; }
        this.executePostRequest('/account/orders', { token: this.token, start: start, limit: limit })
            .subscribe(function (response) {
            if (response.error) {
                error(response.message);
            }
            else {
                success(response.orders);
            }
        }, function (err) {
            error(err);
        });
    };
    AccountService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, auth_service_1.AuthService])
    ], AccountService);
    return AccountService;
}(base_service_1.BaseService));
exports.AccountService = AccountService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYWNjb3VudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEseURBQXNEO0FBQ3RELHNDQUEyQztBQUMzQyxzQ0FBcUM7QUFFckMseURBQXVEO0FBR3ZEO0lBQW9DLGtDQUFXO0lBRzNDLHdCQUFZLElBQVUsRUFBVSxXQUF3QjtRQUF4RCxZQUNJLGtCQUFNLElBQUksQ0FBQyxTQUlkO1FBTCtCLGlCQUFXLEdBQVgsV0FBVyxDQUFhO1FBRmhELFdBQUssR0FBRyxFQUFFLENBQUM7UUFLZixXQUFXO1FBQ1gsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFBOztJQUM1QyxDQUFDO0lBRUQscUNBQVksR0FBWixVQUFhLE9BQU8sRUFBRSxLQUFLO1FBQ3ZCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxvQkFBb0IsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDL0QsU0FBUyxDQUNOLFVBQUEsUUFBUTtZQUNKLEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNqQixLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1lBQzNCLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixPQUFPLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1lBQy9CLENBQUM7UUFDTCxDQUFDLEVBQ0QsVUFBQSxHQUFHO1lBQ0MsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ2QsQ0FBQyxDQUNKLENBQUE7SUFDVCxDQUFDO0lBRUQsa0NBQVMsR0FBVCxVQUFVLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBUyxFQUFFLEtBQVU7UUFBckIsc0JBQUEsRUFBQSxTQUFTO1FBQUUsc0JBQUEsRUFBQSxVQUFVO1FBQzNDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFHLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBQyxDQUFDO2FBQzVGLFNBQVMsQ0FDTixVQUFBLFFBQVE7WUFDSixFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDakIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQTtZQUMzQixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osT0FBTyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUM1QixDQUFDO1FBQ0wsQ0FBQyxFQUNELFVBQUEsR0FBRztZQUNDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQTtRQUNkLENBQUMsQ0FDSixDQUFBO0lBQ0wsQ0FBQztJQXhDUSxjQUFjO1FBRDFCLGlCQUFVLEVBQUU7eUNBSVMsV0FBSSxFQUF1QiwwQkFBVztPQUgvQyxjQUFjLENBeUMxQjtJQUFELHFCQUFDO0NBQUEsQUF6Q0QsQ0FBb0MsMEJBQVcsR0F5QzlDO0FBekNZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmFzZVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9iYXNlLnNlcnZpY2UnXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cCB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XHJcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uU2V0dGluZ3MgZnJvbSBcImFwcGxpY2F0aW9uLXNldHRpbmdzXCI7XHJcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFjY291bnRTZXJ2aWNlIGV4dGVuZHMgQmFzZVNlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSB0b2tlbiA9ICcnO1xyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3RvcihodHRwOiBIdHRwLCBwcml2YXRlIGF1dGhTZXJ2aWNlOiBBdXRoU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKGh0dHApXHJcblxyXG4gICAgICAgIC8vU2V0IHRva2VuXHJcbiAgICAgICAgdGhpcy50b2tlbiA9IHRoaXMuYXV0aFNlcnZpY2UuZ2V0VG9rZW4oKVxyXG4gICAgfVxyXG5cclxuICAgIGdldEFkZHJlc3NlcyhzdWNjZXNzLCBlcnJvcikge1xyXG4gICAgICAgIHRoaXMuZXhlY3V0ZVBvc3RSZXF1ZXN0KCcvYWNjb3VudC9hZGRyZXNzZXMnLCB7IHRva2VuOiB0aGlzLnRva2VuIH0pXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlLmVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yKHJlc3BvbnNlLm1lc3NhZ2UpXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzcyhyZXNwb25zZS5hZGRyZXNzZXMpXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IoZXJyKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T3JkZXJzKHN1Y2Nlc3MsIGVycm9yLCBzdGFydCA9IDAsIGxpbWl0ID0gMTApe1xyXG4gICAgICAgIHRoaXMuZXhlY3V0ZVBvc3RSZXF1ZXN0KCcvYWNjb3VudC9vcmRlcnMnLCB7IHRva2VuOiB0aGlzLnRva2VuICwgc3RhcnQ6IHN0YXJ0LCBsaW1pdDogbGltaXR9KVxyXG4gICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5lcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yKHJlc3BvbnNlLm1lc3NhZ2UpXHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2Uub3JkZXJzKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgZXJyb3IoZXJyKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKVxyXG4gICAgfVxyXG59Il19