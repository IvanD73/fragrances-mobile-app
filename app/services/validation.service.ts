import { Injectable } from '@angular/core';

@Injectable()
export class ValidationService{
    static isValidEmail(string){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(string);
    }

    static isValidPassword(string: String){
        return string.length >= 6
    }

    static isValidConfirm(string1:String, string2: String){
        return string1 == string2
    }

    static isValidFirstname(string){
        return string.length >= 3
    }

    static isValidLastname(string){
        return string.length >= 3
    }

    static isValidAddress(string){
        return string.length >= 3
    }

    static isValidCity(string){
        return string.length >= 3
    }

    static isValidCountryId(country_id){
        return !!country_id
    }

    static isValidZoneId(zone_id){
        return !!zone_id
    }

    static isValidTelephone(string){
        return true
        /*
        //https://stackoverflow.com/questions/4338267/validate-phone-number-with-javascript
        //Maybe change it, hasnt been tested much
        var phoneRe = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
        var digits = string.replace(/\D/g, "");
        return phoneRe.test(digits);
        */
    }

    
}