import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { RouterExtensions } from "nativescript-angular/router";
import { AuthService } from '../services/auth.service'

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {
  constructor(
    private authService: AuthService,
    private router: Router,
    private routerExtensions: RouterExtensions
  ) {

  }

  canActivate() {
    if (this.authService.isLogged()) {
      return true;
    } else {
      this.routerExtensions.navigate(["/login"]);
      return false;
    }
  }

  canActivateChild() {
    if (this.authService.isLogged()) {
      return true;
    } else {
      this.routerExtensions.navigate(["/login"]);
      return false;
    }
  }
}