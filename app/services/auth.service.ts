import { BaseService } from '../services/base.service'
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import * as applicationSettings from "application-settings";

@Injectable()
export class AuthService extends BaseService {
    constructor(http: Http) {
        super(http)
    }

    /* Used in another service for obtaining the user token*/
    public getToken(){
        return applicationSettings.getString('token', '')
    }

    private setToken(token){
        applicationSettings.setString('token', token)
    }

    isLogged() {
        return !!this.getToken()
    }

    
    logout() {
        this.setToken('')
    }

    getCustomer(success, error){
        if(!this.isLogged()){
            error('User is not logged');
        }

        this.executePostRequest('/account/customer', {token: this.getToken()})
            .subscribe( response => {
                if(!response.error){
                    success(response.customer)
                }else{
                    error(response.message)
                }
            })
    }

    editCustomer(customer, success, error){
        if(!this.isLogged()){
            error('User is not logged');
        }

        this.executePostRequest('/account/edit', {token: this.getToken(), customer: customer})
            .subscribe( response => {
                if(!response.error){
                    success(response.customer)
                }else{
                    error(response.message)
                }
            },
            error => {
                
            })
    }

    loginWithEmailAndPassword(email, password, success, error) {
        let credentials = {
            email: email,
            password: password
        }
        
        this.executePostRequest('/account/login', credentials)
            .subscribe(
                response => {
                    if(!response.error){
                        //Logs the customer
                        this.setToken(response.token)
                        success()
                    }else{
                        error(error.message)
                    }
                    
                }
            )
    }

    register(customer, success, error){
        this.executePostRequest('/account/register', customer)
        .subscribe(
            response => {
                if(!response.error){
                    //Logs the customer
                    this.setToken(response.token)
                    success()
                }else{
                    error(error.message)
                }
            }
        )
    }
}