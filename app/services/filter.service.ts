export class FilterService{

	getManufacturers(){
		return [
			{name:'Abercrombie & Fitch', id:1},
			{name:'Acqua di Parma', id:2},
			{name:'Agent Provocateur', id:3},
			{name:'Alaia', id:4},
			{name:'Amouage', id:5},
			{name:'Angel Schlesser', id:6},
			{name:'Antonio Puig', id:7},
			{name:'Antonio Banderas', id:8},
			{name:'Blumarine', id:9},
			{name:'Bottega Veneta', id:10},
			{name:'Boucheron', id:11},
			{name:'Bruno Banan', id:12},
			{name:'Burberry', id:13},
			{name:'Calvin Klein', id:14},
			{name:'Dsquared2', id:15},
		]
	}

	getАromaticGroups(){
		return [
			{name:'Ароматна морска', id:1},
			{name:'Ароматна папратова', id:2},
			{name:'Ароматна плодова', id:3},
			{name:'Ароматна тревиста', id:4},
			{name:'Ароматни подправки', id:5},
			{name:'Шипрова цветна', id:6},
			{name:'Шипрова плодова', id:7},
			{name:'Цитрусова ароматна', id:8},
			{name:'Цитрусова сладникава', id:9},
				
		]
	}

}