import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { DrawerPage } from "../../drawer.page";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { CategoriesService } from '../../../services/categories.service';

@Component({
    selector: 'subcategoriesList',
    templateUrl:'./pages/categories/subcategory-list/subcategoryList.component.html',
    providers:[CategoriesService]
})

export class SubcategoryList extends DrawerPage implements OnInit  {
	selectedCategoryChildrens:Array<Object>;
    parentId;

    constructor(
    	private changeDetectorRef: ChangeDetectorRef,
        private routerExtensions: RouterExtensions,
    	private route: ActivatedRoute,
        private categoriesServices: CategoriesService) {
        super(changeDetectorRef);
    }

    ngOnInit(){
        this.route.params.subscribe(
            (id) => this.parentId = id.id
        )

    	this.selectedCategoryChildrens = this.categoriesServices.getSubcategoriesByParentId(this.parentId);
        console.dir(this.selectedCategoryChildrens);
    }

    onCategoryTap(category){
        this.routerExtensions.navigate(["/category", category.id]);
    }

}
