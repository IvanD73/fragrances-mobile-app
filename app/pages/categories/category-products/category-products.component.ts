import { Component, OnInit, ChangeDetectorRef, ViewChild, ViewContainerRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { DrawerPage } from "../../drawer.page";
import { CategoriesService } from '../../../services/categories.service';
import { ProductsServices } from '../../../services/products.services';
import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { FilterModalComponent } from '../../../components/modal/filter/filter-modal.component';

@Component({
    selector: 'categories-page',
    templateUrl: './pages/categories/category-products/category-products.component.html',
    providers:[CategoriesService]
})

export class CategoryProducts extends DrawerPage implements OnInit  {
	filterDropdown:boolean = false;
	products;
    gridView:string = 'list';

    constructor(
    	private changeDetectorRef: ChangeDetectorRef,
    	private categoriesServices: CategoriesService,
    	private productService: ProductsServices,
        private routerExtensions: RouterExtensions,
        private modal: ModalDialogService,
        private vcRef: ViewContainerRef,
        ){
        super(changeDetectorRef);
    }

    ngOnInit(){
    	this.products = this.productService.getProductsByCategoryId();
    }

    toggleFilterDropdown(){
    	this.filterDropdown = !this.filterDropdown;
    }

    onFilterChange(){
        console.log('TODO CHANGE FILTER')
        this.filterDropdown = !this.filterDropdown;
    }

    openFilterModal(){
        let options = {
            context: {},
            fullscreen: false,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(FilterModalComponent, options).then(res => {
            console.log(res);
        });
    }

    onProductTap(product){
        this.routerExtensions.navigate(["/product",  product.id]);
    }

    changeGridView(){
        if(this.gridView === 'list'){
            this.gridView = 'block';
        }else{
            this.gridView = "list";
        }
    }


}
