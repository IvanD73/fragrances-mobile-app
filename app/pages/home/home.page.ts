import { Component, OnInit, ChangeDetectorRef, ViewContainerRef, ViewChild, AfterViewInit } from "@angular/core";
import { DrawerPage } from "../drawer.page";
import { ProductsServices } from '../../services/products.services';
import { RouterExtensions } from "nativescript-angular/router";
import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { SearchModalComponent } from '../../components/modal/search/search-modal.component';


@Component({
    selector: 'home-page',
    templateUrl: 'pages/home/home.page.html',
})
export class HomePage extends DrawerPage {
    newProducts: Array<Object>;
    saleProducts: Array<Object>;
    mostViewProducts: Array<Object>;
    showSearch: boolean = false;
    
    
    constructor(
        private changeDetectorRef: ChangeDetectorRef,
        private productsServices: ProductsServices,
        private routerExtensions: RouterExtensions,
        private modal: ModalDialogService,
        private vcRef: ViewContainerRef
    ) {
        super(changeDetectorRef);
    }

    ngOnInit() {
        this.newProducts = this.productsServices.getNewProducts();
        this.saleProducts = this.productsServices.getSaleProducts();
        this.mostViewProducts = this.productsServices.getMostViewProducts();
    }

    ngAfterViewInit() {
        //Call parent method
        super.ngAfterViewInit()

    }
    

    onSearchTap() {
        let options = {
            context: {},
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(SearchModalComponent, options).then(res => {
            console.log(res);
        });
    }
    

    onProductTap(id: number) {
        this.routerExtensions.navigate(["/product/" + id]);
    }


}
