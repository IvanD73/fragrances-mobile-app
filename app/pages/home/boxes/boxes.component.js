"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var BoxesComponent = (function () {
    function BoxesComponent() {
    }
    BoxesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'boxes',
            templateUrl: 'boxes.component.html'
        })
    ], BoxesComponent);
    return BoxesComponent;
}());
exports.BoxesComponent = BoxesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm94ZXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYm94ZXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXdDO0FBUXhDO0lBQUE7SUFBNEIsQ0FBQztJQUFoQixjQUFjO1FBTjFCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLE9BQU87WUFDakIsV0FBVyxFQUFFLHNCQUFzQjtTQUN0QyxDQUFDO09BRVcsY0FBYyxDQUFFO0lBQUQscUJBQUM7Q0FBQSxBQUE3QixJQUE2QjtBQUFoQix3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50fSBmcm9tICAnQGFuZ3VsYXIvY29yZSdcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnYm94ZXMnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdib3hlcy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBCb3hlc0NvbXBvbmVudHt9Il19