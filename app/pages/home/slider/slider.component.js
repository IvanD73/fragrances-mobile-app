"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SliderComponent = (function () {
    function SliderComponent() {
        this.elements = [];
    }
    SliderComponent.prototype.ngOnInit = function () {
        this.elements = [
            { image: '~/images/slider/banner-1.jpg' },
            { image: '~/images/slider/banner-2.jpg' },
            { image: '~/images/slider/banner-1.jpg' }
        ];
    };
    SliderComponent.prototype.onSlideTap = function (slide) {
        console.log(JSON.stringify(slide));
    };
    SliderComponent.prototype.ngAfterViewInit = function () {
        //Automatcally start the slider
        this.autostartSlider();
    };
    SliderComponent.prototype.onSlideChanged = function () {
    };
    SliderComponent.prototype.onTap = function (element) {
        //Slider Tapped
    };
    SliderComponent.prototype.autostartSlider = function (interval) {
        if (interval === void 0) { interval = 10000; }
    };
    SliderComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'slider-component',
            templateUrl: 'slider.component.html'
        })
    ], SliderComponent);
    return SliderComponent;
}());
exports.SliderComponent = SliderComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNsaWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBaUU7QUFRakU7SUFOQTtRQU9JLGFBQVEsR0FBZSxFQUFFLENBQUM7SUE4QjlCLENBQUM7SUE1Qkcsa0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUc7WUFDWixFQUFDLEtBQUssRUFBRSw4QkFBOEIsRUFBQztZQUN2QyxFQUFDLEtBQUssRUFBRSw4QkFBOEIsRUFBQztZQUN2QyxFQUFDLEtBQUssRUFBRSw4QkFBOEIsRUFBQztTQUMxQyxDQUFBO0lBQ0wsQ0FBQztJQUVELG9DQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUE7SUFDdEMsQ0FBQztJQUVELHlDQUFlLEdBQWY7UUFDSSwrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO0lBQzFCLENBQUM7SUFFRCx3Q0FBYyxHQUFkO0lBRUEsQ0FBQztJQUVELCtCQUFLLEdBQUwsVUFBTSxPQUFPO1FBQ1QsZUFBZTtJQUNuQixDQUFDO0lBRUQseUNBQWUsR0FBZixVQUFnQixRQUF3QjtRQUF4Qix5QkFBQSxFQUFBLGdCQUF3QjtJQUV4QyxDQUFDO0lBOUJRLGVBQWU7UUFOM0IsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsa0JBQWtCO1lBQzVCLFdBQVcsRUFBRSx1QkFBdUI7U0FDdkMsQ0FBQztPQUVXLGVBQWUsQ0ErQjNCO0lBQUQsc0JBQUM7Q0FBQSxBQS9CRCxJQStCQztBQS9CWSwwQ0FBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBBZnRlclZpZXdJbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnc2xpZGVyLWNvbXBvbmVudCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ3NsaWRlci5jb21wb25lbnQuaHRtbCdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTbGlkZXJDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0e1xyXG4gICAgZWxlbWVudHM6IEFycmF5PGFueT4gPSBbXTtcclxuXHJcbiAgICBuZ09uSW5pdCgpe1xyXG4gICAgICAgIHRoaXMuZWxlbWVudHMgPSBbXHJcbiAgICAgICAgICAgIHtpbWFnZTogJ34vaW1hZ2VzL3NsaWRlci9iYW5uZXItMS5qcGcnfSxcclxuICAgICAgICAgICAge2ltYWdlOiAnfi9pbWFnZXMvc2xpZGVyL2Jhbm5lci0yLmpwZyd9LFxyXG4gICAgICAgICAgICB7aW1hZ2U6ICd+L2ltYWdlcy9zbGlkZXIvYmFubmVyLTEuanBnJ31cclxuICAgICAgICBdXHJcbiAgICB9XHJcblxyXG4gICAgb25TbGlkZVRhcChzbGlkZSl7XHJcbiAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkoc2xpZGUpKVxyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgICAgICAvL0F1dG9tYXRjYWxseSBzdGFydCB0aGUgc2xpZGVyXHJcbiAgICAgICAgdGhpcy5hdXRvc3RhcnRTbGlkZXIoKVxyXG4gICAgfVxyXG5cclxuICAgIG9uU2xpZGVDaGFuZ2VkKCl7XHJcblxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBvblRhcChlbGVtZW50KXtcclxuICAgICAgICAvL1NsaWRlciBUYXBwZWRcclxuICAgIH1cclxuXHJcbiAgICBhdXRvc3RhcnRTbGlkZXIoaW50ZXJ2YWw6IG51bWJlciA9IDEwMDAwKSB7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbn0iXX0=