import { Component,ChangeDetectorRef, ViewContainerRef  } from "@angular/core";
import { OnInit } from '@angular/core';
import { Page } from "../../page";
import { AuthService } from '../../../services/auth.service'
import { Location } from "@angular/common";
import { RouterExtensions } from "nativescript-angular/router";
import { AccountService } from "../../../services/account.service";
import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { AddressModalComponent } from '../../../components/modal/address/address-modal.component'

@Component({
    moduleId: module.id,
    selector: 'account-addresses',
    templateUrl: 'account-addresses.page.html',
})

export class AccountAddressesPage extends Page implements OnInit{
    addresses = [];
    
    constructor( private location: Location,
                 private accountAddressService: AccountService,
                 private modal: ModalDialogService,
                 private vcRef: ViewContainerRef
                ) {
                    super(location);
    }

    ngOnInit(){
        this.accountAddressService.getAddresses(
            addresses => {
               this.addresses = addresses
            },
            error => {
                console.log(error)
            }
        )
    }

    editExistingAddress(address){
        let options = {
            context: {},
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(AddressModalComponent, options).then(res => {
           //close callback
        });
    }
}