"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../page");
var common_1 = require("@angular/common");
var account_service_1 = require("../../../services/account.service");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var address_modal_component_1 = require("../../../components/modal/address/address-modal.component");
var AccountAddressesPage = (function (_super) {
    __extends(AccountAddressesPage, _super);
    function AccountAddressesPage(location, accountAddressService, modal, vcRef) {
        var _this = _super.call(this, location) || this;
        _this.location = location;
        _this.accountAddressService = accountAddressService;
        _this.modal = modal;
        _this.vcRef = vcRef;
        _this.addresses = [];
        return _this;
    }
    AccountAddressesPage.prototype.ngOnInit = function () {
        var _this = this;
        this.accountAddressService.getAddresses(function (addresses) {
            _this.addresses = addresses;
        }, function (error) {
            console.log(error);
        });
    };
    AccountAddressesPage.prototype.editExistingAddress = function (address) {
        var options = {
            context: {},
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(address_modal_component_1.AddressModalComponent, options).then(function (res) {
            //close callback
        });
    };
    AccountAddressesPage = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'account-addresses',
            templateUrl: 'account-addresses.page.html',
        }),
        __metadata("design:paramtypes", [common_1.Location,
            account_service_1.AccountService,
            dialogs_1.ModalDialogService,
            core_1.ViewContainerRef])
    ], AccountAddressesPage);
    return AccountAddressesPage;
}(page_1.Page));
exports.AccountAddressesPage = AccountAddressesPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1hZGRyZXNzZXMucGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjY291bnQtYWRkcmVzc2VzLnBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBK0U7QUFFL0UsbUNBQWtDO0FBRWxDLDBDQUEyQztBQUUzQyxxRUFBbUU7QUFDbkUsbUVBQTZFO0FBQzdFLHFHQUFpRztBQVFqRztJQUEwQyx3Q0FBSTtJQUcxQyw4QkFBcUIsUUFBa0IsRUFDbEIscUJBQXFDLEVBQ3JDLEtBQXlCLEVBQ3pCLEtBQXVCO1FBSDVDLFlBS2dCLGtCQUFNLFFBQVEsQ0FBQyxTQUM5QjtRQU5vQixjQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLDJCQUFxQixHQUFyQixxQkFBcUIsQ0FBZ0I7UUFDckMsV0FBSyxHQUFMLEtBQUssQ0FBb0I7UUFDekIsV0FBSyxHQUFMLEtBQUssQ0FBa0I7UUFMNUMsZUFBUyxHQUFHLEVBQUUsQ0FBQzs7SUFRZixDQUFDO0lBRUQsdUNBQVEsR0FBUjtRQUFBLGlCQVNDO1FBUkcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksQ0FDbkMsVUFBQSxTQUFTO1lBQ04sS0FBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7UUFDN0IsQ0FBQyxFQUNELFVBQUEsS0FBSztZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDdEIsQ0FBQyxDQUNKLENBQUE7SUFDTCxDQUFDO0lBRUQsa0RBQW1CLEdBQW5CLFVBQW9CLE9BQU87UUFDdkIsSUFBSSxPQUFPLEdBQUc7WUFDVixPQUFPLEVBQUUsRUFBRTtZQUNYLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxLQUFLO1NBQy9CLENBQUM7UUFDRixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQywrQ0FBcUIsRUFBRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO1lBQzFELGdCQUFnQjtRQUNuQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUEvQlEsb0JBQW9CO1FBTmhDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixXQUFXLEVBQUUsNkJBQTZCO1NBQzdDLENBQUM7eUNBS2lDLGlCQUFRO1lBQ0ssZ0NBQWM7WUFDOUIsNEJBQWtCO1lBQ2xCLHVCQUFnQjtPQU5uQyxvQkFBb0IsQ0FnQ2hDO0lBQUQsMkJBQUM7Q0FBQSxBQWhDRCxDQUEwQyxXQUFJLEdBZ0M3QztBQWhDWSxvREFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsQ2hhbmdlRGV0ZWN0b3JSZWYsIFZpZXdDb250YWluZXJSZWYgIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwiLi4vLi4vcGFnZVwiO1xyXG5pbXBvcnQgeyBBdXRoU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL2F1dGguc2VydmljZSdcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSBcIi4uLy4uLy4uL3NlcnZpY2VzL2FjY291bnQuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBNb2RhbERpYWxvZ1NlcnZpY2UgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZGlyZWN0aXZlcy9kaWFsb2dzXCI7XHJcbmltcG9ydCB7IEFkZHJlc3NNb2RhbENvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uL2NvbXBvbmVudHMvbW9kYWwvYWRkcmVzcy9hZGRyZXNzLW1vZGFsLmNvbXBvbmVudCdcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnYWNjb3VudC1hZGRyZXNzZXMnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdhY2NvdW50LWFkZHJlc3Nlcy5wYWdlLmh0bWwnLFxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFjY291bnRBZGRyZXNzZXNQYWdlIGV4dGVuZHMgUGFnZSBpbXBsZW1lbnRzIE9uSW5pdHtcclxuICAgIGFkZHJlc3NlcyA9IFtdO1xyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3RvciggcHJpdmF0ZSBsb2NhdGlvbjogTG9jYXRpb24sXHJcbiAgICAgICAgICAgICAgICAgcHJpdmF0ZSBhY2NvdW50QWRkcmVzc1NlcnZpY2U6IEFjY291bnRTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgIHByaXZhdGUgbW9kYWw6IE1vZGFsRGlhbG9nU2VydmljZSxcclxuICAgICAgICAgICAgICAgICBwcml2YXRlIHZjUmVmOiBWaWV3Q29udGFpbmVyUmVmXHJcbiAgICAgICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgICAgICBzdXBlcihsb2NhdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKXtcclxuICAgICAgICB0aGlzLmFjY291bnRBZGRyZXNzU2VydmljZS5nZXRBZGRyZXNzZXMoXHJcbiAgICAgICAgICAgIGFkZHJlc3NlcyA9PiB7XHJcbiAgICAgICAgICAgICAgIHRoaXMuYWRkcmVzc2VzID0gYWRkcmVzc2VzXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKVxyXG4gICAgfVxyXG5cclxuICAgIGVkaXRFeGlzdGluZ0FkZHJlc3MoYWRkcmVzcyl7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGNvbnRleHQ6IHt9LFxyXG4gICAgICAgICAgICBmdWxsc2NyZWVuOiB0cnVlLFxyXG4gICAgICAgICAgICB2aWV3Q29udGFpbmVyUmVmOiB0aGlzLnZjUmVmXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLm1vZGFsLnNob3dNb2RhbChBZGRyZXNzTW9kYWxDb21wb25lbnQsIG9wdGlvbnMpLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgICAvL2Nsb3NlIGNhbGxiYWNrXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iXX0=