"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../page");
var common_1 = require("@angular/common");
var auth_service_1 = require("../../../services/auth.service");
var validation_service_1 = require("../../../services/validation.service");
var AccountEditPage = (function (_super) {
    __extends(AccountEditPage, _super);
    function AccountEditPage(location, authService) {
        var _this = _super.call(this, location) || this;
        _this.location = location;
        _this.authService = authService;
        _this.customer = {
            firstname: '',
            lastname: '',
            email: '',
            telephone: '',
            fax: '',
            newsletter: ''
        };
        _this.error = {
            firstname: '',
            lastname: '',
            email: '',
            telephone: '',
            fax: ''
        };
        return _this;
    }
    AccountEditPage.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getCustomer(function (customer) {
            _this.customer = customer;
        }, function (error) {
            console.log(error);
        });
    };
    AccountEditPage.prototype.editCustomer = function () {
        if (this.validateCustomer()) {
            this.authService.editCustomer(this.customer, function (customer) {
                /* User has been succesfully updated */
                /* todo: notification */
            }, function (error) {
                console.log(error);
            });
        }
    };
    AccountEditPage.prototype.validateCustomer = function () {
        this.clearErrors();
        if (!validation_service_1.ValidationService.isValidFirstname(this.customer.firstname)) {
            this.error.firstname = 'Invalid firstname';
        }
        if (!validation_service_1.ValidationService.isValidLastname(this.customer.lastname)) {
            this.error.lastname = 'Invalid lastname';
        }
        if (!validation_service_1.ValidationService.isValidEmail(this.customer.email)) {
            this.error.email = 'Invalid email';
        }
        if (!validation_service_1.ValidationService.isValidTelephone(this.customer.telephone)) {
            this.error.telephone = 'Invalid telephone';
        }
        return !this.hasErrors();
    };
    AccountEditPage.prototype.hasErrors = function () {
        for (var key in this.error) {
            if (this.error[key]) {
                return true;
            }
        }
        return false;
    };
    AccountEditPage.prototype.clearErrors = function () {
        for (var key in this.error) {
            this.error[key] = '';
        }
    };
    AccountEditPage = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'account-edit',
            templateUrl: 'account-edit.page.html',
        }),
        __metadata("design:paramtypes", [common_1.Location, auth_service_1.AuthService])
    ], AccountEditPage);
    return AccountEditPage;
}(page_1.Page));
exports.AccountEditPage = AccountEditPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1lZGl0LnBhZ2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhY2NvdW50LWVkaXQucGFnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUE2RDtBQUU3RCxtQ0FBa0M7QUFDbEMsMENBQTJDO0FBQzNDLCtEQUE0RDtBQUU1RCwyRUFBd0U7QUFReEU7SUFBcUMsbUNBQUk7SUFrQnJDLHlCQUFvQixRQUFrQixFQUFVLFdBQXdCO1FBQXhFLFlBQ0ksa0JBQU0sUUFBUSxDQUFDLFNBQ2xCO1FBRm1CLGNBQVEsR0FBUixRQUFRLENBQVU7UUFBVSxpQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQWpCeEUsY0FBUSxHQUFHO1lBQ1AsU0FBUyxFQUFFLEVBQUU7WUFDYixRQUFRLEVBQUUsRUFBRTtZQUNaLEtBQUssRUFBRSxFQUFFO1lBQ1QsU0FBUyxFQUFFLEVBQUU7WUFDYixHQUFHLEVBQUUsRUFBRTtZQUNQLFVBQVUsRUFBRSxFQUFFO1NBQ2pCLENBQUE7UUFFRCxXQUFLLEdBQUc7WUFDSixTQUFTLEVBQUUsRUFBRTtZQUNiLFFBQVEsRUFBRSxFQUFFO1lBQ1osS0FBSyxFQUFFLEVBQUU7WUFDVCxTQUFTLEVBQUUsRUFBRTtZQUNiLEdBQUcsRUFBRSxFQUFFO1NBQ1YsQ0FBQTs7SUFJRCxDQUFDO0lBRUQsa0NBQVEsR0FBUjtRQUFBLGlCQVNDO1FBUkcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQ3hCLFVBQUEsUUFBUTtZQUNKLEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFBO1FBQzVCLENBQUMsRUFDRCxVQUFBLEtBQUs7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3RCLENBQUMsQ0FDSixDQUFBO0lBQ0wsQ0FBQztJQUVELHNDQUFZLEdBQVo7UUFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFDdkMsVUFBQSxRQUFRO2dCQUNKLHVDQUF1QztnQkFDdkMsd0JBQXdCO1lBQzVCLENBQUMsRUFDRCxVQUFBLEtBQUs7Z0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUN0QixDQUFDLENBQ0osQ0FBQTtRQUNMLENBQUM7SUFDTCxDQUFDO0lBR0QsMENBQWdCLEdBQWhCO1FBQ0ksSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFBO1FBRWxCLEVBQUUsQ0FBQyxDQUFDLENBQUMsc0NBQWlCLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUE7UUFDOUMsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLENBQUMsc0NBQWlCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLGtCQUFrQixDQUFBO1FBQzVDLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLHNDQUFpQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxlQUFlLENBQUE7UUFDdEMsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLENBQUMsc0NBQWlCLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUE7UUFDOUMsQ0FBQztRQUVELE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQTtJQUM1QixDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUNJLEdBQUcsQ0FBQyxDQUFDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsQixNQUFNLENBQUMsSUFBSSxDQUFBO1lBQ2YsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFBO0lBQ2hCLENBQUM7SUFFRCxxQ0FBVyxHQUFYO1FBQ0ksR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLENBQUE7UUFDeEIsQ0FBQztJQUNMLENBQUM7SUFuRlEsZUFBZTtRQU4zQixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFFBQVEsRUFBRSxjQUFjO1lBQ3hCLFdBQVcsRUFBRSx3QkFBd0I7U0FDeEMsQ0FBQzt5Q0FvQmdDLGlCQUFRLEVBQXVCLDBCQUFXO09BbEIvRCxlQUFlLENBb0YzQjtJQUFELHNCQUFDO0NBQUEsQUFwRkQsQ0FBcUMsV0FBSSxHQW9GeEM7QUFwRlksMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwiLi4vLi4vcGFnZVwiO1xyXG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnXHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFZhbGlkYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvdmFsaWRhdGlvbi5zZXJ2aWNlJ1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdhY2NvdW50LWVkaXQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdhY2NvdW50LWVkaXQucGFnZS5odG1sJyxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBY2NvdW50RWRpdFBhZ2UgZXh0ZW5kcyBQYWdlIGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGN1c3RvbWVyID0ge1xyXG4gICAgICAgIGZpcnN0bmFtZTogJycsXHJcbiAgICAgICAgbGFzdG5hbWU6ICcnLFxyXG4gICAgICAgIGVtYWlsOiAnJyxcclxuICAgICAgICB0ZWxlcGhvbmU6ICcnLFxyXG4gICAgICAgIGZheDogJycsXHJcbiAgICAgICAgbmV3c2xldHRlcjogJydcclxuICAgIH1cclxuXHJcbiAgICBlcnJvciA9IHtcclxuICAgICAgICBmaXJzdG5hbWU6ICcnLFxyXG4gICAgICAgIGxhc3RuYW1lOiAnJyxcclxuICAgICAgICBlbWFpbDogJycsXHJcbiAgICAgICAgdGVsZXBob25lOiAnJyxcclxuICAgICAgICBmYXg6ICcnXHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBsb2NhdGlvbjogTG9jYXRpb24sIHByaXZhdGUgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIobG9jYXRpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuYXV0aFNlcnZpY2UuZ2V0Q3VzdG9tZXIoXHJcbiAgICAgICAgICAgIGN1c3RvbWVyID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3VzdG9tZXIgPSBjdXN0b21lclxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcilcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIClcclxuICAgIH1cclxuXHJcbiAgICBlZGl0Q3VzdG9tZXIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudmFsaWRhdGVDdXN0b21lcigpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXV0aFNlcnZpY2UuZWRpdEN1c3RvbWVyKHRoaXMuY3VzdG9tZXIsXHJcbiAgICAgICAgICAgICAgICBjdXN0b21lciA9PiB7IFxyXG4gICAgICAgICAgICAgICAgICAgIC8qIFVzZXIgaGFzIGJlZW4gc3VjY2VzZnVsbHkgdXBkYXRlZCAqL1xyXG4gICAgICAgICAgICAgICAgICAgIC8qIHRvZG86IG5vdGlmaWNhdGlvbiAqL1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcilcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG4gICAgdmFsaWRhdGVDdXN0b21lcigpIHtcclxuICAgICAgICB0aGlzLmNsZWFyRXJyb3JzKClcclxuXHJcbiAgICAgICAgaWYgKCFWYWxpZGF0aW9uU2VydmljZS5pc1ZhbGlkRmlyc3RuYW1lKHRoaXMuY3VzdG9tZXIuZmlyc3RuYW1lKSkge1xyXG4gICAgICAgICAgICB0aGlzLmVycm9yLmZpcnN0bmFtZSA9ICdJbnZhbGlkIGZpcnN0bmFtZSdcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghVmFsaWRhdGlvblNlcnZpY2UuaXNWYWxpZExhc3RuYW1lKHRoaXMuY3VzdG9tZXIubGFzdG5hbWUpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3IubGFzdG5hbWUgPSAnSW52YWxpZCBsYXN0bmFtZSdcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghVmFsaWRhdGlvblNlcnZpY2UuaXNWYWxpZEVtYWlsKHRoaXMuY3VzdG9tZXIuZW1haWwpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1haWwgPSAnSW52YWxpZCBlbWFpbCdcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghVmFsaWRhdGlvblNlcnZpY2UuaXNWYWxpZFRlbGVwaG9uZSh0aGlzLmN1c3RvbWVyLnRlbGVwaG9uZSkpIHtcclxuICAgICAgICAgICAgdGhpcy5lcnJvci50ZWxlcGhvbmUgPSAnSW52YWxpZCB0ZWxlcGhvbmUnXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gIXRoaXMuaGFzRXJyb3JzKClcclxuICAgIH1cclxuXHJcbiAgICBoYXNFcnJvcnMoKSB7XHJcbiAgICAgICAgZm9yICh2YXIga2V5IGluIHRoaXMuZXJyb3IpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZXJyb3Jba2V5XSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWVcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2VcclxuICAgIH1cclxuXHJcbiAgICBjbGVhckVycm9ycygpIHtcclxuICAgICAgICBmb3IgKHZhciBrZXkgaW4gdGhpcy5lcnJvcikge1xyXG4gICAgICAgICAgICB0aGlzLmVycm9yW2tleV0gPSAnJ1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSJdfQ==