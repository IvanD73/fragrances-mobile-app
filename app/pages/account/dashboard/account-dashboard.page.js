"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var drawer_page_1 = require("../../drawer.page");
var auth_service_1 = require("../../../services/auth.service");
var router_1 = require("nativescript-angular/router");
var AccountDashboardPage = (function (_super) {
    __extends(AccountDashboardPage, _super);
    function AccountDashboardPage(changeDetectorRef, authService, routerExtensions) {
        var _this = _super.call(this, changeDetectorRef) || this;
        _this.changeDetectorRef = changeDetectorRef;
        _this.authService = authService;
        _this.routerExtensions = routerExtensions;
        return _this;
    }
    AccountDashboardPage.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getCustomer(function (customer) {
            console.log(JSON.stringify(customer));
        }, function (error) {
            //Error getting user, might be logged on a different device
            //Logout user and redirect to login
            _this.authService.logout();
            _this.routerExtensions.navigate(["/login"]);
        });
    };
    AccountDashboardPage = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'account-dashboard',
            templateUrl: 'account-dashboard.page.html',
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef,
            auth_service_1.AuthService,
            router_1.RouterExtensions])
    ], AccountDashboardPage);
    return AccountDashboardPage;
}(drawer_page_1.DrawerPage));
exports.AccountDashboardPage = AccountDashboardPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1kYXNoYm9hcmQucGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjY291bnQtZGFzaGJvYXJkLnBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBNEQ7QUFFNUQsaURBQStDO0FBQy9DLCtEQUE0RDtBQUM1RCxzREFBK0Q7QUFTL0Q7SUFBMEMsd0NBQVU7SUFDaEQsOEJBQXFCLGlCQUFvQyxFQUNwQyxXQUF1QixFQUN2QixnQkFBa0M7UUFGdkQsWUFJSSxrQkFBTSxpQkFBaUIsQ0FBQyxTQUMzQjtRQUxvQix1QkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLGlCQUFXLEdBQVgsV0FBVyxDQUFZO1FBQ3ZCLHNCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7O0lBR3ZELENBQUM7SUFFRCx1Q0FBUSxHQUFSO1FBQUEsaUJBU0M7UUFSRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBRSxVQUFBLFFBQVE7WUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7UUFDekMsQ0FBQyxFQUFFLFVBQUEsS0FBSztZQUNKLDJEQUEyRDtZQUMzRCxtQ0FBbUM7WUFDbkMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQTtZQUN6QixLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUMvQyxDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFqQlEsb0JBQW9CO1FBTmhDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixXQUFXLEVBQUUsNkJBQTZCO1NBQzdDLENBQUM7eUNBRzBDLHdCQUFpQjtZQUN4QiwwQkFBVztZQUNMLHlCQUFnQjtPQUg5QyxvQkFBb0IsQ0FrQmhDO0lBQUQsMkJBQUM7Q0FBQSxBQWxCRCxDQUEwQyx3QkFBVSxHQWtCbkQ7QUFsQlksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERyYXdlclBhZ2UgfSBmcm9tIFwiLi4vLi4vZHJhd2VyLnBhZ2VcIjtcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnXHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdhY2NvdW50LWRhc2hib2FyZCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2FjY291bnQtZGFzaGJvYXJkLnBhZ2UuaHRtbCcsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQWNjb3VudERhc2hib2FyZFBhZ2UgZXh0ZW5kcyBEcmF3ZXJQYWdlIGltcGxlbWVudHMgT25Jbml0e1xyXG4gICAgY29uc3RydWN0b3IoIHByaXZhdGUgY2hhbmdlRGV0ZWN0b3JSZWY6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgICAgICAgICAgICAgIHByaXZhdGUgYXV0aFNlcnZpY2U6QXV0aFNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICAgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zXHJcbiAgICAgICAgICAgICAgICApIHtcclxuICAgICAgICBzdXBlcihjaGFuZ2VEZXRlY3RvclJlZilcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpe1xyXG4gICAgICAgIHRoaXMuYXV0aFNlcnZpY2UuZ2V0Q3VzdG9tZXIoIGN1c3RvbWVyID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkoY3VzdG9tZXIpKVxyXG4gICAgICAgIH0sIGVycm9yID0+IHtcclxuICAgICAgICAgICAgLy9FcnJvciBnZXR0aW5nIHVzZXIsIG1pZ2h0IGJlIGxvZ2dlZCBvbiBhIGRpZmZlcmVudCBkZXZpY2VcclxuICAgICAgICAgICAgLy9Mb2dvdXQgdXNlciBhbmQgcmVkaXJlY3QgdG8gbG9naW5cclxuICAgICAgICAgICAgdGhpcy5hdXRoU2VydmljZS5sb2dvdXQoKVxyXG4gICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2xvZ2luXCJdKTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG59Il19