"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../page");
var common_1 = require("@angular/common");
var account_service_1 = require("../../../services/account.service");
var AccountOrdersPage = (function (_super) {
    __extends(AccountOrdersPage, _super);
    function AccountOrdersPage(location, accountService) {
        var _this = _super.call(this, location) || this;
        _this.location = location;
        _this.accountService = accountService;
        _this.start = 0;
        _this.limit = 10;
        _this.orders = [];
        return _this;
    }
    AccountOrdersPage.prototype.ngOnInit = function () {
        this.getOrders();
    };
    AccountOrdersPage.prototype.getOrders = function () {
        var _this = this;
        this.accountService.getOrders(function (orders) {
            _this.orders = orders;
            console.log(orders);
        }, function (error) {
            console.log(error);
        }, this.start, this.limit);
    };
    AccountOrdersPage = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'account-orders',
            templateUrl: 'account-orders.page.html',
        }),
        __metadata("design:paramtypes", [common_1.Location, account_service_1.AccountService])
    ], AccountOrdersPage);
    return AccountOrdersPage;
}(page_1.Page));
exports.AccountOrdersPage = AccountOrdersPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1vcmRlcnMucGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjY291bnQtb3JkZXJzLnBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBNkQ7QUFFN0QsbUNBQWtDO0FBR2xDLDBDQUEyQztBQUMzQyxxRUFBbUU7QUFRbkU7SUFBdUMscUNBQUk7SUFLdkMsMkJBQW9CLFFBQWtCLEVBQVUsY0FBOEI7UUFBOUUsWUFDSSxrQkFBTSxRQUFRLENBQUMsU0FDbEI7UUFGbUIsY0FBUSxHQUFSLFFBQVEsQ0FBVTtRQUFVLG9CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUp0RSxXQUFLLEdBQUcsQ0FBQyxDQUFBO1FBQ1QsV0FBSyxHQUFHLEVBQUUsQ0FBQTtRQUNsQixZQUFNLEdBQUcsRUFBRSxDQUFBOztJQUlYLENBQUM7SUFFRCxvQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFBO0lBQ3BCLENBQUM7SUFFTSxxQ0FBUyxHQUFoQjtRQUFBLGlCQVlDO1FBWEcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQ3pCLFVBQUEsTUFBTTtZQUNGLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO1lBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7UUFDdkIsQ0FBQyxFQUNELFVBQUEsS0FBSztZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDdEIsQ0FBQyxFQUNELElBQUksQ0FBQyxLQUFLLEVBQ1YsSUFBSSxDQUFDLEtBQUssQ0FDYixDQUFBO0lBQ0wsQ0FBQztJQXpCUSxpQkFBaUI7UUFON0IsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsZ0JBQWdCO1lBQzFCLFdBQVcsRUFBRSwwQkFBMEI7U0FDMUMsQ0FBQzt5Q0FPZ0MsaUJBQVEsRUFBMEIsZ0NBQWM7T0FMckUsaUJBQWlCLENBMkI3QjtJQUFELHdCQUFDO0NBQUEsQUEzQkQsQ0FBdUMsV0FBSSxHQTJCMUM7QUEzQlksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBDaGFuZ2VEZXRlY3RvclJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcIi4uLy4uL3BhZ2VcIjtcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnXHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQgeyBBY2NvdW50U2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9zZXJ2aWNlcy9hY2NvdW50LnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHNlbGVjdG9yOiAnYWNjb3VudC1vcmRlcnMnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdhY2NvdW50LW9yZGVycy5wYWdlLmh0bWwnLFxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEFjY291bnRPcmRlcnNQYWdlIGV4dGVuZHMgUGFnZSBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBwcml2YXRlIHN0YXJ0ID0gMFxyXG4gICAgcHJpdmF0ZSBsaW1pdCA9IDEwXHJcbiAgICBvcmRlcnMgPSBbXVxyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbiwgcHJpdmF0ZSBhY2NvdW50U2VydmljZTogQWNjb3VudFNlcnZpY2UpIHtcclxuICAgICAgICBzdXBlcihsb2NhdGlvbik7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuZ2V0T3JkZXJzKClcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0T3JkZXJzKCl7XHJcbiAgICAgICAgdGhpcy5hY2NvdW50U2VydmljZS5nZXRPcmRlcnMoXHJcbiAgICAgICAgICAgIG9yZGVycyA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9yZGVycyA9IG9yZGVyc1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cob3JkZXJzKVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcilcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdGhpcy5zdGFydCxcclxuICAgICAgICAgICAgdGhpcy5saW1pdFxyXG4gICAgICAgIClcclxuICAgIH1cclxuICAgIFxyXG59Il19