"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Page = (function () {
    function Page(_location) {
        this._location = _location;
    }
    Page.prototype.goBack = function () {
        this._location.back();
    };
    return Page;
}());
exports.Page = Page;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUVJLGNBQW9CLFNBQW1CO1FBQW5CLGNBQVMsR0FBVCxTQUFTLENBQVU7SUFDdkMsQ0FBQztJQUVELHFCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFDTCxXQUFDO0FBQUQsQ0FBQyxBQVJELElBUUM7QUFSWSxvQkFBSSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TG9jYXRpb259IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5leHBvcnQgY2xhc3MgUGFnZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfbG9jYXRpb246IExvY2F0aW9uKSB7XHJcbiAgICB9XHJcblxyXG4gICAgZ29CYWNrKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX2xvY2F0aW9uLmJhY2soKTtcclxuICAgIH1cclxufVxyXG4iXX0=