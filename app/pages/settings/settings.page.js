"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var page_1 = require("../page");
var SettingsPage = (function (_super) {
    __extends(SettingsPage, _super);
    function SettingsPage(location) {
        var _this = _super.call(this, location) || this;
        _this.location = location;
        return _this;
    }
    SettingsPage = __decorate([
        core_1.Component({
            selector: 'settings-page',
            templateUrl: 'pages/settings/settings.page.html'
        }),
        __metadata("design:paramtypes", [common_1.Location])
    ], SettingsPage);
    return SettingsPage;
}(page_1.Page));
exports.SettingsPage = SettingsPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MucGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNldHRpbmdzLnBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBd0M7QUFDeEMsMENBQXlDO0FBQ3pDLGdDQUE2QjtBQU03QjtJQUFrQyxnQ0FBSTtJQUVsQyxzQkFBb0IsUUFBa0I7UUFBdEMsWUFDSSxrQkFBTSxRQUFRLENBQUMsU0FDbEI7UUFGbUIsY0FBUSxHQUFSLFFBQVEsQ0FBVTs7SUFFdEMsQ0FBQztJQUpRLFlBQVk7UUFKeEIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxlQUFlO1lBQ3pCLFdBQVcsRUFBRSxtQ0FBbUM7U0FDbkQsQ0FBQzt5Q0FHZ0MsaUJBQVE7T0FGN0IsWUFBWSxDQUt4QjtJQUFELG1CQUFDO0NBQUEsQUFMRCxDQUFrQyxXQUFJLEdBS3JDO0FBTFksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtMb2NhdGlvbn0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQge1BhZ2V9IGZyb20gXCIuLi9wYWdlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnc2V0dGluZ3MtcGFnZScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ3BhZ2VzL3NldHRpbmdzL3NldHRpbmdzLnBhZ2UuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNldHRpbmdzUGFnZSBleHRlbmRzIFBhZ2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbG9jYXRpb246IExvY2F0aW9uKSB7XHJcbiAgICAgICAgc3VwZXIobG9jYXRpb24pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==