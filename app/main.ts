import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { AppModule } from "./app.module";
import { startMonitoring } from "tns-core-modules/connectivity";
import { alert } from "ui/dialogs";
import { exit } from 'nativescript-exit';

//Monitor internet connection, and exit in case it is lost
startMonitoring(hasInterConnection => {
    if(!hasInterConnection){
        alert("The application requrires internet connection")
            .then(()=> {
                exit()
            })
    }
})

platformNativeScriptDynamic().bootstrapModule(AppModule);
