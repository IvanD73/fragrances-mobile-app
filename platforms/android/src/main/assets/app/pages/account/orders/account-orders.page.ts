import { Component, ChangeDetectorRef } from "@angular/core";
import { OnInit } from '@angular/core';
import { Page } from "../../page";
import { AuthService } from '../../../services/auth.service'
import { RouterExtensions } from "nativescript-angular/router";
import { Location } from "@angular/common";
import { AccountService } from "../../../services/account.service";

@Component({
    moduleId: module.id,
    selector: 'account-orders',
    templateUrl: 'account-orders.page.html',
})

export class AccountOrdersPage extends Page implements OnInit {
    private start = 0
    private limit = 10
    orders = []
    
    constructor(private location: Location, private accountService: AccountService) {
        super(location);
    }
    
    ngOnInit() {
        this.getOrders()
    }

    public getOrders(){
        this.accountService.getOrders(
            orders => {
                this.orders = orders
                console.log(orders)
            },
            error => {
                console.log(error)
            },
            this.start,
            this.limit
        )
    }
    
}