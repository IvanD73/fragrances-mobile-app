import { Component,ChangeDetectorRef } from "@angular/core";
import { OnInit } from '@angular/core';
import { DrawerPage } from "../../drawer.page";
import { AuthService } from '../../../services/auth.service'
import { RouterExtensions } from "nativescript-angular/router";


@Component({
    moduleId: module.id,
    selector: 'account-dashboard',
    templateUrl: 'account-dashboard.page.html',
})

export class AccountDashboardPage extends DrawerPage implements OnInit{
    constructor( private changeDetectorRef: ChangeDetectorRef,
                 private authService:AuthService,
                 private routerExtensions: RouterExtensions
                ) {
        super(changeDetectorRef)
    }

    ngOnInit(){
        this.authService.getCustomer( customer => {
            console.log(JSON.stringify(customer))
        }, error => {
            //Error getting user, might be logged on a different device
            //Logout user and redirect to login
            this.authService.logout()
            this.routerExtensions.navigate(["/login"]);
        })
    }
}