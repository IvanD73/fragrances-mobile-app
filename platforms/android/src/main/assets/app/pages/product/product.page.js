"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var drawer_page_1 = require("../drawer.page");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var productImage_modal_component_1 = require("../../components/modal/productImage/productImage-modal.component");
var ProductPage = (function (_super) {
    __extends(ProductPage, _super);
    function ProductPage(changeDetectorRef, modal, vcRef) {
        var _this = _super.call(this, changeDetectorRef) || this;
        _this.changeDetectorRef = changeDetectorRef;
        _this.modal = modal;
        _this.vcRef = vcRef;
        return _this;
    }
    ProductPage.prototype.showProductImageModal = function () {
        var options = {
            context: {},
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(productImage_modal_component_1.ProductImageModalComponent, options).then(function (res) {
            console.log(res);
        });
    };
    ProductPage = __decorate([
        core_1.Component({
            selector: 'product-page',
            templateUrl: 'pages/product/product.page.html',
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef,
            dialogs_1.ModalDialogService,
            core_1.ViewContainerRef])
    ], ProductPage);
    return ProductPage;
}(drawer_page_1.DrawerPage));
exports.ProductPage = ProductPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdC5wYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicHJvZHVjdC5wYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQStFO0FBQy9FLDhDQUE0QztBQUM1QyxtRUFBNkU7QUFDN0UsaUhBQThHO0FBTzlHO0lBQWlDLCtCQUFVO0lBRXZDLHFCQUNTLGlCQUFvQyxFQUNwQyxLQUF3QixFQUN4QixLQUFzQjtRQUgvQixZQUtJLGtCQUFNLGlCQUFpQixDQUFDLFNBQzNCO1FBTFEsdUJBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxXQUFLLEdBQUwsS0FBSyxDQUFtQjtRQUN4QixXQUFLLEdBQUwsS0FBSyxDQUFpQjs7SUFHL0IsQ0FBQztJQUVELDJDQUFxQixHQUFyQjtRQUVJLElBQUksT0FBTyxHQUFHO1lBQ1YsT0FBTyxFQUFFLEVBQUU7WUFDWCxVQUFVLEVBQUUsSUFBSTtZQUNoQixnQkFBZ0IsRUFBRSxJQUFJLENBQUMsS0FBSztTQUMvQixDQUFDO1FBQ0YsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMseURBQTBCLEVBQUUsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsR0FBRztZQUM5RCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQXBCUSxXQUFXO1FBTHZCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsY0FBYztZQUN4QixXQUFXLEVBQUUsaUNBQWlDO1NBQ2pELENBQUM7eUNBSzhCLHdCQUFpQjtZQUM5Qiw0QkFBa0I7WUFDbEIsdUJBQWdCO09BTHRCLFdBQVcsQ0FxQnZCO0lBQUQsa0JBQUM7Q0FBQSxBQXJCRCxDQUFpQyx3QkFBVSxHQXFCMUM7QUFyQlksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIENoYW5nZURldGVjdG9yUmVmLCBWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgRHJhd2VyUGFnZSB9IGZyb20gXCIuLi9kcmF3ZXIucGFnZVwiO1xyXG5pbXBvcnQgeyBNb2RhbERpYWxvZ1NlcnZpY2UgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZGlyZWN0aXZlcy9kaWFsb2dzXCI7XHJcbmltcG9ydCB7IFByb2R1Y3RJbWFnZU1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9tb2RhbC9wcm9kdWN0SW1hZ2UvcHJvZHVjdEltYWdlLW1vZGFsLmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAncHJvZHVjdC1wYWdlJyxcclxuICAgIHRlbXBsYXRlVXJsOiAncGFnZXMvcHJvZHVjdC9wcm9kdWN0LnBhZ2UuaHRtbCcsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUHJvZHVjdFBhZ2UgZXh0ZW5kcyBEcmF3ZXJQYWdlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgIFx0cHJpdmF0ZSBjaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICBcdHByaXZhdGUgbW9kYWw6TW9kYWxEaWFsb2dTZXJ2aWNlLFxyXG4gICAgXHRwcml2YXRlIHZjUmVmOlZpZXdDb250YWluZXJSZWZcclxuICAgICl7XHJcbiAgICAgICAgc3VwZXIoY2hhbmdlRGV0ZWN0b3JSZWYpO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dQcm9kdWN0SW1hZ2VNb2RhbCgpIHtcclxuICAgIFx0XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGNvbnRleHQ6IHt9LFxyXG4gICAgICAgICAgICBmdWxsc2NyZWVuOiB0cnVlLFxyXG4gICAgICAgICAgICB2aWV3Q29udGFpbmVyUmVmOiB0aGlzLnZjUmVmXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLm1vZGFsLnNob3dNb2RhbChQcm9kdWN0SW1hZ2VNb2RhbENvbXBvbmVudCwgb3B0aW9ucykudGhlbihyZXMgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==