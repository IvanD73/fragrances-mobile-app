import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { DrawerPage } from "../drawer.page";
import { CategoriesService } from '../../services/categories.service';



@Component({
    selector: 'categories-page',
    templateUrl: 'pages/categories/categories.page.html',
    providers: [CategoriesService]
})

export class CategoriesPage extends DrawerPage implements OnInit  {
    constructor(
    	private changeDetectorRef: ChangeDetectorRef,
    	private categoriesServices: CategoriesService) {
        super(changeDetectorRef);
    }

    ngOnInit(){}



}
