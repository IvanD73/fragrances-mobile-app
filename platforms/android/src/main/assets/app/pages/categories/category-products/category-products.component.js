"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var drawer_page_1 = require("../../drawer.page");
var categories_service_1 = require("../../../services/categories.service");
var products_services_1 = require("../../../services/products.services");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var filter_modal_component_1 = require("../../../components/modal/filter/filter-modal.component");
var CategoryProducts = (function (_super) {
    __extends(CategoryProducts, _super);
    function CategoryProducts(changeDetectorRef, categoriesServices, productService, routerExtensions, modal, vcRef) {
        var _this = _super.call(this, changeDetectorRef) || this;
        _this.changeDetectorRef = changeDetectorRef;
        _this.categoriesServices = categoriesServices;
        _this.productService = productService;
        _this.routerExtensions = routerExtensions;
        _this.modal = modal;
        _this.vcRef = vcRef;
        _this.filterDropdown = false;
        _this.gridView = 'list';
        return _this;
    }
    CategoryProducts.prototype.ngOnInit = function () {
        this.products = this.productService.getProductsByCategoryId();
    };
    CategoryProducts.prototype.toggleFilterDropdown = function () {
        this.filterDropdown = !this.filterDropdown;
    };
    CategoryProducts.prototype.onFilterChange = function () {
        console.log('TODO CHANGE FILTER');
        this.filterDropdown = !this.filterDropdown;
    };
    CategoryProducts.prototype.openFilterModal = function () {
        var options = {
            context: {},
            fullscreen: false,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(filter_modal_component_1.FilterModalComponent, options).then(function (res) {
            console.log(res);
        });
    };
    CategoryProducts.prototype.onProductTap = function (product) {
        this.routerExtensions.navigate(["/product", product.id]);
    };
    CategoryProducts.prototype.changeGridView = function () {
        if (this.gridView === 'list') {
            this.gridView = 'block';
        }
        else {
            this.gridView = "list";
        }
    };
    CategoryProducts = __decorate([
        core_1.Component({
            selector: 'categories-page',
            templateUrl: './pages/categories/category-products/category-products.component.html',
            providers: [categories_service_1.CategoriesService]
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef,
            categories_service_1.CategoriesService,
            products_services_1.ProductsServices,
            router_1.RouterExtensions,
            dialogs_1.ModalDialogService,
            core_1.ViewContainerRef])
    ], CategoryProducts);
    return CategoryProducts;
}(drawer_page_1.DrawerPage));
exports.CategoryProducts = CategoryProducts;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0ZWdvcnktcHJvZHVjdHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY2F0ZWdvcnktcHJvZHVjdHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtHO0FBQ2xHLHNEQUErRDtBQUMvRCxpREFBK0M7QUFDL0MsMkVBQXlFO0FBQ3pFLHlFQUF1RTtBQUN2RSxtRUFBNkU7QUFDN0Usa0dBQStGO0FBUS9GO0lBQXNDLG9DQUFVO0lBSzVDLDBCQUNTLGlCQUFvQyxFQUNwQyxrQkFBcUMsRUFDckMsY0FBZ0MsRUFDN0IsZ0JBQWtDLEVBQ2xDLEtBQXlCLEVBQ3pCLEtBQXVCO1FBTm5DLFlBUUksa0JBQU0saUJBQWlCLENBQUMsU0FDM0I7UUFSUSx1QkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLHdCQUFrQixHQUFsQixrQkFBa0IsQ0FBbUI7UUFDckMsb0JBQWMsR0FBZCxjQUFjLENBQWtCO1FBQzdCLHNCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsV0FBSyxHQUFMLEtBQUssQ0FBb0I7UUFDekIsV0FBSyxHQUFMLEtBQUssQ0FBa0I7UUFWdEMsb0JBQWMsR0FBVyxLQUFLLENBQUM7UUFFNUIsY0FBUSxHQUFVLE1BQU0sQ0FBQzs7SUFXekIsQ0FBQztJQUVELG1DQUFRLEdBQVI7UUFDQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztJQUMvRCxDQUFDO0lBRUQsK0NBQW9CLEdBQXBCO1FBQ0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDNUMsQ0FBQztJQUVELHlDQUFjLEdBQWQ7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUE7UUFDakMsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0MsQ0FBQztJQUVELDBDQUFlLEdBQWY7UUFDSSxJQUFJLE9BQU8sR0FBRztZQUNWLE9BQU8sRUFBRSxFQUFFO1lBQ1gsVUFBVSxFQUFFLEtBQUs7WUFDakIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDL0IsQ0FBQztRQUNGLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLDZDQUFvQixFQUFFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7WUFDeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx1Q0FBWSxHQUFaLFVBQWEsT0FBTztRQUNoQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxFQUFHLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCx5Q0FBYyxHQUFkO1FBQ0ksRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLFFBQVEsS0FBSyxNQUFNLENBQUMsQ0FBQSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1FBQzVCLENBQUM7UUFBQSxJQUFJLENBQUEsQ0FBQztZQUNGLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDO1FBQzNCLENBQUM7SUFDTCxDQUFDO0lBbERRLGdCQUFnQjtRQU41QixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixXQUFXLEVBQUUsdUVBQXVFO1lBQ3BGLFNBQVMsRUFBQyxDQUFDLHNDQUFpQixDQUFDO1NBQ2hDLENBQUM7eUNBUThCLHdCQUFpQjtZQUNoQixzQ0FBaUI7WUFDckIsb0NBQWdCO1lBQ1gseUJBQWdCO1lBQzNCLDRCQUFrQjtZQUNsQix1QkFBZ0I7T0FYMUIsZ0JBQWdCLENBcUQ1QjtJQUFELHVCQUFDO0NBQUEsQUFyREQsQ0FBc0Msd0JBQVUsR0FxRC9DO0FBckRZLDRDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBDaGFuZ2VEZXRlY3RvclJlZiwgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgRHJhd2VyUGFnZSB9IGZyb20gXCIuLi8uLi9kcmF3ZXIucGFnZVwiO1xyXG5pbXBvcnQgeyBDYXRlZ29yaWVzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL2NhdGVnb3JpZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IFByb2R1Y3RzU2VydmljZXMgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9wcm9kdWN0cy5zZXJ2aWNlcyc7XHJcbmltcG9ydCB7IE1vZGFsRGlhbG9nU2VydmljZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9kaXJlY3RpdmVzL2RpYWxvZ3NcIjtcclxuaW1wb3J0IHsgRmlsdGVyTW9kYWxDb21wb25lbnQgfSBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL21vZGFsL2ZpbHRlci9maWx0ZXItbW9kYWwuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdjYXRlZ29yaWVzLXBhZ2UnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3BhZ2VzL2NhdGVnb3JpZXMvY2F0ZWdvcnktcHJvZHVjdHMvY2F0ZWdvcnktcHJvZHVjdHMuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgcHJvdmlkZXJzOltDYXRlZ29yaWVzU2VydmljZV1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXRlZ29yeVByb2R1Y3RzIGV4dGVuZHMgRHJhd2VyUGFnZSBpbXBsZW1lbnRzIE9uSW5pdCAge1xyXG5cdGZpbHRlckRyb3Bkb3duOmJvb2xlYW4gPSBmYWxzZTtcclxuXHRwcm9kdWN0cztcclxuICAgIGdyaWRWaWV3OnN0cmluZyA9ICdsaXN0JztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgIFx0cHJpdmF0ZSBjaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICBcdHByaXZhdGUgY2F0ZWdvcmllc1NlcnZpY2VzOiBDYXRlZ29yaWVzU2VydmljZSxcclxuICAgIFx0cHJpdmF0ZSBwcm9kdWN0U2VydmljZTogUHJvZHVjdHNTZXJ2aWNlcyxcclxuICAgICAgICBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsXHJcbiAgICAgICAgcHJpdmF0ZSBtb2RhbDogTW9kYWxEaWFsb2dTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgdmNSZWY6IFZpZXdDb250YWluZXJSZWYsXHJcbiAgICAgICAgKXtcclxuICAgICAgICBzdXBlcihjaGFuZ2VEZXRlY3RvclJlZik7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKXtcclxuICAgIFx0dGhpcy5wcm9kdWN0cyA9IHRoaXMucHJvZHVjdFNlcnZpY2UuZ2V0UHJvZHVjdHNCeUNhdGVnb3J5SWQoKTtcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVGaWx0ZXJEcm9wZG93bigpe1xyXG4gICAgXHR0aGlzLmZpbHRlckRyb3Bkb3duID0gIXRoaXMuZmlsdGVyRHJvcGRvd247XHJcbiAgICB9XHJcblxyXG4gICAgb25GaWx0ZXJDaGFuZ2UoKXtcclxuICAgICAgICBjb25zb2xlLmxvZygnVE9ETyBDSEFOR0UgRklMVEVSJylcclxuICAgICAgICB0aGlzLmZpbHRlckRyb3Bkb3duID0gIXRoaXMuZmlsdGVyRHJvcGRvd247XHJcbiAgICB9XHJcblxyXG4gICAgb3BlbkZpbHRlck1vZGFsKCl7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGNvbnRleHQ6IHt9LFxyXG4gICAgICAgICAgICBmdWxsc2NyZWVuOiBmYWxzZSxcclxuICAgICAgICAgICAgdmlld0NvbnRhaW5lclJlZjogdGhpcy52Y1JlZlxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5tb2RhbC5zaG93TW9kYWwoRmlsdGVyTW9kYWxDb21wb25lbnQsIG9wdGlvbnMpLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cocmVzKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvblByb2R1Y3RUYXAocHJvZHVjdCl7XHJcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9wcm9kdWN0XCIsICBwcm9kdWN0LmlkXSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlR3JpZFZpZXcoKXtcclxuICAgICAgICBpZih0aGlzLmdyaWRWaWV3ID09PSAnbGlzdCcpe1xyXG4gICAgICAgICAgICB0aGlzLmdyaWRWaWV3ID0gJ2Jsb2NrJztcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgdGhpcy5ncmlkVmlldyA9IFwibGlzdFwiO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG59XHJcbiJdfQ==