import { Component, OnInit, AfterViewInit } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: 'slider-component',
    templateUrl: 'slider.component.html'
})

export class SliderComponent implements AfterViewInit{
    elements: Array<any> = [];

    ngOnInit(){
        this.elements = [
            {image: '~/images/slider/banner-1.jpg'},
            {image: '~/images/slider/banner-2.jpg'},
            {image: '~/images/slider/banner-1.jpg'}
        ]
    }

    onSlideTap(slide){
        console.log(JSON.stringify(slide))
    }

    ngAfterViewInit() {
        //Automatcally start the slider
        this.autostartSlider()
    }

    onSlideChanged(){

    }
    
    onTap(element){
        //Slider Tapped
    }

    autostartSlider(interval: number = 10000) {
        
    }
}