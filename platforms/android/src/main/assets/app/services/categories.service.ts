export class CategoriesService{

	getCategories(){
		return [
			{id:1, name:'Category 1', subcategories:true},
			{id:2, name:'Category 2', subcategories:true},
			{id:3, name:'Category 3', subcategories:true},
			{id:4, name:'Category 4'},
			{id:5, name:'Category 5'},
			{id:5, name:'Category 6'},
			{id:5, name:'Category 7'},
			{id:5, name:'Category 8'},
			{id:5, name:'Category 9'},
			{id:5, name:'Category 10'}
		]
	}

	getSubcategoriesByParentId(id){
		return [
			{id:22, name:'Subcat Name 1'},
			{id:23, name:'Subcat Name 2'},
			{id:24, name:'Subcat Name 3'},
			{id:25, name:'Subcat Name 4'},
			{id:26, name:'Subcat Name 5'},
			{id:27, name:'Subcat Name 6'}
				
		]
	}

}