import { Component } from "@angular/core";
import { AuthService } from '../../services/auth.service'
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    moduleId: module.id,
    selector: 'logout-component',
    templateUrl: 'logout.component.html',
})

export class LogoutComponent{
    constructor(private authService: AuthService, private routerExtensions: RouterExtensions,){
        this.authService.logout()
        this.routerExtensions.navigate(['/login'])
    }
    
    
}
