import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/directives/dialogs";

@Component({
    moduleId: module.id,
    selector: "address-modal",
    templateUrl: "address-modal.component.html",
})
export class AddressModalComponent {
    public constructor(private params: ModalDialogParams) {
        
    }
    
    public close(res: string) {
        this.params.closeCallback();
    }

}