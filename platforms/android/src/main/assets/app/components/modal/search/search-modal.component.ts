import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/directives/dialogs";

@Component({
    selector: "search-modal",
    templateUrl: "components/modal/search/search-modal.component.html",
})
export class SearchModalComponent {

    public constructor(private params: ModalDialogParams) {
        
    }

    public close(res: string) {
        this.params.closeCallback(res);
    }

}