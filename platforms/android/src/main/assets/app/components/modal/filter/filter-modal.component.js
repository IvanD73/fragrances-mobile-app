"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var filter_service_1 = require("../../../services/filter.service");
var FilterModalComponent = (function () {
    function FilterModalComponent(params, filterService) {
        this.params = params;
        this.filterService = filterService;
        this.currentFilters = [
            { manufacturers: [] },
            { price: "10лв. - 25лв." },
            { aromaticGroups: [] },
            { moreOptions: [] }
        ];
        this.manufacturers = [];
        this.aromaticGroups = [];
        this.showFilterOptions = false;
        this.showingFilterHeading = 'Филтрирай';
        this.showingFilterName = '';
        this.manufacturers = this.filterService.getManufacturers();
        this.aromaticGroups = this.filterService.getАromaticGroups();
    }
    FilterModalComponent.prototype.close = function (res) {
        this.params.closeCallback(res);
    };
    FilterModalComponent.prototype.hideOptions = function () {
        this.showFilterOptions = false;
        this.showingFilterName = '';
        this.showingFilterHeading = 'Филтрирай';
    };
    FilterModalComponent.prototype.displayFilterOptions = function (filterName) {
        this.showFilterOptions = !this.showFilterOptions;
        this.showingFilterName = filterName;
        this.setFilterHeadingName(filterName);
    };
    FilterModalComponent.prototype.setFilterHeadingName = function (filterName) {
        switch (filterName) {
            case 'manufacturers': {
                this.showingFilterHeading = 'Марки';
                break;
            }
            case 'price': {
                this.showingFilterHeading = 'Цени';
                break;
            }
            case 'aromatic-groups': {
                this.showingFilterHeading = 'Ароматни групи';
                break;
            }
            case 'more': {
                this.showingFilterHeading = 'Още';
                break;
            }
            default: {
                this.showingFilterHeading = 'Филтрирай';
                break;
            }
        }
    };
    FilterModalComponent = __decorate([
        core_1.Component({
            selector: "filter-modal",
            templateUrl: "components/modal/filter/filter-modal.component.html",
        }),
        __metadata("design:paramtypes", [dialogs_1.ModalDialogParams,
            filter_service_1.FilterService])
    ], FilterModalComponent);
    return FilterModalComponent;
}());
exports.FilterModalComponent = FilterModalComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZpbHRlci1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBcUQ7QUFDckQsbUVBQTRFO0FBQzVFLG1FQUFpRTtBQU1qRTtJQWNJLDhCQUNTLE1BQXlCLEVBQ3pCLGFBQTRCO1FBRDVCLFdBQU0sR0FBTixNQUFNLENBQW1CO1FBQ3pCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBZnhDLG1CQUFjLEdBQUc7WUFDaEIsRUFBQyxhQUFhLEVBQUMsRUFBRSxFQUFDO1lBQ2xCLEVBQUMsS0FBSyxFQUFDLGVBQWUsRUFBQztZQUN2QixFQUFDLGNBQWMsRUFBQyxFQUFFLEVBQUM7WUFDbkIsRUFBQyxXQUFXLEVBQUMsRUFBRSxFQUFDO1NBQ2hCLENBQUE7UUFFRCxrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNwQixzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDMUIseUJBQW9CLEdBQUcsV0FBVyxDQUFDO1FBQ25DLHNCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUtoQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUMzRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUVqRSxDQUFDO0lBRU0sb0NBQUssR0FBWixVQUFhLEdBQVc7UUFDcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELDBDQUFXLEdBQVg7UUFDQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFdBQVcsQ0FBQztJQUN6QyxDQUFDO0lBRUQsbURBQW9CLEdBQXBCLFVBQXFCLFVBQVU7UUFDOUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ2pELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxVQUFVLENBQUM7UUFDcEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxtREFBb0IsR0FBcEIsVUFBcUIsVUFBVTtRQUM5QixNQUFNLENBQUEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLEtBQUssZUFBZSxFQUFFLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxPQUFPLENBQUM7Z0JBQ3BDLEtBQUssQ0FBQztZQUNULENBQUM7WUFDRCxLQUFLLE9BQU8sRUFBRSxDQUFDO2dCQUNaLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxNQUFNLENBQUM7Z0JBQ25DLEtBQUssQ0FBQztZQUNULENBQUM7WUFDRCxLQUFLLGlCQUFpQixFQUFFLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxnQkFBZ0IsQ0FBQztnQkFDN0MsS0FBSyxDQUFDO1lBQ1QsQ0FBQztZQUNELEtBQUssTUFBTSxFQUFFLENBQUM7Z0JBQ1gsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztnQkFDbEMsS0FBSyxDQUFDO1lBQ1QsQ0FBQztZQUNELFNBQVMsQ0FBQztnQkFDUCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsV0FBVyxDQUFDO2dCQUN4QyxLQUFLLENBQUM7WUFDVCxDQUFDO1FBQ0osQ0FBQztJQUNDLENBQUM7SUE3RFEsb0JBQW9CO1FBSmhDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsY0FBYztZQUN4QixXQUFXLEVBQUUscURBQXFEO1NBQ3JFLENBQUM7eUNBZ0JtQiwyQkFBaUI7WUFDViw4QkFBYTtPQWhCNUIsb0JBQW9CLENBK0RoQztJQUFELDJCQUFDO0NBQUEsQUEvREQsSUErREM7QUEvRFksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBNb2RhbERpYWxvZ1BhcmFtcyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9kaXJlY3RpdmVzL2RpYWxvZ3NcIjtcclxuaW1wb3J0IHsgRmlsdGVyU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL2ZpbHRlci5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwiZmlsdGVyLW1vZGFsXCIsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCJjb21wb25lbnRzL21vZGFsL2ZpbHRlci9maWx0ZXItbW9kYWwuY29tcG9uZW50Lmh0bWxcIixcclxufSlcclxuZXhwb3J0IGNsYXNzIEZpbHRlck1vZGFsQ29tcG9uZW50IHtcclxuXHRjdXJyZW50RmlsdGVycyA9IFtcclxuXHRcdHttYW51ZmFjdHVyZXJzOltdfSxcclxuXHRcdHtwcmljZTpcIjEw0LvQsi4gLSAyNdC70LIuXCJ9LFxyXG5cdFx0e2Fyb21hdGljR3JvdXBzOltdfSxcclxuXHRcdHttb3JlT3B0aW9uczpbXX1cclxuXHRdXHJcblxyXG5cdG1hbnVmYWN0dXJlcnMgPSBbXTtcclxuXHRhcm9tYXRpY0dyb3VwcyA9IFtdO1xyXG5cdHNob3dGaWx0ZXJPcHRpb25zID0gZmFsc2U7XHJcblx0c2hvd2luZ0ZpbHRlckhlYWRpbmcgPSAn0KTQuNC70YLRgNC40YDQsNC5JztcclxuXHRzaG93aW5nRmlsdGVyTmFtZSA9ICcnO1xyXG5cdFxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKFxyXG4gICAgXHRwcml2YXRlIHBhcmFtczogTW9kYWxEaWFsb2dQYXJhbXMsXHJcbiAgICBcdHByaXZhdGUgZmlsdGVyU2VydmljZTogRmlsdGVyU2VydmljZSwpe1xyXG4gICAgICAgIHRoaXMubWFudWZhY3R1cmVycyA9IHRoaXMuZmlsdGVyU2VydmljZS5nZXRNYW51ZmFjdHVyZXJzKCk7XHJcbiAgICAgICAgdGhpcy5hcm9tYXRpY0dyb3VwcyA9IHRoaXMuZmlsdGVyU2VydmljZS5nZXTQkHJvbWF0aWNHcm91cHMoKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNsb3NlKHJlczogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5wYXJhbXMuY2xvc2VDYWxsYmFjayhyZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIGhpZGVPcHRpb25zKCl7XHJcbiAgICBcdHRoaXMuc2hvd0ZpbHRlck9wdGlvbnMgPSBmYWxzZTtcclxuICAgIFx0dGhpcy5zaG93aW5nRmlsdGVyTmFtZSA9ICcnO1xyXG4gICAgXHR0aGlzLnNob3dpbmdGaWx0ZXJIZWFkaW5nID0gJ9Ck0LjQu9GC0YDQuNGA0LDQuSc7XHJcbiAgICB9XHJcblxyXG4gICBcdGRpc3BsYXlGaWx0ZXJPcHRpb25zKGZpbHRlck5hbWUpe1xyXG4gICBcdFx0dGhpcy5zaG93RmlsdGVyT3B0aW9ucyA9ICF0aGlzLnNob3dGaWx0ZXJPcHRpb25zO1xyXG4gICBcdFx0dGhpcy5zaG93aW5nRmlsdGVyTmFtZSA9IGZpbHRlck5hbWU7XHJcbiAgIFx0XHR0aGlzLnNldEZpbHRlckhlYWRpbmdOYW1lKGZpbHRlck5hbWUpO1x0XHJcbiAgIFx0fVxyXG5cclxuICAgXHRzZXRGaWx0ZXJIZWFkaW5nTmFtZShmaWx0ZXJOYW1lKXtcclxuICAgXHRcdHN3aXRjaChmaWx0ZXJOYW1lKSB7IFxyXG5cdFx0ICAgY2FzZSAnbWFudWZhY3R1cmVycyc6IHtcclxuXHRcdCAgICAgIHRoaXMuc2hvd2luZ0ZpbHRlckhlYWRpbmcgPSAn0JzQsNGA0LrQuCc7IFxyXG5cdFx0ICAgICAgYnJlYWs7IFxyXG5cdFx0ICAgfSBcclxuXHRcdCAgIGNhc2UgJ3ByaWNlJzogeyBcclxuXHRcdCAgICAgIHRoaXMuc2hvd2luZ0ZpbHRlckhlYWRpbmcgPSAn0KbQtdC90LgnOyBcclxuXHRcdCAgICAgIGJyZWFrOyBcclxuXHRcdCAgIH1cclxuXHRcdCAgIGNhc2UgJ2Fyb21hdGljLWdyb3Vwcyc6IHsgXHJcblx0XHQgICAgICB0aGlzLnNob3dpbmdGaWx0ZXJIZWFkaW5nID0gJ9CQ0YDQvtC80LDRgtC90Lgg0LPRgNGD0L/QuCc7IFxyXG5cdFx0ICAgICAgYnJlYWs7IFxyXG5cdFx0ICAgfVxyXG5cdFx0ICAgY2FzZSAnbW9yZSc6IHsgXHJcblx0XHQgICAgICB0aGlzLnNob3dpbmdGaWx0ZXJIZWFkaW5nID0gJ9Ce0YnQtSc7IFxyXG5cdFx0ICAgICAgYnJlYWs7IFxyXG5cdFx0ICAgfVxyXG5cdFx0ICAgZGVmYXVsdDogeyBcclxuXHRcdCAgICAgIHRoaXMuc2hvd2luZ0ZpbHRlckhlYWRpbmcgPSAn0KTQuNC70YLRgNC40YDQsNC5JzsgXHJcblx0XHQgICAgICBicmVhazsgXHJcblx0XHQgICB9IFxyXG5cdFx0fSBcclxuICAgXHR9XHJcblxyXG59Il19