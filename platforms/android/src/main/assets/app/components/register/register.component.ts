import { Component } from "@angular/core";
import { ValidationService } from '../../services/validation.service'
import {AuthService} from '../../services/auth.service'
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: 'register-component',
    templateUrl: 'components/register/register.component.html',
})

export class RegisterComponent{
    //fields
    firstname = ''
    lastname = ''
    password = ''
    confirm = ''
    email = ''
    telephone = ''
    address_1 = ''
    city = ''
    country_id = '55'
    zone_id = '1'
    newsletter = '1'

    //errors
    error_firstname = ''
    error_lastname = ''
    error_password = ''
    error_confirm = ''
    error_email = ''
    error_telephone = ''
    error_address_1 = ''
    error_city = ''
    error_country_id = ''
    error_zone_id = ''
    error_register = ''

    constructor(private authService: AuthService, private routerExtensions: RouterExtensions){
        
    }

    register(){
        if(this.validateRegisterCredentials()){
            //Create customer object
            let customer = {
                firstname: this.firstname,
                lastname: this.lastname,
                password: this.password,
                email: this.email,
                telephone: this.telephone,
                address_1: this.address_1,
                city: this.city,
                country_id: this.country_id,
                zone_id: this.zone_id,
                newsletter: this.newsletter
            }
            this.authService.register(customer,
                () => {
                    this.routerExtensions.navigate(['/account']);
                },
                error => {
                    this.error_register = error
                }
            )
        }
    }

    validateRegisterCredentials(){
        this.resetErrors()

        if(!ValidationService.isValidFirstname(this.firstname)){
            this.error_firstname = 'Invalid firstname'
        }

        if(!ValidationService.isValidLastname(this.lastname)){
            this.error_lastname = 'Invalid lastname'
        }

        if(!ValidationService.isValidEmail(this.email)){
            this.error_email = 'Invalid email'
        }

        if(!ValidationService.isValidTelephone(this.telephone)){
            this.error_telephone = 'Invalid telehone'
        }

        if(!ValidationService.isValidCity(this.city)){
            this.error_city = 'Invalid city'
        }

        if(!ValidationService.isValidCountryId(this.country_id)){
            this.error_country_id = 'Invalid country'
        }

        if(!ValidationService.isValidZoneId(this.zone_id)){
            this.error_zone_id = 'invalid zone id'
        }

        if(!ValidationService.isValidAddress(this.address_1)){
            this.error_address_1 = 'Invalid address'
        }

        if(!ValidationService.isValidPassword(this.password)){
            this.error_password = 'Invalid password'
        }

        if(!ValidationService.isValidConfirm(this.password, this.confirm)){
            this.error_confirm = 'Invalid confirm'
        }

        return this.hasErrors()
    }

    private resetErrors(){
        this.error_firstname  = ''
        this.error_lastname = ''
        this.error_password = ''
        this.error_confirm = ''
        this.error_email = ''
        this.error_telephone = ''
        this.error_address_1 = ''
        this.error_city = ''
        this.error_country_id = ''
        this.error_zone_id = ''
        this.error_register = '' 
    }

    private hasErrors(){
        return  !this.error_firstname &&
                !this.error_lastname &&
                !this.error_email &&
                !this.error_telephone &&
                !this.error_address_1 &&
                !this.error_city &&
                !this.error_country_id &&
                !this.error_zone_id &&
                !this.error_confirm
    }
}
