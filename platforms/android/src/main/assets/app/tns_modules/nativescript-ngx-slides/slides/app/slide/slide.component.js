"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SlideComponent = (function () {
    function SlideComponent() {
        this.cssClass = '';
        this.tap = new core_1.EventEmitter();
        this.cssClass = this.cssClass ? this.cssClass : '';
    }
    Object.defineProperty(SlideComponent.prototype, "slideWidth", {
        set: function (width) {
            this.layout.width = width;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SlideComponent.prototype, "slideHeight", {
        set: function (height) {
            this.layout.height = height;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SlideComponent.prototype, "layout", {
        get: function () {
            return this.slideLayout.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    SlideComponent.prototype.ngOnInit = function () {
    };
    SlideComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.slideLayout.nativeElement.on('tap', function (args) {
            _this.tap.next(args);
        });
    };
    __decorate([
        core_1.ViewChild('slideLayout'),
        __metadata("design:type", core_1.ElementRef)
    ], SlideComponent.prototype, "slideLayout", void 0);
    __decorate([
        core_1.Input('class'),
        __metadata("design:type", String)
    ], SlideComponent.prototype, "cssClass", void 0);
    __decorate([
        core_1.Output('tap'),
        __metadata("design:type", Object)
    ], SlideComponent.prototype, "tap", void 0);
    SlideComponent = __decorate([
        core_1.Component({
            selector: 'slide',
            template: "\n\t<StackLayout #slideLayout [class]=\"cssClass\">\n\t\t<ng-content></ng-content>\n\t</StackLayout>\n\t",
        }),
        __metadata("design:paramtypes", [])
    ], SlideComponent);
    return SlideComponent;
}());
exports.SlideComponent = SlideComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2xpZGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXNHO0FBY3RHO0lBa0JDO1FBaEJnQixhQUFRLEdBQVcsRUFBRSxDQUFDO1FBQ3ZCLFFBQUcsR0FBRyxJQUFJLG1CQUFZLEVBQTZCLENBQUM7UUFnQmxFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztJQUNwRCxDQUFDO0lBZkQsc0JBQUksc0NBQVU7YUFBZCxVQUFlLEtBQWE7WUFDM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQzNCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksdUNBQVc7YUFBZixVQUFnQixNQUF1QjtZQUN0QyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBUSxNQUFNLENBQUM7UUFDbEMsQ0FBQzs7O09BQUE7SUFHRCxzQkFBSSxrQ0FBTTthQUFWO1lBQ0MsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFBO1FBQ3RDLENBQUM7OztPQUFBO0lBTUQsaUNBQVEsR0FBUjtJQUNBLENBQUM7SUFFRCx3Q0FBZSxHQUFmO1FBQUEsaUJBSUM7UUFIQSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLFVBQUMsSUFBK0I7WUFDeEUsS0FBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBNUJ5QjtRQUF6QixnQkFBUyxDQUFDLGFBQWEsQ0FBQztrQ0FBYyxpQkFBVTt1REFBQztJQUNsQztRQUFmLFlBQUssQ0FBQyxPQUFPLENBQUM7O29EQUF1QjtJQUN2QjtRQUFkLGFBQU0sQ0FBQyxLQUFLLENBQUM7OytDQUFxRDtJQUh2RCxjQUFjO1FBVDFCLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsT0FBTztZQUNqQixRQUFRLEVBQUUsMEdBSVQ7U0FDRCxDQUFDOztPQUVXLGNBQWMsQ0ErQjFCO0lBQUQscUJBQUM7Q0FBQSxBQS9CRCxJQStCQztBQS9CWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgVmlld0NoaWxkLCBFbGVtZW50UmVmLCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdGFja0xheW91dCB9IGZyb20gJ3VpL2xheW91dHMvc3RhY2stbGF5b3V0JztcclxuaW1wb3J0ICogYXMgZ2VzdHVyZXMgZnJvbSAndWkvZ2VzdHVyZXMnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdHNlbGVjdG9yOiAnc2xpZGUnLFxyXG5cdHRlbXBsYXRlOiBgXHJcblx0PFN0YWNrTGF5b3V0ICNzbGlkZUxheW91dCBbY2xhc3NdPVwiY3NzQ2xhc3NcIj5cclxuXHRcdDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuXHQ8L1N0YWNrTGF5b3V0PlxyXG5cdGAsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2xpZGVDb21wb25lbnQge1xyXG5cdEBWaWV3Q2hpbGQoJ3NsaWRlTGF5b3V0Jykgc2xpZGVMYXlvdXQ6IEVsZW1lbnRSZWY7XHJcblx0QElucHV0KCdjbGFzcycpIGNzc0NsYXNzOiBzdHJpbmcgPSAnJztcclxuXHRAT3V0cHV0KCd0YXAnKSB0YXAgPSBuZXcgRXZlbnRFbWl0dGVyPGdlc3R1cmVzLkdlc3R1cmVFdmVudERhdGE+KCk7XHJcblxyXG5cdHNldCBzbGlkZVdpZHRoKHdpZHRoOiBudW1iZXIpIHtcclxuXHRcdHRoaXMubGF5b3V0LndpZHRoID0gd2lkdGg7XHJcblx0fVxyXG5cclxuXHRzZXQgc2xpZGVIZWlnaHQoaGVpZ2h0OiBudW1iZXIgfCBzdHJpbmcpIHtcclxuXHRcdHRoaXMubGF5b3V0LmhlaWdodCA9IDxhbnk+aGVpZ2h0O1xyXG5cdH1cclxuXHJcblxyXG5cdGdldCBsYXlvdXQoKTogU3RhY2tMYXlvdXQge1xyXG5cdFx0cmV0dXJuIHRoaXMuc2xpZGVMYXlvdXQubmF0aXZlRWxlbWVudFxyXG5cdH1cclxuXHJcblx0Y29uc3RydWN0b3IoKSB7XHJcblx0XHR0aGlzLmNzc0NsYXNzID0gdGhpcy5jc3NDbGFzcyA/IHRoaXMuY3NzQ2xhc3MgOiAnJztcclxuXHR9XHJcblxyXG5cdG5nT25Jbml0KCkge1xyXG5cdH1cclxuXHJcblx0bmdBZnRlclZpZXdJbml0KCl7XHJcblx0XHR0aGlzLnNsaWRlTGF5b3V0Lm5hdGl2ZUVsZW1lbnQub24oJ3RhcCcsIChhcmdzOiBnZXN0dXJlcy5HZXN0dXJlRXZlbnREYXRhKTogdm9pZCA9PiB7XHJcblx0XHRcdHRoaXMudGFwLm5leHQoYXJncyk7XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG59XHJcbiJdfQ==