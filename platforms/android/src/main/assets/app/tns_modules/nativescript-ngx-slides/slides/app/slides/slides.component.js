"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var slide_component_1 = require("../slide/slide.component");
var gestures = require("ui/gestures");
var platform = require("platform");
var AnimationModule = require("ui/animation");
var enums_1 = require("ui/enums");
var app = require("application");
var absolute_layout_1 = require("ui/layouts/absolute-layout");
var stack_layout_1 = require("ui/layouts/stack-layout");
var direction;
(function (direction) {
    direction[direction["none"] = 0] = "none";
    direction[direction["left"] = 1] = "left";
    direction[direction["right"] = 2] = "right";
})(direction || (direction = {}));
var cancellationReason;
(function (cancellationReason) {
    cancellationReason[cancellationReason["user"] = 0] = "user";
    cancellationReason[cancellationReason["noPrevSlides"] = 1] = "noPrevSlides";
    cancellationReason[cancellationReason["noMoreSlides"] = 2] = "noMoreSlides";
})(cancellationReason || (cancellationReason = {}));
var SlidesComponent = (function () {
    function SlidesComponent(ref) {
        this.ref = ref;
        this.cssClass = '';
        this.changed = new core_1.EventEmitter();
        this.finished = new core_1.EventEmitter();
        this.tap = new core_1.EventEmitter();
        this.direction = direction.none;
        this.FOOTER_HEIGHT = 50;
        this.indicators = [];
    }
    Object.defineProperty(SlidesComponent.prototype, "hasNext", {
        get: function () {
            return !!this.currentSlide && !!this.currentSlide.right;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SlidesComponent.prototype, "hasPrevious", {
        get: function () {
            return !!this.currentSlide && !!this.currentSlide.left;
        },
        enumerable: true,
        configurable: true
    });
    SlidesComponent.prototype.ngOnInit = function () {
        this.loop = this.loop ? this.loop : false;
        this.pageIndicators = this.pageIndicators ? this.pageIndicators : false;
        this.pageWidth = this.pageWidth ? this.pageWidth : platform.screen.mainScreen.widthDIPs;
        this.pageHeight = this.pageHeight ? this.pageHeight : platform.screen.mainScreen.heightDIPs;
        this.footerMarginTop = this.footerMarginTop ? this.footerMarginTop : this.calculateFoorterMarginTop(this.pageHeight);
        // handle orientation change
        app.on(app.orientationChangedEvent, this.onOrientationChanged, this);
    };
    SlidesComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // loop through slides and setup height and widith
        this.slides.forEach(function (slide) {
            absolute_layout_1.AbsoluteLayout.setLeft(slide.layout, _this.pageWidth);
            slide.slideWidth = _this.pageWidth;
            slide.slideHeight = _this.pageHeight;
        });
        this.currentSlide = this.buildSlideMap(this.slides.toArray());
        if (this.currentSlide) {
            this.positionSlides(this.currentSlide);
            this.applySwipe(this.pageWidth);
        }
        if (this.pageIndicators) {
            this.buildFooter(this.slides.length);
            this.setActivePageIndicator(0);
        }
    };
    SlidesComponent.prototype.ngOnDestroy = function () {
        app.off(app.orientationChangedEvent, this.onOrientationChanged, this);
    };
    SlidesComponent.prototype.onOrientationChanged = function (args) {
        var _this = this;
        // event and page orientation didn't seem to always be on the same page so
        // setting it in the time out addresses this.
        setTimeout(function () {
            // the values are either 'landscape' or 'portrait'
            // platform.screen.mainScreen.heightDIPs/widthDIPs holds original screen size
            if (args.newValue === 'landscape') {
                _this.pageWidth = (app.android) ?
                    platform.screen.mainScreen.heightDIPs : platform.screen.mainScreen.widthDIPs;
                _this.pageHeight = (app.android) ?
                    platform.screen.mainScreen.widthDIPs : platform.screen.mainScreen.heightDIPs;
            }
            else {
                _this.pageWidth = platform.screen.mainScreen.widthDIPs;
                _this.pageHeight = platform.screen.mainScreen.heightDIPs;
            }
            _this.footerMarginTop = _this.calculateFoorterMarginTop(_this.pageHeight);
            // loop through slides and setup height and widith
            _this.slides.forEach(function (slide) {
                absolute_layout_1.AbsoluteLayout.setLeft(slide.layout, _this.pageWidth);
                slide.slideWidth = _this.pageWidth;
                slide.slideHeight = _this.pageHeight;
                slide.layout.eachLayoutChild(function (view) {
                    if (view instanceof stack_layout_1.StackLayout) {
                        absolute_layout_1.AbsoluteLayout.setLeft(view, _this.pageWidth);
                        view.width = _this.pageWidth;
                        view.height = _this.pageHeight;
                    }
                });
            });
            if (_this.currentSlide) {
                _this.positionSlides(_this.currentSlide);
            }
            if (_this.pageIndicators) {
                _this.buildFooter(_this.slides.length);
            }
        }, 17); // one frame @ 60 frames/s, no flicker
    };
    SlidesComponent.prototype.setActivePageIndicator = function (activeIndex) {
        this.indicators.map(function (indicator, index) {
            if (index === activeIndex) {
                indicator.active = true;
            }
            else {
                indicator.active = false;
            }
        });
        this.indicators = this.indicators.slice();
        this.ref.detectChanges();
    };
    //
    // private  functions
    //
    // position footer
    SlidesComponent.prototype.calculateFoorterMarginTop = function (pageHeight) {
        return pageHeight - (pageHeight / 6);
    };
    // footer stuff
    SlidesComponent.prototype.buildFooter = function (pageCount) {
        if (pageCount === void 0) { pageCount = 5; }
        var footerSection = this.footer.nativeElement;
        footerSection.horizontalAlignment = 'center';
        footerSection.orientation = 'horizontal';
        footerSection.marginTop = this.footerMarginTop;
        footerSection.height = this.FOOTER_HEIGHT;
        footerSection.width = this.pageWidth;
        if (app.ios) {
            footerSection.clipToBounds = false;
        }
        var index = 0;
        this.indicators = [];
        while (index < pageCount) {
            this.indicators.push({ active: false });
            index++;
        }
    };
    SlidesComponent.prototype.setupPanel = function (slide) {
        this.direction = direction.none;
        this.transitioning = false;
        this.currentSlide.slide.layout.off('pan');
        this.currentSlide = slide;
        // sets up each slide so that they are positioned to transition either way.
        this.positionSlides(this.currentSlide);
        //if (this.disablePan === false) {
        this.applySwipe(this.pageWidth);
        //}
        if (this.pageIndicators) {
            this.setActivePageIndicator(this.currentSlide.index);
        }
        this.changed.next(this.currentSlide.index);
        if (this.currentSlide.index === this.slides.length - 1) {
            this.finished.next(null);
        }
    };
    SlidesComponent.prototype.positionSlides = function (slide) {
        // sets up each slide so that they are positioned to transition either way.
        if (slide.left != null && slide.left.slide != null) {
            slide.left.slide.layout.translateX = -this.pageWidth * 2;
        }
        slide.slide.layout.translateX = -this.pageWidth;
        if (slide.right != null && slide.right.slide != null) {
            slide.right.slide.layout.translateX = 0;
        }
    };
    SlidesComponent.prototype.showRightSlide = function (slideMap, offset, endingVelocity, duration) {
        if (offset === void 0) { offset = this.pageWidth; }
        if (endingVelocity === void 0) { endingVelocity = 32; }
        if (duration === void 0) { duration = 300; }
        var animationDuration;
        animationDuration = duration; // default value
        var transition = new Array();
        transition.push({
            target: slideMap.right.slide.layout,
            translate: { x: -this.pageWidth, y: 0 },
            duration: animationDuration,
            curve: enums_1.AnimationCurve.easeOut
        });
        transition.push({
            target: slideMap.slide.layout,
            translate: { x: -this.pageWidth * 2, y: 0 },
            duration: animationDuration,
            curve: enums_1.AnimationCurve.easeOut
        });
        var animationSet = new AnimationModule.Animation(transition, false);
        return animationSet.play();
    };
    SlidesComponent.prototype.showLeftSlide = function (slideMap, offset, endingVelocity, duration) {
        if (offset === void 0) { offset = this.pageWidth; }
        if (endingVelocity === void 0) { endingVelocity = 32; }
        if (duration === void 0) { duration = 300; }
        var animationDuration;
        animationDuration = duration; // default value
        var transition = new Array();
        transition.push({
            target: slideMap.left.slide.layout,
            translate: { x: -this.pageWidth, y: 0 },
            duration: animationDuration,
            curve: enums_1.AnimationCurve.easeOut
        });
        transition.push({
            target: slideMap.slide.layout,
            translate: { x: 0, y: 0 },
            duration: animationDuration,
            curve: enums_1.AnimationCurve.easeOut
        });
        var animationSet = new AnimationModule.Animation(transition, false);
        return animationSet.play();
    };
    SlidesComponent.prototype.applySwipe = function (pageWidth) {
        var _this = this;
        var previousDelta = -1; //hack to get around ios firing pan event after release
        var endingVelocity = 0;
        var startTime, deltaTime;
        this.transitioning = false;
        this.currentSlide.slide.layout.on('tap', function (args) {
            _this.tap.next(args);
        });
        this.currentSlide.slide.layout.on('pan', function (args) {
            if (args.state === gestures.GestureStateTypes.began) {
                startTime = Date.now();
                previousDelta = 0;
                endingVelocity = 250;
                //this.triggerStartEvent();
            }
            else if (args.state === gestures.GestureStateTypes.ended) {
                deltaTime = Date.now() - startTime;
                // if velocityScrolling is enabled then calculate the velocitty
                // swiping left to right.
                if (args.deltaX > (pageWidth / 3)) {
                    if (_this.hasPrevious) {
                        _this.transitioning = true;
                        _this.showLeftSlide(_this.currentSlide, args.deltaX, endingVelocity).then(function () {
                            _this.setupPanel(_this.currentSlide.left);
                            //this.triggerChangeEventLeftToRight();
                        });
                    }
                    else {
                        //We're at the start
                        //Notify no more slides
                        //this.triggerCancelEvent(cancellationReason.noPrevSlides);
                    }
                    return;
                }
                else if (args.deltaX < (-pageWidth / 3)) {
                    if (_this.hasNext) {
                        _this.transitioning = true;
                        _this.showRightSlide(_this.currentSlide, args.deltaX, endingVelocity).then(function () {
                            _this.setupPanel(_this.currentSlide.right);
                            // Notify changed
                            //this.triggerChangeEventRightToLeft();
                            if (!_this.hasNext) {
                                // Notify finsihed
                                // this.notify({
                                // 	eventName: SlideContainer.FINISHED_EVENT,
                                // 	object: this
                                // });
                            }
                        });
                    }
                    else {
                        // We're at the end
                        // Notify no more slides
                        //this.triggerCancelEvent(cancellationReason.noMoreSlides);
                    }
                    return;
                }
                if (_this.transitioning === false) {
                    //Notify cancelled
                    //this.triggerCancelEvent(cancellationReason.user);
                    _this.transitioning = true;
                    _this.currentSlide.slide.layout.animate({
                        translate: { x: -_this.pageWidth, y: 0 },
                        duration: 200,
                        curve: enums_1.AnimationCurve.easeOut
                    });
                    if (_this.hasNext) {
                        _this.currentSlide.right.slide.layout.animate({
                            translate: { x: 0, y: 0 },
                            duration: 200,
                            curve: enums_1.AnimationCurve.easeOut
                        });
                        if (app.ios)
                            _this.currentSlide.right.slide.layout.translateX = 0;
                    }
                    if (_this.hasPrevious) {
                        _this.currentSlide.left.slide.layout.animate({
                            translate: { x: -_this.pageWidth * 2, y: 0 },
                            duration: 200,
                            curve: enums_1.AnimationCurve.easeOut
                        });
                        if (app.ios)
                            _this.currentSlide.left.slide.layout.translateX = -_this.pageWidth;
                    }
                    if (app.ios)
                        _this.currentSlide.slide.layout.translateX = -_this.pageWidth;
                    _this.transitioning = false;
                }
            }
            else {
                if (!_this.transitioning
                    && previousDelta !== args.deltaX
                    && args.deltaX != null
                    && args.deltaX < 0) {
                    if (_this.hasNext) {
                        _this.direction = direction.left;
                        _this.currentSlide.slide.layout.translateX = args.deltaX - _this.pageWidth;
                        _this.currentSlide.right.slide.layout.translateX = args.deltaX;
                    }
                }
                else if (!_this.transitioning
                    && previousDelta !== args.deltaX
                    && args.deltaX != null
                    && args.deltaX > 0) {
                    if (_this.hasPrevious) {
                        _this.direction = direction.right;
                        _this.currentSlide.slide.layout.translateX = args.deltaX - _this.pageWidth;
                        _this.currentSlide.left.slide.layout.translateX = -(_this.pageWidth * 2) + args.deltaX;
                    }
                }
                if (args.deltaX !== 0) {
                    previousDelta = args.deltaX;
                }
            }
        });
    };
    SlidesComponent.prototype.buildSlideMap = function (slides) {
        var _this = this;
        this._slideMap = [];
        slides.forEach(function (slide, index) {
            _this._slideMap.push({
                slide: slide,
                index: index,
            });
        });
        this._slideMap.forEach(function (mapping, index) {
            if (_this._slideMap[index - 1] != null)
                mapping.left = _this._slideMap[index - 1];
            if (_this._slideMap[index + 1] != null)
                mapping.right = _this._slideMap[index + 1];
        });
        if (this.loop) {
            this._slideMap[0].left = this._slideMap[this._slideMap.length - 1];
            this._slideMap[this._slideMap.length - 1].right = this._slideMap[0];
        }
        return this._slideMap[0];
    };
    SlidesComponent.prototype.GoToSlide = function (num, traverseDuration, landingDuration) {
        var _this = this;
        if (traverseDuration === void 0) { traverseDuration = 50; }
        if (landingDuration === void 0) { landingDuration = 200; }
        if (this.currentSlide.index == num)
            return;
        var duration = landingDuration;
        if (Math.abs(num - this.currentSlide.index) != 1)
            duration = traverseDuration;
        if (this.currentSlide.index < num)
            this.nextSlide(duration).then(function () { return _this.GoToSlide(num); });
        else
            this.previousSlide(duration).then(function () { return _this.GoToSlide(num); });
    };
    SlidesComponent.prototype.nextSlide = function (duration) {
        var _this = this;
        if (!this.hasNext) {
            //this.triggerCancelEvent(cancellationReason.noMoreSlides);
            return;
        }
        this.direction = direction.left;
        this.transitioning = true;
        //	this.triggerStartEvent();
        return this.showRightSlide(this.currentSlide, null, null, duration).then(function () {
            _this.setupPanel(_this.currentSlide.right);
            //this.triggerChangeEventRightToLeft();
        });
    };
    SlidesComponent.prototype.previousSlide = function (duration) {
        var _this = this;
        if (!this.hasPrevious) {
            //this.triggerCancelEvent(cancellationReason.noPrevSlides);
            return;
        }
        this.direction = direction.right;
        this.transitioning = true;
        //this.triggerStartEvent();
        return this.showLeftSlide(this.currentSlide, null, null, duration).then(function () {
            _this.setupPanel(_this.currentSlide.left);
            //this.triggerChangeEventLeftToRight();
        });
    };
    __decorate([
        core_1.ContentChildren(core_1.forwardRef(function () { return slide_component_1.SlideComponent; })),
        __metadata("design:type", core_1.QueryList)
    ], SlidesComponent.prototype, "slides", void 0);
    __decorate([
        core_1.ViewChild('footer'),
        __metadata("design:type", core_1.ElementRef)
    ], SlidesComponent.prototype, "footer", void 0);
    __decorate([
        core_1.Input('pageWidth'),
        __metadata("design:type", Number)
    ], SlidesComponent.prototype, "pageWidth", void 0);
    __decorate([
        core_1.Input('pageHeight'),
        __metadata("design:type", Number)
    ], SlidesComponent.prototype, "pageHeight", void 0);
    __decorate([
        core_1.Input('footerMarginTop'),
        __metadata("design:type", Number)
    ], SlidesComponent.prototype, "footerMarginTop", void 0);
    __decorate([
        core_1.Input('loop'),
        __metadata("design:type", Boolean)
    ], SlidesComponent.prototype, "loop", void 0);
    __decorate([
        core_1.Input('pageIndicators'),
        __metadata("design:type", Boolean)
    ], SlidesComponent.prototype, "pageIndicators", void 0);
    __decorate([
        core_1.Input('class'),
        __metadata("design:type", String)
    ], SlidesComponent.prototype, "cssClass", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], SlidesComponent.prototype, "changed", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], SlidesComponent.prototype, "finished", void 0);
    __decorate([
        core_1.Output('tap'),
        __metadata("design:type", core_1.EventEmitter)
    ], SlidesComponent.prototype, "tap", void 0);
    SlidesComponent = __decorate([
        core_1.Component({
            selector: 'slides',
            template: "\n\t<AbsoluteLayout width=\"100%\">\n\t\t<ng-content></ng-content>\n\t\t<StackLayout *ngIf=\"pageIndicators\" #footer style=\"width:100%; height:20%;\">\n\t\t\t<Label *ngFor=\"let indicator of indicators\"\n\t\t\t\t[class.slide-indicator-active]=\"indicator.active == true\"\n\t\t\t\t[class.slide-indicator-inactive]=\"indicator.active == false\t\"\n\t\t\t></Label>\n\t\t</StackLayout>\n\t</AbsoluteLayout>\n\t",
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef])
    ], SlidesComponent);
    return SlidesComponent;
}());
exports.SlidesComponent = SlidesComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2xpZGVzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNsaWRlcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBOE07QUFFOU0sNERBQTBEO0FBQzFELHNDQUF3QztBQUN4QyxtQ0FBcUM7QUFDckMsOENBQWdEO0FBQ2hELGtDQUF1RDtBQUN2RCxpQ0FBbUM7QUFDbkMsOERBQTREO0FBQzVELHdEQUFzRDtBQWN0RCxJQUFLLFNBSUo7QUFKRCxXQUFLLFNBQVM7SUFDYix5Q0FBSSxDQUFBO0lBQ0oseUNBQUksQ0FBQTtJQUNKLDJDQUFLLENBQUE7QUFDTixDQUFDLEVBSkksU0FBUyxLQUFULFNBQVMsUUFJYjtBQUVELElBQUssa0JBSUo7QUFKRCxXQUFLLGtCQUFrQjtJQUN0QiwyREFBSSxDQUFBO0lBQ0osMkVBQVksQ0FBQTtJQUNaLDJFQUFZLENBQUE7QUFDYixDQUFDLEVBSkksa0JBQWtCLEtBQWxCLGtCQUFrQixRQUl0QjtBQWtCRDtJQTZCQyx5QkFBb0IsR0FBc0I7UUFBdEIsUUFBRyxHQUFILEdBQUcsQ0FBbUI7UUFwQjFCLGFBQVEsR0FBVyxFQUFFLENBQUM7UUFDNUIsWUFBTyxHQUFzQixJQUFJLG1CQUFZLEVBQUUsQ0FBQztRQUNoRCxhQUFRLEdBQXNCLElBQUksbUJBQVksRUFBRSxDQUFDO1FBQzVDLFFBQUcsR0FBNEMsSUFBSSxtQkFBWSxFQUE2QixDQUFDO1FBR3BHLGNBQVMsR0FBYyxTQUFTLENBQUMsSUFBSSxDQUFDO1FBQ3RDLGtCQUFhLEdBQVcsRUFBRSxDQUFDO1FBY2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFURCxzQkFBSSxvQ0FBTzthQUFYO1lBQ0MsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQztRQUN6RCxDQUFDOzs7T0FBQTtJQUNELHNCQUFJLHdDQUFXO2FBQWY7WUFDQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO1FBQ3hELENBQUM7OztPQUFBO0lBTUQsa0NBQVEsR0FBUjtRQUVDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztRQUMxQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDeEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDO1FBQ3hGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQztRQUM1RixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3JILDRCQUE0QjtRQUM1QixHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyx1QkFBdUIsRUFBRSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUVELHlDQUFlLEdBQWY7UUFBQSxpQkFvQkM7UUFuQkEsa0RBQWtEO1FBQ2xELElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUMsS0FBcUI7WUFDekMsZ0NBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckQsS0FBSyxDQUFDLFVBQVUsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDO1lBQ2xDLEtBQUssQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQztRQUVyQyxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFFOUQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakMsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDaEMsQ0FBQztJQUNGLENBQUM7SUFFRCxxQ0FBVyxHQUFYO1FBQ0MsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCw4Q0FBb0IsR0FBcEIsVUFBcUIsSUFBcUM7UUFBMUQsaUJBNENDO1FBMUNBLDBFQUEwRTtRQUMxRSw2Q0FBNkM7UUFDN0MsVUFBVSxDQUFDO1lBRVYsa0RBQWtEO1lBQ2xELDZFQUE2RTtZQUM3RSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQ25DLEtBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDO29CQUM3QixRQUFRLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDO2dCQUM5RSxLQUFJLENBQUMsVUFBVSxHQUFHLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQztvQkFDOUIsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQztZQUMvRSxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ1AsS0FBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7Z0JBQ3RELEtBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDO1lBQ3pELENBQUM7WUFFRCxLQUFJLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFFdkUsa0RBQWtEO1lBQ2xELEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUMsS0FBcUI7Z0JBQ3pDLGdDQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNyRCxLQUFLLENBQUMsVUFBVSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQ2xDLEtBQUssQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQztnQkFDcEMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsVUFBQyxJQUFTO29CQUN0QyxFQUFFLENBQUMsQ0FBQyxJQUFJLFlBQVksMEJBQVcsQ0FBQyxDQUFDLENBQUM7d0JBQ2pDLGdDQUFjLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBQzdDLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQzt3QkFDNUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDO29CQUMvQixDQUFDO2dCQUNGLENBQUMsQ0FBQyxDQUFDO1lBQ0osQ0FBQyxDQUFDLENBQUM7WUFFSCxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDeEMsQ0FBQztZQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN6QixLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdEMsQ0FBQztRQUdGLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLHNDQUFzQztJQUMvQyxDQUFDO0lBRUQsZ0RBQXNCLEdBQXRCLFVBQXVCLFdBQW1CO1FBQ3pDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQUMsU0FBc0IsRUFBRSxLQUFhO1lBQ3pELEVBQUUsQ0FBQyxDQUFDLEtBQUssS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUN6QixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ1AsU0FBUyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDMUIsQ0FBQztRQUNGLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFVBQVUsR0FBTyxJQUFJLENBQUMsVUFBVSxRQUFDLENBQUM7UUFDdkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRUQsRUFBRTtJQUNGLHFCQUFxQjtJQUNyQixFQUFFO0lBRUYsa0JBQWtCO0lBQ1YsbURBQXlCLEdBQWpDLFVBQWtDLFVBQWtCO1FBQ25ELE1BQU0sQ0FBQyxVQUFVLEdBQUcsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELGVBQWU7SUFDUCxxQ0FBVyxHQUFuQixVQUFvQixTQUFxQjtRQUFyQiwwQkFBQSxFQUFBLGFBQXFCO1FBRXhDLElBQU0sYUFBYSxHQUFpQixJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWMsQ0FBQztRQUMvRCxhQUFhLENBQUMsbUJBQW1CLEdBQUcsUUFBUSxDQUFDO1FBQzdDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsWUFBWSxDQUFDO1FBQ3pDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUMvQyxhQUFhLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDMUMsYUFBYSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBRXJDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ2IsYUFBYSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDcEMsQ0FBQztRQUVELElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLE9BQU8sS0FBSyxHQUFHLFNBQVMsRUFBRSxDQUFDO1lBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDeEMsS0FBSyxFQUFFLENBQUM7UUFDVCxDQUFDO0lBQ0YsQ0FBQztJQUVPLG9DQUFVLEdBQWxCLFVBQW1CLEtBQWdCO1FBQ2xDLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQztRQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBRTFCLDJFQUEyRTtRQUMzRSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUV2QyxrQ0FBa0M7UUFDbEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEMsR0FBRztRQUVILEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3RELENBQUM7UUFFRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRTNDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUIsQ0FBQztJQUNGLENBQUM7SUFFTyx3Q0FBYyxHQUF0QixVQUF1QixLQUFnQjtRQUN0QywyRUFBMkU7UUFDM0UsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNwRCxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7UUFDMUQsQ0FBQztRQUNELEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDaEQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN0RCxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUN6QyxDQUFDO0lBQ0YsQ0FBQztJQUVPLHdDQUFjLEdBQXRCLFVBQXVCLFFBQW1CLEVBQUUsTUFBK0IsRUFBRSxjQUEyQixFQUFFLFFBQXNCO1FBQXBGLHVCQUFBLEVBQUEsU0FBaUIsSUFBSSxDQUFDLFNBQVM7UUFBRSwrQkFBQSxFQUFBLG1CQUEyQjtRQUFFLHlCQUFBLEVBQUEsY0FBc0I7UUFDL0gsSUFBSSxpQkFBeUIsQ0FBQztRQUM5QixpQkFBaUIsR0FBRyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0I7UUFFOUMsSUFBSSxVQUFVLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUU3QixVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ2YsTUFBTSxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU07WUFDbkMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFO1lBQ3ZDLFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsS0FBSyxFQUFFLHNCQUFjLENBQUMsT0FBTztTQUM3QixDQUFDLENBQUM7UUFDSCxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ2YsTUFBTSxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTTtZQUM3QixTQUFTLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFO1lBQzNDLFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsS0FBSyxFQUFFLHNCQUFjLENBQUMsT0FBTztTQUM3QixDQUFDLENBQUM7UUFDSCxJQUFJLFlBQVksR0FBRyxJQUFJLGVBQWUsQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRXBFLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVPLHVDQUFhLEdBQXJCLFVBQXNCLFFBQW1CLEVBQUUsTUFBK0IsRUFBRSxjQUEyQixFQUFFLFFBQXNCO1FBQXBGLHVCQUFBLEVBQUEsU0FBaUIsSUFBSSxDQUFDLFNBQVM7UUFBRSwrQkFBQSxFQUFBLG1CQUEyQjtRQUFFLHlCQUFBLEVBQUEsY0FBc0I7UUFFOUgsSUFBSSxpQkFBeUIsQ0FBQztRQUM5QixpQkFBaUIsR0FBRyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0I7UUFDOUMsSUFBSSxVQUFVLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUU3QixVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ2YsTUFBTSxFQUFFLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU07WUFDbEMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFO1lBQ3ZDLFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsS0FBSyxFQUFFLHNCQUFjLENBQUMsT0FBTztTQUM3QixDQUFDLENBQUM7UUFDSCxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ2YsTUFBTSxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTTtZQUM3QixTQUFTLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDekIsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixLQUFLLEVBQUUsc0JBQWMsQ0FBQyxPQUFPO1NBQzdCLENBQUMsQ0FBQztRQUNILElBQUksWUFBWSxHQUFHLElBQUksZUFBZSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFcEUsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUU1QixDQUFDO0lBRU0sb0NBQVUsR0FBakIsVUFBa0IsU0FBaUI7UUFBbkMsaUJBOEhDO1FBN0hBLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsdURBQXVEO1FBQy9FLElBQUksY0FBYyxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLFNBQVMsRUFBRSxTQUFTLENBQUM7UUFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFFM0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsVUFBQyxJQUErQjtZQUN4RSxLQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNyQixDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLFVBQUMsSUFBa0M7WUFDM0UsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDckQsU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDdkIsYUFBYSxHQUFHLENBQUMsQ0FBQztnQkFDbEIsY0FBYyxHQUFHLEdBQUcsQ0FBQztnQkFFckIsMkJBQTJCO1lBQzVCLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDNUQsU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ25DLCtEQUErRDtnQkFFL0QseUJBQXlCO2dCQUN6QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDbkMsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7d0JBQ3RCLEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO3dCQUMxQixLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQUM7NEJBQ3ZFLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzs0QkFFeEMsdUNBQXVDO3dCQUN4QyxDQUFDLENBQUMsQ0FBQztvQkFDSixDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNQLG9CQUFvQjt3QkFDcEIsdUJBQXVCO3dCQUN2QiwyREFBMkQ7b0JBQzVELENBQUM7b0JBQ0QsTUFBTSxDQUFDO2dCQUNSLENBQUM7Z0JBRUQsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUNsQixLQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQzt3QkFDMUIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsY0FBYyxDQUFDLENBQUMsSUFBSSxDQUFDOzRCQUN4RSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBRXpDLGlCQUFpQjs0QkFDakIsdUNBQXVDOzRCQUV2QyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dDQUNuQixrQkFBa0I7Z0NBQ2xCLGdCQUFnQjtnQ0FDaEIsNkNBQTZDO2dDQUM3QyxnQkFBZ0I7Z0NBQ2hCLE1BQU07NEJBQ1AsQ0FBQzt3QkFDRixDQUFDLENBQUMsQ0FBQztvQkFDSixDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQjt3QkFDbkIsd0JBQXdCO3dCQUN4QiwyREFBMkQ7b0JBQzVELENBQUM7b0JBQ0QsTUFBTSxDQUFDO2dCQUNSLENBQUM7Z0JBRUQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLGFBQWEsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNsQyxrQkFBa0I7b0JBQ2xCLG1EQUFtRDtvQkFDbkQsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7d0JBQ3RDLFNBQVMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEtBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRTt3QkFDdkMsUUFBUSxFQUFFLEdBQUc7d0JBQ2IsS0FBSyxFQUFFLHNCQUFjLENBQUMsT0FBTztxQkFDN0IsQ0FBQyxDQUFDO29CQUNILEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUNsQixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQzs0QkFDNUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFOzRCQUN6QixRQUFRLEVBQUUsR0FBRzs0QkFDYixLQUFLLEVBQUUsc0JBQWMsQ0FBQyxPQUFPO3lCQUM3QixDQUFDLENBQUM7d0JBQ0gsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQzs0QkFDWCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUM7b0JBQ3RELENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7d0JBQ3RCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDOzRCQUMzQyxTQUFTLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxLQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFOzRCQUMzQyxRQUFRLEVBQUUsR0FBRzs0QkFDYixLQUFLLEVBQUUsc0JBQWMsQ0FBQyxPQUFPO3lCQUM3QixDQUFDLENBQUM7d0JBQ0gsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQzs0QkFDWCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUM7b0JBRW5FLENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQzt3QkFDWCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQztvQkFFN0QsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7Z0JBQzVCLENBQUM7WUFDRixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ1AsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsYUFBYTt1QkFDbkIsYUFBYSxLQUFLLElBQUksQ0FBQyxNQUFNO3VCQUM3QixJQUFJLENBQUMsTUFBTSxJQUFJLElBQUk7dUJBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFckIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQ2xCLEtBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQzt3QkFDaEMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUM7d0JBQ3pFLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBRS9ELENBQUM7Z0JBQ0YsQ0FBQztnQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsYUFBYTt1QkFDMUIsYUFBYSxLQUFLLElBQUksQ0FBQyxNQUFNO3VCQUM3QixJQUFJLENBQUMsTUFBTSxJQUFJLElBQUk7dUJBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFFckIsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7d0JBQ3RCLEtBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQzt3QkFDakMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUM7d0JBQ3pFLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxLQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ3RGLENBQUM7Z0JBQ0YsQ0FBQztnQkFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZCLGFBQWEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUM3QixDQUFDO1lBRUYsQ0FBQztRQUNGLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVPLHVDQUFhLEdBQXJCLFVBQXNCLE1BQXdCO1FBQTlDLGlCQW9CQztRQW5CQSxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUMsS0FBcUIsRUFBRSxLQUFhO1lBQ25ELEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO2dCQUNuQixLQUFLLEVBQUUsS0FBSztnQkFDWixLQUFLLEVBQUUsS0FBSzthQUNaLENBQUMsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFrQixFQUFFLEtBQWE7WUFDeEQsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDO2dCQUNyQyxPQUFPLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzFDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQztnQkFDckMsT0FBTyxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM1QyxDQUFDLENBQUMsQ0FBQztRQUVILEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNuRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRU0sbUNBQVMsR0FBaEIsVUFBaUIsR0FBVyxFQUFFLGdCQUE2QixFQUFFLGVBQTZCO1FBQTFGLGlCQVVDO1FBVjZCLGlDQUFBLEVBQUEscUJBQTZCO1FBQUUsZ0NBQUEsRUFBQSxxQkFBNkI7UUFDekYsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLElBQUksR0FBRyxDQUFDO1lBQUMsTUFBTSxDQUFDO1FBRTNDLElBQUksUUFBUSxHQUFXLGVBQWUsQ0FBQztRQUN2QyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUFDLFFBQVEsR0FBRyxnQkFBZ0IsQ0FBQztRQUU5RSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7WUFDakMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQW5CLENBQW1CLENBQUMsQ0FBQztRQUMxRCxJQUFJO1lBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQW5CLENBQW1CLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRU0sbUNBQVMsR0FBaEIsVUFBaUIsUUFBaUI7UUFBbEMsaUJBYUM7UUFaQSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ25CLDJEQUEyRDtZQUMzRCxNQUFNLENBQUM7UUFDUixDQUFDO1FBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLDRCQUE0QjtRQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3hFLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6Qyx1Q0FBdUM7UUFDeEMsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBR00sdUNBQWEsR0FBcEIsVUFBcUIsUUFBaUI7UUFBdEMsaUJBY0M7UUFiQSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLDJEQUEyRDtZQUMzRCxNQUFNLENBQUM7UUFDUixDQUFDO1FBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLDJCQUEyQjtRQUMzQixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3ZFLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV4Qyx1Q0FBdUM7UUFDeEMsQ0FBQyxDQUFDLENBQUM7SUFDSixDQUFDO0lBaGJrRDtRQUFsRCxzQkFBZSxDQUFDLGlCQUFVLENBQUMsY0FBTSxPQUFBLGdDQUFjLEVBQWQsQ0FBYyxDQUFDLENBQUM7a0NBQVMsZ0JBQVM7bURBQWlCO0lBQ2hFO1FBQXBCLGdCQUFTLENBQUMsUUFBUSxDQUFDO2tDQUFTLGlCQUFVO21EQUFDO0lBQ3BCO1FBQW5CLFlBQUssQ0FBQyxXQUFXLENBQUM7O3NEQUFtQjtJQUNqQjtRQUFwQixZQUFLLENBQUMsWUFBWSxDQUFDOzt1REFBb0I7SUFDZDtRQUF6QixZQUFLLENBQUMsaUJBQWlCLENBQUM7OzREQUF5QjtJQUNuQztRQUFkLFlBQUssQ0FBQyxNQUFNLENBQUM7O2lEQUFlO0lBQ0o7UUFBeEIsWUFBSyxDQUFDLGdCQUFnQixDQUFDOzsyREFBeUI7SUFDakM7UUFBZixZQUFLLENBQUMsT0FBTyxDQUFDOztxREFBdUI7SUFDNUI7UUFBVCxhQUFNLEVBQUU7a0NBQVUsbUJBQVk7b0RBQTJCO0lBQ2hEO1FBQVQsYUFBTSxFQUFFO2tDQUFXLG1CQUFZO3FEQUEyQjtJQUM1QztRQUFkLGFBQU0sQ0FBQyxLQUFLLENBQUM7a0NBQU0sbUJBQVk7Z0RBQTRFO0lBWmhHLGVBQWU7UUFoQjNCLGdCQUFTLENBQUM7WUFDVixRQUFRLEVBQUUsUUFBUTtZQUNsQixRQUFRLEVBQUUsNFpBVVQ7WUFDRCxhQUFhLEVBQUUsd0JBQWlCLENBQUMsSUFBSTtTQUNyQyxDQUFDO3lDQStCd0Isd0JBQWlCO09BN0I5QixlQUFlLENBbWIzQjtJQUFELHNCQUFDO0NBQUEsQUFuYkQsSUFtYkM7QUFuYlksMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgVmlld0VuY2Fwc3VsYXRpb24sIENoYW5nZURldGVjdG9yUmVmLCBPbkRlc3Ryb3ksIGZvcndhcmRSZWYsIFZpZXdDaGlsZCwgQ29udGVudENoaWxkcmVuLCBFbGVtZW50UmVmLCBRdWVyeUxpc3QsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgU2xpZGVDb21wb25lbnQgfSBmcm9tICcuLi9zbGlkZS9zbGlkZS5jb21wb25lbnQnO1xyXG5pbXBvcnQgKiBhcyBnZXN0dXJlcyBmcm9tICd1aS9nZXN0dXJlcyc7XHJcbmltcG9ydCAqIGFzIHBsYXRmb3JtIGZyb20gJ3BsYXRmb3JtJztcclxuaW1wb3J0ICogYXMgQW5pbWF0aW9uTW9kdWxlIGZyb20gJ3VpL2FuaW1hdGlvbic7XHJcbmltcG9ydCB7IEFuaW1hdGlvbkN1cnZlLCBPcmllbnRhdGlvbiB9IGZyb20gJ3VpL2VudW1zJztcclxuaW1wb3J0ICogYXMgYXBwIGZyb20gJ2FwcGxpY2F0aW9uJztcclxuaW1wb3J0IHsgQWJzb2x1dGVMYXlvdXQgfSBmcm9tICd1aS9sYXlvdXRzL2Fic29sdXRlLWxheW91dCc7XHJcbmltcG9ydCB7IFN0YWNrTGF5b3V0IH0gZnJvbSAndWkvbGF5b3V0cy9zdGFjay1sYXlvdXQnO1xyXG5pbXBvcnQgeyBMYWJlbCB9IGZyb20gJ3VpL2xhYmVsJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgSUluZGljYXRvcnMge1xyXG5cdGFjdGl2ZTogYm9vbGVhbjtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBJU2xpZGVNYXAge1xyXG5cdHNsaWRlOiBTbGlkZUNvbXBvbmVudDtcclxuXHRpbmRleDogbnVtYmVyO1xyXG5cdGxlZnQ/OiBJU2xpZGVNYXA7XHJcblx0cmlnaHQ/OiBJU2xpZGVNYXA7XHJcbn1cclxuXHJcbmVudW0gZGlyZWN0aW9uIHtcclxuXHRub25lLFxyXG5cdGxlZnQsXHJcblx0cmlnaHRcclxufVxyXG5cclxuZW51bSBjYW5jZWxsYXRpb25SZWFzb24ge1xyXG5cdHVzZXIsXHJcblx0bm9QcmV2U2xpZGVzLFxyXG5cdG5vTW9yZVNsaWRlc1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogJ3NsaWRlcycsXHJcblx0dGVtcGxhdGU6IGBcclxuXHQ8QWJzb2x1dGVMYXlvdXQgd2lkdGg9XCIxMDAlXCI+XHJcblx0XHQ8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcblx0XHQ8U3RhY2tMYXlvdXQgKm5nSWY9XCJwYWdlSW5kaWNhdG9yc1wiICNmb290ZXIgc3R5bGU9XCJ3aWR0aDoxMDAlOyBoZWlnaHQ6MjAlO1wiPlxyXG5cdFx0XHQ8TGFiZWwgKm5nRm9yPVwibGV0IGluZGljYXRvciBvZiBpbmRpY2F0b3JzXCJcclxuXHRcdFx0XHRbY2xhc3Muc2xpZGUtaW5kaWNhdG9yLWFjdGl2ZV09XCJpbmRpY2F0b3IuYWN0aXZlID09IHRydWVcIlxyXG5cdFx0XHRcdFtjbGFzcy5zbGlkZS1pbmRpY2F0b3ItaW5hY3RpdmVdPVwiaW5kaWNhdG9yLmFjdGl2ZSA9PSBmYWxzZVx0XCJcclxuXHRcdFx0PjwvTGFiZWw+XHJcblx0XHQ8L1N0YWNrTGF5b3V0PlxyXG5cdDwvQWJzb2x1dGVMYXlvdXQ+XHJcblx0YCxcclxuXHRlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2xpZGVzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcblx0QENvbnRlbnRDaGlsZHJlbihmb3J3YXJkUmVmKCgpID0+IFNsaWRlQ29tcG9uZW50KSkgc2xpZGVzOiBRdWVyeUxpc3Q8U2xpZGVDb21wb25lbnQ+O1xyXG5cdEBWaWV3Q2hpbGQoJ2Zvb3RlcicpIGZvb3RlcjogRWxlbWVudFJlZjtcclxuXHRASW5wdXQoJ3BhZ2VXaWR0aCcpIHBhZ2VXaWR0aDogbnVtYmVyO1xyXG5cdEBJbnB1dCgncGFnZUhlaWdodCcpIHBhZ2VIZWlnaHQ6IG51bWJlcjtcclxuXHRASW5wdXQoJ2Zvb3Rlck1hcmdpblRvcCcpIGZvb3Rlck1hcmdpblRvcDogbnVtYmVyO1xyXG5cdEBJbnB1dCgnbG9vcCcpIGxvb3A6IGJvb2xlYW47XHJcblx0QElucHV0KCdwYWdlSW5kaWNhdG9ycycpIHBhZ2VJbmRpY2F0b3JzOiBib29sZWFuO1xyXG5cdEBJbnB1dCgnY2xhc3MnKSBjc3NDbGFzczogc3RyaW5nID0gJyc7XHJcblx0QE91dHB1dCgpIGNoYW5nZWQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cdEBPdXRwdXQoKSBmaW5pc2hlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblx0QE91dHB1dCgndGFwJykgdGFwOiBFdmVudEVtaXR0ZXI8Z2VzdHVyZXMuR2VzdHVyZUV2ZW50RGF0YT4gPSBuZXcgRXZlbnRFbWl0dGVyPGdlc3R1cmVzLkdlc3R1cmVFdmVudERhdGE+KCk7XHJcblxyXG5cdHByaXZhdGUgdHJhbnNpdGlvbmluZzogYm9vbGVhbjtcclxuXHRwcml2YXRlIGRpcmVjdGlvbjogZGlyZWN0aW9uID0gZGlyZWN0aW9uLm5vbmU7XHJcblx0cHJpdmF0ZSBGT09URVJfSEVJR0hUOiBudW1iZXIgPSA1MDtcclxuXHJcblx0aW5kaWNhdG9yczogSUluZGljYXRvcnNbXTtcclxuXHRjdXJyZW50U2xpZGU6IElTbGlkZU1hcDtcclxuXHRfc2xpZGVNYXA6IElTbGlkZU1hcFtdO1xyXG5cclxuXHRnZXQgaGFzTmV4dCgpOiBib29sZWFuIHtcclxuXHRcdHJldHVybiAhIXRoaXMuY3VycmVudFNsaWRlICYmICEhdGhpcy5jdXJyZW50U2xpZGUucmlnaHQ7XHJcblx0fVxyXG5cdGdldCBoYXNQcmV2aW91cygpOiBib29sZWFuIHtcclxuXHRcdHJldHVybiAhIXRoaXMuY3VycmVudFNsaWRlICYmICEhdGhpcy5jdXJyZW50U2xpZGUubGVmdDtcclxuXHR9XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgcmVmOiBDaGFuZ2VEZXRlY3RvclJlZikge1xyXG5cdFx0dGhpcy5pbmRpY2F0b3JzID0gW107XHJcblx0fVxyXG5cclxuXHRuZ09uSW5pdCgpIHtcclxuXHJcblx0XHR0aGlzLmxvb3AgPSB0aGlzLmxvb3AgPyB0aGlzLmxvb3AgOiBmYWxzZTtcclxuXHRcdHRoaXMucGFnZUluZGljYXRvcnMgPSB0aGlzLnBhZ2VJbmRpY2F0b3JzID8gdGhpcy5wYWdlSW5kaWNhdG9ycyA6IGZhbHNlO1xyXG5cdFx0dGhpcy5wYWdlV2lkdGggPSB0aGlzLnBhZ2VXaWR0aCA/IHRoaXMucGFnZVdpZHRoIDogcGxhdGZvcm0uc2NyZWVuLm1haW5TY3JlZW4ud2lkdGhESVBzO1xyXG5cdFx0dGhpcy5wYWdlSGVpZ2h0ID0gdGhpcy5wYWdlSGVpZ2h0ID8gdGhpcy5wYWdlSGVpZ2h0IDogcGxhdGZvcm0uc2NyZWVuLm1haW5TY3JlZW4uaGVpZ2h0RElQcztcclxuXHRcdHRoaXMuZm9vdGVyTWFyZ2luVG9wID0gdGhpcy5mb290ZXJNYXJnaW5Ub3AgPyB0aGlzLmZvb3Rlck1hcmdpblRvcCA6IHRoaXMuY2FsY3VsYXRlRm9vcnRlck1hcmdpblRvcCh0aGlzLnBhZ2VIZWlnaHQpO1xyXG5cdFx0Ly8gaGFuZGxlIG9yaWVudGF0aW9uIGNoYW5nZVxyXG5cdFx0YXBwLm9uKGFwcC5vcmllbnRhdGlvbkNoYW5nZWRFdmVudCwgdGhpcy5vbk9yaWVudGF0aW9uQ2hhbmdlZCwgdGhpcyk7XHJcblx0fVxyXG5cclxuXHRuZ0FmdGVyVmlld0luaXQoKSB7XHJcblx0XHQvLyBsb29wIHRocm91Z2ggc2xpZGVzIGFuZCBzZXR1cCBoZWlnaHQgYW5kIHdpZGl0aFxyXG5cdFx0dGhpcy5zbGlkZXMuZm9yRWFjaCgoc2xpZGU6IFNsaWRlQ29tcG9uZW50KSA9PiB7XHJcblx0XHRcdEFic29sdXRlTGF5b3V0LnNldExlZnQoc2xpZGUubGF5b3V0LCB0aGlzLnBhZ2VXaWR0aCk7XHJcblx0XHRcdHNsaWRlLnNsaWRlV2lkdGggPSB0aGlzLnBhZ2VXaWR0aDtcclxuXHRcdFx0c2xpZGUuc2xpZGVIZWlnaHQgPSB0aGlzLnBhZ2VIZWlnaHQ7XHJcblxyXG5cdFx0fSk7XHJcblxyXG5cdFx0dGhpcy5jdXJyZW50U2xpZGUgPSB0aGlzLmJ1aWxkU2xpZGVNYXAodGhpcy5zbGlkZXMudG9BcnJheSgpKTtcclxuXHJcblx0XHRpZiAodGhpcy5jdXJyZW50U2xpZGUpIHtcclxuXHRcdFx0dGhpcy5wb3NpdGlvblNsaWRlcyh0aGlzLmN1cnJlbnRTbGlkZSk7XHJcblx0XHRcdHRoaXMuYXBwbHlTd2lwZSh0aGlzLnBhZ2VXaWR0aCk7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKHRoaXMucGFnZUluZGljYXRvcnMpIHtcclxuXHRcdFx0dGhpcy5idWlsZEZvb3Rlcih0aGlzLnNsaWRlcy5sZW5ndGgpO1xyXG5cdFx0XHR0aGlzLnNldEFjdGl2ZVBhZ2VJbmRpY2F0b3IoMCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRuZ09uRGVzdHJveSgpIHtcclxuXHRcdGFwcC5vZmYoYXBwLm9yaWVudGF0aW9uQ2hhbmdlZEV2ZW50LCB0aGlzLm9uT3JpZW50YXRpb25DaGFuZ2VkLCB0aGlzKTtcclxuXHR9XHJcblxyXG5cdG9uT3JpZW50YXRpb25DaGFuZ2VkKGFyZ3M6IGFwcC5PcmllbnRhdGlvbkNoYW5nZWRFdmVudERhdGEpIHtcclxuXHJcblx0XHQvLyBldmVudCBhbmQgcGFnZSBvcmllbnRhdGlvbiBkaWRuJ3Qgc2VlbSB0byBhbHdheXMgYmUgb24gdGhlIHNhbWUgcGFnZSBzb1xyXG5cdFx0Ly8gc2V0dGluZyBpdCBpbiB0aGUgdGltZSBvdXQgYWRkcmVzc2VzIHRoaXMuXHJcblx0XHRzZXRUaW1lb3V0KCgpID0+IHtcclxuXHJcblx0XHRcdC8vIHRoZSB2YWx1ZXMgYXJlIGVpdGhlciAnbGFuZHNjYXBlJyBvciAncG9ydHJhaXQnXHJcblx0XHRcdC8vIHBsYXRmb3JtLnNjcmVlbi5tYWluU2NyZWVuLmhlaWdodERJUHMvd2lkdGhESVBzIGhvbGRzIG9yaWdpbmFsIHNjcmVlbiBzaXplXHJcblx0XHRcdGlmIChhcmdzLm5ld1ZhbHVlID09PSAnbGFuZHNjYXBlJykge1xyXG5cdFx0XHRcdHRoaXMucGFnZVdpZHRoID0gKGFwcC5hbmRyb2lkKSA/XHJcblx0XHRcdFx0XHRwbGF0Zm9ybS5zY3JlZW4ubWFpblNjcmVlbi5oZWlnaHRESVBzIDogcGxhdGZvcm0uc2NyZWVuLm1haW5TY3JlZW4ud2lkdGhESVBzO1xyXG5cdFx0XHRcdHRoaXMucGFnZUhlaWdodCA9IChhcHAuYW5kcm9pZCkgP1xyXG5cdFx0XHRcdFx0cGxhdGZvcm0uc2NyZWVuLm1haW5TY3JlZW4ud2lkdGhESVBzIDogcGxhdGZvcm0uc2NyZWVuLm1haW5TY3JlZW4uaGVpZ2h0RElQcztcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHR0aGlzLnBhZ2VXaWR0aCA9IHBsYXRmb3JtLnNjcmVlbi5tYWluU2NyZWVuLndpZHRoRElQcztcclxuXHRcdFx0XHR0aGlzLnBhZ2VIZWlnaHQgPSBwbGF0Zm9ybS5zY3JlZW4ubWFpblNjcmVlbi5oZWlnaHRESVBzO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHR0aGlzLmZvb3Rlck1hcmdpblRvcCA9IHRoaXMuY2FsY3VsYXRlRm9vcnRlck1hcmdpblRvcCh0aGlzLnBhZ2VIZWlnaHQpO1xyXG5cclxuXHRcdFx0Ly8gbG9vcCB0aHJvdWdoIHNsaWRlcyBhbmQgc2V0dXAgaGVpZ2h0IGFuZCB3aWRpdGhcclxuXHRcdFx0dGhpcy5zbGlkZXMuZm9yRWFjaCgoc2xpZGU6IFNsaWRlQ29tcG9uZW50KSA9PiB7XHJcblx0XHRcdFx0QWJzb2x1dGVMYXlvdXQuc2V0TGVmdChzbGlkZS5sYXlvdXQsIHRoaXMucGFnZVdpZHRoKTtcclxuXHRcdFx0XHRzbGlkZS5zbGlkZVdpZHRoID0gdGhpcy5wYWdlV2lkdGg7XHJcblx0XHRcdFx0c2xpZGUuc2xpZGVIZWlnaHQgPSB0aGlzLnBhZ2VIZWlnaHQ7XHJcblx0XHRcdFx0c2xpZGUubGF5b3V0LmVhY2hMYXlvdXRDaGlsZCgodmlldzogYW55KSA9PiB7XHJcblx0XHRcdFx0XHRpZiAodmlldyBpbnN0YW5jZW9mIFN0YWNrTGF5b3V0KSB7XHJcblx0XHRcdFx0XHRcdEFic29sdXRlTGF5b3V0LnNldExlZnQodmlldywgdGhpcy5wYWdlV2lkdGgpO1xyXG5cdFx0XHRcdFx0XHR2aWV3LndpZHRoID0gdGhpcy5wYWdlV2lkdGg7XHJcblx0XHRcdFx0XHRcdHZpZXcuaGVpZ2h0ID0gdGhpcy5wYWdlSGVpZ2h0O1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHRcdGlmICh0aGlzLmN1cnJlbnRTbGlkZSkge1xyXG5cdFx0XHRcdHRoaXMucG9zaXRpb25TbGlkZXModGhpcy5jdXJyZW50U2xpZGUpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAodGhpcy5wYWdlSW5kaWNhdG9ycykge1xyXG5cdFx0XHRcdHRoaXMuYnVpbGRGb290ZXIodGhpcy5zbGlkZXMubGVuZ3RoKTtcclxuXHRcdFx0fVxyXG5cclxuXHJcblx0XHR9LCAxNyk7IC8vIG9uZSBmcmFtZSBAIDYwIGZyYW1lcy9zLCBubyBmbGlja2VyXHJcblx0fVxyXG5cclxuXHRzZXRBY3RpdmVQYWdlSW5kaWNhdG9yKGFjdGl2ZUluZGV4OiBudW1iZXIpIHtcclxuXHRcdHRoaXMuaW5kaWNhdG9ycy5tYXAoKGluZGljYXRvcjogSUluZGljYXRvcnMsIGluZGV4OiBudW1iZXIpID0+IHtcclxuXHRcdFx0aWYgKGluZGV4ID09PSBhY3RpdmVJbmRleCkge1xyXG5cdFx0XHRcdGluZGljYXRvci5hY3RpdmUgPSB0cnVlO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdGluZGljYXRvci5hY3RpdmUgPSBmYWxzZTtcclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblxyXG5cdFx0dGhpcy5pbmRpY2F0b3JzID0gWy4uLnRoaXMuaW5kaWNhdG9yc107XHJcblx0XHR0aGlzLnJlZi5kZXRlY3RDaGFuZ2VzKCk7XHJcblx0fVxyXG5cclxuXHQvL1xyXG5cdC8vIHByaXZhdGUgIGZ1bmN0aW9uc1xyXG5cdC8vXHJcblxyXG5cdC8vIHBvc2l0aW9uIGZvb3RlclxyXG5cdHByaXZhdGUgY2FsY3VsYXRlRm9vcnRlck1hcmdpblRvcChwYWdlSGVpZ2h0OiBudW1iZXIpOiBudW1iZXIge1xyXG5cdFx0cmV0dXJuIHBhZ2VIZWlnaHQgLSAocGFnZUhlaWdodCAvIDYpO1xyXG5cdH1cclxuXHJcblx0Ly8gZm9vdGVyIHN0dWZmXHJcblx0cHJpdmF0ZSBidWlsZEZvb3RlcihwYWdlQ291bnQ6IG51bWJlciA9IDUpOiB2b2lkIHtcclxuXHJcblx0XHRjb25zdCBmb290ZXJTZWN0aW9uID0gKDxTdGFja0xheW91dD50aGlzLmZvb3Rlci5uYXRpdmVFbGVtZW50KTtcclxuXHRcdGZvb3RlclNlY3Rpb24uaG9yaXpvbnRhbEFsaWdubWVudCA9ICdjZW50ZXInO1xyXG5cdFx0Zm9vdGVyU2VjdGlvbi5vcmllbnRhdGlvbiA9ICdob3Jpem9udGFsJztcclxuXHRcdGZvb3RlclNlY3Rpb24ubWFyZ2luVG9wID0gdGhpcy5mb290ZXJNYXJnaW5Ub3A7XHJcblx0XHRmb290ZXJTZWN0aW9uLmhlaWdodCA9IHRoaXMuRk9PVEVSX0hFSUdIVDtcclxuXHRcdGZvb3RlclNlY3Rpb24ud2lkdGggPSB0aGlzLnBhZ2VXaWR0aDtcclxuXHJcblx0XHRpZiAoYXBwLmlvcykge1xyXG5cdFx0XHRmb290ZXJTZWN0aW9uLmNsaXBUb0JvdW5kcyA9IGZhbHNlO1xyXG5cdFx0fVxyXG5cclxuXHRcdGxldCBpbmRleCA9IDA7XHJcblx0XHR0aGlzLmluZGljYXRvcnMgPSBbXTtcclxuXHRcdHdoaWxlIChpbmRleCA8IHBhZ2VDb3VudCkge1xyXG5cdFx0XHR0aGlzLmluZGljYXRvcnMucHVzaCh7IGFjdGl2ZTogZmFsc2UgfSk7XHJcblx0XHRcdGluZGV4Kys7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRwcml2YXRlIHNldHVwUGFuZWwoc2xpZGU6IElTbGlkZU1hcCkge1xyXG5cdFx0dGhpcy5kaXJlY3Rpb24gPSBkaXJlY3Rpb24ubm9uZTtcclxuXHRcdHRoaXMudHJhbnNpdGlvbmluZyA9IGZhbHNlO1xyXG5cdFx0dGhpcy5jdXJyZW50U2xpZGUuc2xpZGUubGF5b3V0Lm9mZigncGFuJyk7XHJcblx0XHR0aGlzLmN1cnJlbnRTbGlkZSA9IHNsaWRlO1xyXG5cclxuXHRcdC8vIHNldHMgdXAgZWFjaCBzbGlkZSBzbyB0aGF0IHRoZXkgYXJlIHBvc2l0aW9uZWQgdG8gdHJhbnNpdGlvbiBlaXRoZXIgd2F5LlxyXG5cdFx0dGhpcy5wb3NpdGlvblNsaWRlcyh0aGlzLmN1cnJlbnRTbGlkZSk7XHJcblxyXG5cdFx0Ly9pZiAodGhpcy5kaXNhYmxlUGFuID09PSBmYWxzZSkge1xyXG5cdFx0dGhpcy5hcHBseVN3aXBlKHRoaXMucGFnZVdpZHRoKTtcclxuXHRcdC8vfVxyXG5cclxuXHRcdGlmICh0aGlzLnBhZ2VJbmRpY2F0b3JzKSB7XHJcblx0XHRcdHRoaXMuc2V0QWN0aXZlUGFnZUluZGljYXRvcih0aGlzLmN1cnJlbnRTbGlkZS5pbmRleCk7XHJcblx0XHR9XHJcblxyXG5cdFx0dGhpcy5jaGFuZ2VkLm5leHQodGhpcy5jdXJyZW50U2xpZGUuaW5kZXgpO1xyXG5cclxuXHRcdGlmICh0aGlzLmN1cnJlbnRTbGlkZS5pbmRleCA9PT0gdGhpcy5zbGlkZXMubGVuZ3RoIC0gMSkge1xyXG5cdFx0XHR0aGlzLmZpbmlzaGVkLm5leHQobnVsbCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRwcml2YXRlIHBvc2l0aW9uU2xpZGVzKHNsaWRlOiBJU2xpZGVNYXApIHtcclxuXHRcdC8vIHNldHMgdXAgZWFjaCBzbGlkZSBzbyB0aGF0IHRoZXkgYXJlIHBvc2l0aW9uZWQgdG8gdHJhbnNpdGlvbiBlaXRoZXIgd2F5LlxyXG5cdFx0aWYgKHNsaWRlLmxlZnQgIT0gbnVsbCAmJiBzbGlkZS5sZWZ0LnNsaWRlICE9IG51bGwpIHtcclxuXHRcdFx0c2xpZGUubGVmdC5zbGlkZS5sYXlvdXQudHJhbnNsYXRlWCA9IC10aGlzLnBhZ2VXaWR0aCAqIDI7XHJcblx0XHR9XHJcblx0XHRzbGlkZS5zbGlkZS5sYXlvdXQudHJhbnNsYXRlWCA9IC10aGlzLnBhZ2VXaWR0aDtcclxuXHRcdGlmIChzbGlkZS5yaWdodCAhPSBudWxsICYmIHNsaWRlLnJpZ2h0LnNsaWRlICE9IG51bGwpIHtcclxuXHRcdFx0c2xpZGUucmlnaHQuc2xpZGUubGF5b3V0LnRyYW5zbGF0ZVggPSAwO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0cHJpdmF0ZSBzaG93UmlnaHRTbGlkZShzbGlkZU1hcDogSVNsaWRlTWFwLCBvZmZzZXQ6IG51bWJlciA9IHRoaXMucGFnZVdpZHRoLCBlbmRpbmdWZWxvY2l0eTogbnVtYmVyID0gMzIsIGR1cmF0aW9uOiBudW1iZXIgPSAzMDApOiBBbmltYXRpb25Nb2R1bGUuQW5pbWF0aW9uUHJvbWlzZSB7XHJcblx0XHRsZXQgYW5pbWF0aW9uRHVyYXRpb246IG51bWJlcjtcclxuXHRcdGFuaW1hdGlvbkR1cmF0aW9uID0gZHVyYXRpb247IC8vIGRlZmF1bHQgdmFsdWVcclxuXHJcblx0XHRsZXQgdHJhbnNpdGlvbiA9IG5ldyBBcnJheSgpO1xyXG5cclxuXHRcdHRyYW5zaXRpb24ucHVzaCh7XHJcblx0XHRcdHRhcmdldDogc2xpZGVNYXAucmlnaHQuc2xpZGUubGF5b3V0LFxyXG5cdFx0XHR0cmFuc2xhdGU6IHsgeDogLXRoaXMucGFnZVdpZHRoLCB5OiAwIH0sXHJcblx0XHRcdGR1cmF0aW9uOiBhbmltYXRpb25EdXJhdGlvbixcclxuXHRcdFx0Y3VydmU6IEFuaW1hdGlvbkN1cnZlLmVhc2VPdXRcclxuXHRcdH0pO1xyXG5cdFx0dHJhbnNpdGlvbi5wdXNoKHtcclxuXHRcdFx0dGFyZ2V0OiBzbGlkZU1hcC5zbGlkZS5sYXlvdXQsXHJcblx0XHRcdHRyYW5zbGF0ZTogeyB4OiAtdGhpcy5wYWdlV2lkdGggKiAyLCB5OiAwIH0sXHJcblx0XHRcdGR1cmF0aW9uOiBhbmltYXRpb25EdXJhdGlvbixcclxuXHRcdFx0Y3VydmU6IEFuaW1hdGlvbkN1cnZlLmVhc2VPdXRcclxuXHRcdH0pO1xyXG5cdFx0bGV0IGFuaW1hdGlvblNldCA9IG5ldyBBbmltYXRpb25Nb2R1bGUuQW5pbWF0aW9uKHRyYW5zaXRpb24sIGZhbHNlKTtcclxuXHJcblx0XHRyZXR1cm4gYW5pbWF0aW9uU2V0LnBsYXkoKTtcclxuXHR9XHJcblxyXG5cdHByaXZhdGUgc2hvd0xlZnRTbGlkZShzbGlkZU1hcDogSVNsaWRlTWFwLCBvZmZzZXQ6IG51bWJlciA9IHRoaXMucGFnZVdpZHRoLCBlbmRpbmdWZWxvY2l0eTogbnVtYmVyID0gMzIsIGR1cmF0aW9uOiBudW1iZXIgPSAzMDApOiBBbmltYXRpb25Nb2R1bGUuQW5pbWF0aW9uUHJvbWlzZSB7XHJcblxyXG5cdFx0bGV0IGFuaW1hdGlvbkR1cmF0aW9uOiBudW1iZXI7XHJcblx0XHRhbmltYXRpb25EdXJhdGlvbiA9IGR1cmF0aW9uOyAvLyBkZWZhdWx0IHZhbHVlXHJcblx0XHRsZXQgdHJhbnNpdGlvbiA9IG5ldyBBcnJheSgpO1xyXG5cclxuXHRcdHRyYW5zaXRpb24ucHVzaCh7XHJcblx0XHRcdHRhcmdldDogc2xpZGVNYXAubGVmdC5zbGlkZS5sYXlvdXQsXHJcblx0XHRcdHRyYW5zbGF0ZTogeyB4OiAtdGhpcy5wYWdlV2lkdGgsIHk6IDAgfSxcclxuXHRcdFx0ZHVyYXRpb246IGFuaW1hdGlvbkR1cmF0aW9uLFxyXG5cdFx0XHRjdXJ2ZTogQW5pbWF0aW9uQ3VydmUuZWFzZU91dFxyXG5cdFx0fSk7XHJcblx0XHR0cmFuc2l0aW9uLnB1c2goe1xyXG5cdFx0XHR0YXJnZXQ6IHNsaWRlTWFwLnNsaWRlLmxheW91dCxcclxuXHRcdFx0dHJhbnNsYXRlOiB7IHg6IDAsIHk6IDAgfSxcclxuXHRcdFx0ZHVyYXRpb246IGFuaW1hdGlvbkR1cmF0aW9uLFxyXG5cdFx0XHRjdXJ2ZTogQW5pbWF0aW9uQ3VydmUuZWFzZU91dFxyXG5cdFx0fSk7XHJcblx0XHRsZXQgYW5pbWF0aW9uU2V0ID0gbmV3IEFuaW1hdGlvbk1vZHVsZS5BbmltYXRpb24odHJhbnNpdGlvbiwgZmFsc2UpO1xyXG5cclxuXHRcdHJldHVybiBhbmltYXRpb25TZXQucGxheSgpO1xyXG5cclxuXHR9XHJcblxyXG5cdHB1YmxpYyBhcHBseVN3aXBlKHBhZ2VXaWR0aDogbnVtYmVyKTogdm9pZCB7XHJcblx0XHRsZXQgcHJldmlvdXNEZWx0YSA9IC0xOyAvL2hhY2sgdG8gZ2V0IGFyb3VuZCBpb3MgZmlyaW5nIHBhbiBldmVudCBhZnRlciByZWxlYXNlXHJcblx0XHRsZXQgZW5kaW5nVmVsb2NpdHkgPSAwO1xyXG5cdFx0bGV0IHN0YXJ0VGltZSwgZGVsdGFUaW1lO1xyXG5cdFx0dGhpcy50cmFuc2l0aW9uaW5nID0gZmFsc2U7XHJcblxyXG5cdFx0dGhpcy5jdXJyZW50U2xpZGUuc2xpZGUubGF5b3V0Lm9uKCd0YXAnLCAoYXJnczogZ2VzdHVyZXMuR2VzdHVyZUV2ZW50RGF0YSk6IHZvaWQgPT4ge1xyXG5cdFx0XHR0aGlzLnRhcC5uZXh0KGFyZ3MpO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0dGhpcy5jdXJyZW50U2xpZGUuc2xpZGUubGF5b3V0Lm9uKCdwYW4nLCAoYXJnczogZ2VzdHVyZXMuUGFuR2VzdHVyZUV2ZW50RGF0YSk6IHZvaWQgPT4ge1xyXG5cdFx0XHRpZiAoYXJncy5zdGF0ZSA9PT0gZ2VzdHVyZXMuR2VzdHVyZVN0YXRlVHlwZXMuYmVnYW4pIHtcclxuXHRcdFx0XHRzdGFydFRpbWUgPSBEYXRlLm5vdygpO1xyXG5cdFx0XHRcdHByZXZpb3VzRGVsdGEgPSAwO1xyXG5cdFx0XHRcdGVuZGluZ1ZlbG9jaXR5ID0gMjUwO1xyXG5cclxuXHRcdFx0XHQvL3RoaXMudHJpZ2dlclN0YXJ0RXZlbnQoKTtcclxuXHRcdFx0fSBlbHNlIGlmIChhcmdzLnN0YXRlID09PSBnZXN0dXJlcy5HZXN0dXJlU3RhdGVUeXBlcy5lbmRlZCkge1xyXG5cdFx0XHRcdGRlbHRhVGltZSA9IERhdGUubm93KCkgLSBzdGFydFRpbWU7XHJcblx0XHRcdFx0Ly8gaWYgdmVsb2NpdHlTY3JvbGxpbmcgaXMgZW5hYmxlZCB0aGVuIGNhbGN1bGF0ZSB0aGUgdmVsb2NpdHR5XHJcblxyXG5cdFx0XHRcdC8vIHN3aXBpbmcgbGVmdCB0byByaWdodC5cclxuXHRcdFx0XHRpZiAoYXJncy5kZWx0YVggPiAocGFnZVdpZHRoIC8gMykpIHtcclxuXHRcdFx0XHRcdGlmICh0aGlzLmhhc1ByZXZpb3VzKSB7XHJcblx0XHRcdFx0XHRcdHRoaXMudHJhbnNpdGlvbmluZyA9IHRydWU7XHJcblx0XHRcdFx0XHRcdHRoaXMuc2hvd0xlZnRTbGlkZSh0aGlzLmN1cnJlbnRTbGlkZSwgYXJncy5kZWx0YVgsIGVuZGluZ1ZlbG9jaXR5KS50aGVuKCgpID0+IHtcclxuXHRcdFx0XHRcdFx0XHR0aGlzLnNldHVwUGFuZWwodGhpcy5jdXJyZW50U2xpZGUubGVmdCk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdC8vdGhpcy50cmlnZ2VyQ2hhbmdlRXZlbnRMZWZ0VG9SaWdodCgpO1xyXG5cdFx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdC8vV2UncmUgYXQgdGhlIHN0YXJ0XHJcblx0XHRcdFx0XHRcdC8vTm90aWZ5IG5vIG1vcmUgc2xpZGVzXHJcblx0XHRcdFx0XHRcdC8vdGhpcy50cmlnZ2VyQ2FuY2VsRXZlbnQoY2FuY2VsbGF0aW9uUmVhc29uLm5vUHJldlNsaWRlcyk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdC8vIHN3aXBpbmcgcmlnaHQgdG8gbGVmdFxyXG5cdFx0XHRcdGVsc2UgaWYgKGFyZ3MuZGVsdGFYIDwgKC1wYWdlV2lkdGggLyAzKSkge1xyXG5cdFx0XHRcdFx0aWYgKHRoaXMuaGFzTmV4dCkge1xyXG5cdFx0XHRcdFx0XHR0aGlzLnRyYW5zaXRpb25pbmcgPSB0cnVlO1xyXG5cdFx0XHRcdFx0XHR0aGlzLnNob3dSaWdodFNsaWRlKHRoaXMuY3VycmVudFNsaWRlLCBhcmdzLmRlbHRhWCwgZW5kaW5nVmVsb2NpdHkpLnRoZW4oKCkgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdHRoaXMuc2V0dXBQYW5lbCh0aGlzLmN1cnJlbnRTbGlkZS5yaWdodCk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdC8vIE5vdGlmeSBjaGFuZ2VkXHJcblx0XHRcdFx0XHRcdFx0Ly90aGlzLnRyaWdnZXJDaGFuZ2VFdmVudFJpZ2h0VG9MZWZ0KCk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdGlmICghdGhpcy5oYXNOZXh0KSB7XHJcblx0XHRcdFx0XHRcdFx0XHQvLyBOb3RpZnkgZmluc2loZWRcclxuXHRcdFx0XHRcdFx0XHRcdC8vIHRoaXMubm90aWZ5KHtcclxuXHRcdFx0XHRcdFx0XHRcdC8vIFx0ZXZlbnROYW1lOiBTbGlkZUNvbnRhaW5lci5GSU5JU0hFRF9FVkVOVCxcclxuXHRcdFx0XHRcdFx0XHRcdC8vIFx0b2JqZWN0OiB0aGlzXHJcblx0XHRcdFx0XHRcdFx0XHQvLyB9KTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0Ly8gV2UncmUgYXQgdGhlIGVuZFxyXG5cdFx0XHRcdFx0XHQvLyBOb3RpZnkgbm8gbW9yZSBzbGlkZXNcclxuXHRcdFx0XHRcdFx0Ly90aGlzLnRyaWdnZXJDYW5jZWxFdmVudChjYW5jZWxsYXRpb25SZWFzb24ubm9Nb3JlU2xpZGVzKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdHJldHVybjtcclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdGlmICh0aGlzLnRyYW5zaXRpb25pbmcgPT09IGZhbHNlKSB7XHJcblx0XHRcdFx0XHQvL05vdGlmeSBjYW5jZWxsZWRcclxuXHRcdFx0XHRcdC8vdGhpcy50cmlnZ2VyQ2FuY2VsRXZlbnQoY2FuY2VsbGF0aW9uUmVhc29uLnVzZXIpO1xyXG5cdFx0XHRcdFx0dGhpcy50cmFuc2l0aW9uaW5nID0gdHJ1ZTtcclxuXHRcdFx0XHRcdHRoaXMuY3VycmVudFNsaWRlLnNsaWRlLmxheW91dC5hbmltYXRlKHtcclxuXHRcdFx0XHRcdFx0dHJhbnNsYXRlOiB7IHg6IC10aGlzLnBhZ2VXaWR0aCwgeTogMCB9LFxyXG5cdFx0XHRcdFx0XHRkdXJhdGlvbjogMjAwLFxyXG5cdFx0XHRcdFx0XHRjdXJ2ZTogQW5pbWF0aW9uQ3VydmUuZWFzZU91dFxyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRpZiAodGhpcy5oYXNOZXh0KSB7XHJcblx0XHRcdFx0XHRcdHRoaXMuY3VycmVudFNsaWRlLnJpZ2h0LnNsaWRlLmxheW91dC5hbmltYXRlKHtcclxuXHRcdFx0XHRcdFx0XHR0cmFuc2xhdGU6IHsgeDogMCwgeTogMCB9LFxyXG5cdFx0XHRcdFx0XHRcdGR1cmF0aW9uOiAyMDAsXHJcblx0XHRcdFx0XHRcdFx0Y3VydmU6IEFuaW1hdGlvbkN1cnZlLmVhc2VPdXRcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHRcdGlmIChhcHAuaW9zKSAvL2ZvciBzb21lIHJlYXNvbiBpIGhhdmUgdG8gc2V0IHRoZXNlIGluIGlvcyBvciB0aGVyZSBpcyBzb21lIHNvcnQgb2YgYm91bmNlIGJhY2suXHJcblx0XHRcdFx0XHRcdFx0dGhpcy5jdXJyZW50U2xpZGUucmlnaHQuc2xpZGUubGF5b3V0LnRyYW5zbGF0ZVggPSAwO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYgKHRoaXMuaGFzUHJldmlvdXMpIHtcclxuXHRcdFx0XHRcdFx0dGhpcy5jdXJyZW50U2xpZGUubGVmdC5zbGlkZS5sYXlvdXQuYW5pbWF0ZSh7XHJcblx0XHRcdFx0XHRcdFx0dHJhbnNsYXRlOiB7IHg6IC10aGlzLnBhZ2VXaWR0aCAqIDIsIHk6IDAgfSxcclxuXHRcdFx0XHRcdFx0XHRkdXJhdGlvbjogMjAwLFxyXG5cdFx0XHRcdFx0XHRcdGN1cnZlOiBBbmltYXRpb25DdXJ2ZS5lYXNlT3V0XHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHRpZiAoYXBwLmlvcylcclxuXHRcdFx0XHRcdFx0XHR0aGlzLmN1cnJlbnRTbGlkZS5sZWZ0LnNsaWRlLmxheW91dC50cmFuc2xhdGVYID0gLXRoaXMucGFnZVdpZHRoO1xyXG5cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmIChhcHAuaW9zKVxyXG5cdFx0XHRcdFx0XHR0aGlzLmN1cnJlbnRTbGlkZS5zbGlkZS5sYXlvdXQudHJhbnNsYXRlWCA9IC10aGlzLnBhZ2VXaWR0aDtcclxuXHJcblx0XHRcdFx0XHR0aGlzLnRyYW5zaXRpb25pbmcgPSBmYWxzZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0aWYgKCF0aGlzLnRyYW5zaXRpb25pbmdcclxuXHRcdFx0XHRcdCYmIHByZXZpb3VzRGVsdGEgIT09IGFyZ3MuZGVsdGFYXHJcblx0XHRcdFx0XHQmJiBhcmdzLmRlbHRhWCAhPSBudWxsXHJcblx0XHRcdFx0XHQmJiBhcmdzLmRlbHRhWCA8IDApIHtcclxuXHJcblx0XHRcdFx0XHRpZiAodGhpcy5oYXNOZXh0KSB7XHJcblx0XHRcdFx0XHRcdHRoaXMuZGlyZWN0aW9uID0gZGlyZWN0aW9uLmxlZnQ7XHJcblx0XHRcdFx0XHRcdHRoaXMuY3VycmVudFNsaWRlLnNsaWRlLmxheW91dC50cmFuc2xhdGVYID0gYXJncy5kZWx0YVggLSB0aGlzLnBhZ2VXaWR0aDtcclxuXHRcdFx0XHRcdFx0dGhpcy5jdXJyZW50U2xpZGUucmlnaHQuc2xpZGUubGF5b3V0LnRyYW5zbGF0ZVggPSBhcmdzLmRlbHRhWDtcclxuXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSBlbHNlIGlmICghdGhpcy50cmFuc2l0aW9uaW5nXHJcblx0XHRcdFx0XHQmJiBwcmV2aW91c0RlbHRhICE9PSBhcmdzLmRlbHRhWFxyXG5cdFx0XHRcdFx0JiYgYXJncy5kZWx0YVggIT0gbnVsbFxyXG5cdFx0XHRcdFx0JiYgYXJncy5kZWx0YVggPiAwKSB7XHJcblxyXG5cdFx0XHRcdFx0aWYgKHRoaXMuaGFzUHJldmlvdXMpIHtcclxuXHRcdFx0XHRcdFx0dGhpcy5kaXJlY3Rpb24gPSBkaXJlY3Rpb24ucmlnaHQ7XHJcblx0XHRcdFx0XHRcdHRoaXMuY3VycmVudFNsaWRlLnNsaWRlLmxheW91dC50cmFuc2xhdGVYID0gYXJncy5kZWx0YVggLSB0aGlzLnBhZ2VXaWR0aDtcclxuXHRcdFx0XHRcdFx0dGhpcy5jdXJyZW50U2xpZGUubGVmdC5zbGlkZS5sYXlvdXQudHJhbnNsYXRlWCA9IC0odGhpcy5wYWdlV2lkdGggKiAyKSArIGFyZ3MuZGVsdGFYO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aWYgKGFyZ3MuZGVsdGFYICE9PSAwKSB7XHJcblx0XHRcdFx0XHRwcmV2aW91c0RlbHRhID0gYXJncy5kZWx0YVg7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fVxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRwcml2YXRlIGJ1aWxkU2xpZGVNYXAoc2xpZGVzOiBTbGlkZUNvbXBvbmVudFtdKSB7XHJcblx0XHR0aGlzLl9zbGlkZU1hcCA9IFtdO1xyXG5cdFx0c2xpZGVzLmZvckVhY2goKHNsaWRlOiBTbGlkZUNvbXBvbmVudCwgaW5kZXg6IG51bWJlcikgPT4ge1xyXG5cdFx0XHR0aGlzLl9zbGlkZU1hcC5wdXNoKHtcclxuXHRcdFx0XHRzbGlkZTogc2xpZGUsXHJcblx0XHRcdFx0aW5kZXg6IGluZGV4LFxyXG5cdFx0XHR9KTtcclxuXHRcdH0pO1xyXG5cdFx0dGhpcy5fc2xpZGVNYXAuZm9yRWFjaCgobWFwcGluZzogSVNsaWRlTWFwLCBpbmRleDogbnVtYmVyKSA9PiB7XHJcblx0XHRcdGlmICh0aGlzLl9zbGlkZU1hcFtpbmRleCAtIDFdICE9IG51bGwpXHJcblx0XHRcdFx0bWFwcGluZy5sZWZ0ID0gdGhpcy5fc2xpZGVNYXBbaW5kZXggLSAxXTtcclxuXHRcdFx0aWYgKHRoaXMuX3NsaWRlTWFwW2luZGV4ICsgMV0gIT0gbnVsbClcclxuXHRcdFx0XHRtYXBwaW5nLnJpZ2h0ID0gdGhpcy5fc2xpZGVNYXBbaW5kZXggKyAxXTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdGlmICh0aGlzLmxvb3ApIHtcclxuXHRcdFx0dGhpcy5fc2xpZGVNYXBbMF0ubGVmdCA9IHRoaXMuX3NsaWRlTWFwW3RoaXMuX3NsaWRlTWFwLmxlbmd0aCAtIDFdO1xyXG5cdFx0XHR0aGlzLl9zbGlkZU1hcFt0aGlzLl9zbGlkZU1hcC5sZW5ndGggLSAxXS5yaWdodCA9IHRoaXMuX3NsaWRlTWFwWzBdO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHRoaXMuX3NsaWRlTWFwWzBdO1xyXG5cdH1cclxuXHJcblx0cHVibGljIEdvVG9TbGlkZShudW06IG51bWJlciwgdHJhdmVyc2VEdXJhdGlvbjogbnVtYmVyID0gNTAsIGxhbmRpbmdEdXJhdGlvbjogbnVtYmVyID0gMjAwKTogdm9pZCB7XHJcblx0XHRpZiAodGhpcy5jdXJyZW50U2xpZGUuaW5kZXggPT0gbnVtKSByZXR1cm47XHJcblxyXG5cdFx0dmFyIGR1cmF0aW9uOiBudW1iZXIgPSBsYW5kaW5nRHVyYXRpb247XHJcblx0XHRpZiAoTWF0aC5hYnMobnVtIC0gdGhpcy5jdXJyZW50U2xpZGUuaW5kZXgpICE9IDEpIGR1cmF0aW9uID0gdHJhdmVyc2VEdXJhdGlvbjtcclxuXHJcblx0XHRpZiAodGhpcy5jdXJyZW50U2xpZGUuaW5kZXggPCBudW0pXHJcblx0XHRcdHRoaXMubmV4dFNsaWRlKGR1cmF0aW9uKS50aGVuKCgpID0+IHRoaXMuR29Ub1NsaWRlKG51bSkpO1xyXG5cdFx0ZWxzZVxyXG5cdFx0XHR0aGlzLnByZXZpb3VzU2xpZGUoZHVyYXRpb24pLnRoZW4oKCkgPT4gdGhpcy5Hb1RvU2xpZGUobnVtKSk7XHJcblx0fVxyXG5cclxuXHRwdWJsaWMgbmV4dFNsaWRlKGR1cmF0aW9uPzogbnVtYmVyKTogUHJvbWlzZTxhbnk+IHtcclxuXHRcdGlmICghdGhpcy5oYXNOZXh0KSB7XHJcblx0XHRcdC8vdGhpcy50cmlnZ2VyQ2FuY2VsRXZlbnQoY2FuY2VsbGF0aW9uUmVhc29uLm5vTW9yZVNsaWRlcyk7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHJcblx0XHR0aGlzLmRpcmVjdGlvbiA9IGRpcmVjdGlvbi5sZWZ0O1xyXG5cdFx0dGhpcy50cmFuc2l0aW9uaW5nID0gdHJ1ZTtcclxuXHRcdC8vXHR0aGlzLnRyaWdnZXJTdGFydEV2ZW50KCk7XHJcblx0XHRyZXR1cm4gdGhpcy5zaG93UmlnaHRTbGlkZSh0aGlzLmN1cnJlbnRTbGlkZSwgbnVsbCwgbnVsbCwgZHVyYXRpb24pLnRoZW4oKCkgPT4ge1xyXG5cdFx0XHR0aGlzLnNldHVwUGFuZWwodGhpcy5jdXJyZW50U2xpZGUucmlnaHQpO1xyXG5cdFx0XHQvL3RoaXMudHJpZ2dlckNoYW5nZUV2ZW50UmlnaHRUb0xlZnQoKTtcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblxyXG5cdHB1YmxpYyBwcmV2aW91c1NsaWRlKGR1cmF0aW9uPzogbnVtYmVyKTogUHJvbWlzZTxhbnk+IHtcclxuXHRcdGlmICghdGhpcy5oYXNQcmV2aW91cykge1xyXG5cdFx0XHQvL3RoaXMudHJpZ2dlckNhbmNlbEV2ZW50KGNhbmNlbGxhdGlvblJlYXNvbi5ub1ByZXZTbGlkZXMpO1xyXG5cdFx0XHRyZXR1cm47XHJcblx0XHR9XHJcblxyXG5cdFx0dGhpcy5kaXJlY3Rpb24gPSBkaXJlY3Rpb24ucmlnaHQ7XHJcblx0XHR0aGlzLnRyYW5zaXRpb25pbmcgPSB0cnVlO1xyXG5cdFx0Ly90aGlzLnRyaWdnZXJTdGFydEV2ZW50KCk7XHJcblx0XHRyZXR1cm4gdGhpcy5zaG93TGVmdFNsaWRlKHRoaXMuY3VycmVudFNsaWRlLCBudWxsLCBudWxsLCBkdXJhdGlvbikudGhlbigoKSA9PiB7XHJcblx0XHRcdHRoaXMuc2V0dXBQYW5lbCh0aGlzLmN1cnJlbnRTbGlkZS5sZWZ0KTtcclxuXHJcblx0XHRcdC8vdGhpcy50cmlnZ2VyQ2hhbmdlRXZlbnRMZWZ0VG9SaWdodCgpO1xyXG5cdFx0fSk7XHJcblx0fVxyXG59XHJcbiJdfQ==