"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var fonticon_service_1 = require("../services/fonticon.service");
var TNSFontIconPipe = /** @class */ (function () {
    function TNSFontIconPipe(fonticon, _ref) {
        this.fonticon = fonticon;
        this._ref = _ref;
    }
    TNSFontIconPipe.prototype.transform = function (className, args) {
        var _this = this;
        if (!this._collectionName)
            this._collectionName = getCollectionName(className, args);
        if (!this._value || (this.fonticon.css && this.fonticon.css[this._collectionName] && this._value !== this.fonticon.css[this._collectionName][className])) {
            // only subscribe if value is changing
            // if there is a subscription to iconSub, clean it
            this._dispose();
            this._iconSub = this.fonticon.filesLoaded.subscribe(function (data) {
                if (data && data[_this._collectionName] && data[_this._collectionName][className]) {
                    if (_this._value !== data[_this._collectionName][className]) {
                        // only markForCheck if value has changed
                        _this._value = data[_this._collectionName][className];
                        _this._ref.markForCheck();
                        _this._dispose();
                    }
                }
            });
        }
        return this._value;
    };
    TNSFontIconPipe.prototype._dispose = function () {
        if (this._iconSub) {
            this._iconSub.unsubscribe();
            this._iconSub = undefined;
        }
    };
    TNSFontIconPipe.prototype.ngOnDestroy = function () {
        this._dispose();
    };
    TNSFontIconPipe = __decorate([
        core_1.Pipe({
            name: 'fonticon',
            pure: false
        }),
        __metadata("design:paramtypes", [fonticon_service_1.TNSFontIconService, core_1.ChangeDetectorRef])
    ], TNSFontIconPipe);
    return TNSFontIconPipe;
}());
exports.TNSFontIconPipe = TNSFontIconPipe;
// Can be used for optimal performance, however requires usage of Observable values with the async pipe, see demo (app.ts) for example
var TNSFontIconPurePipe = /** @class */ (function () {
    function TNSFontIconPurePipe(fonticon) {
        this.fonticon = fonticon;
    }
    TNSFontIconPurePipe.prototype.transform = function (className, args) {
        if (!this._collectionName)
            this._collectionName = getCollectionName(className, args);
        // console.log(`fonticonPure: ${className}`);
        if (this.fonticon.css && this.fonticon.css[this._collectionName]) {
            return this.fonticon.css[this._collectionName][className];
        }
        else {
            return '';
        }
    };
    TNSFontIconPurePipe = __decorate([
        core_1.Pipe({
            name: 'fonticonPure'
        }),
        __metadata("design:paramtypes", [fonticon_service_1.TNSFontIconService])
    ], TNSFontIconPurePipe);
    return TNSFontIconPurePipe;
}());
exports.TNSFontIconPurePipe = TNSFontIconPurePipe;
function getCollectionName(className, args) {
    if (args && args.length && args[0] !== null) {
        return args[0];
    }
    else if (className && className.indexOf('-') > -1) {
        // derive from classname
        return className.split('-')[0];
    }
    else {
        return '';
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9udGljb24ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZvbnRpY29uLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBZ0Y7QUFFaEYsaUVBQWdFO0FBTWhFO0lBS0UseUJBQW9CLFFBQTRCLEVBQVUsSUFBdUI7UUFBN0QsYUFBUSxHQUFSLFFBQVEsQ0FBb0I7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFtQjtJQUFJLENBQUM7SUFFdEYsbUNBQVMsR0FBVCxVQUFVLFNBQWlCLEVBQUUsSUFBVztRQUF4QyxpQkFzQkM7UUFyQkMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxlQUFlLEdBQUcsaUJBQWlCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRTVELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6SixzQ0FBc0M7WUFDdEMsa0RBQWtEO1lBQ2xELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUVoQixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQVM7Z0JBQzVELEVBQUUsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNoRixFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMxRCx5Q0FBeUM7d0JBQ3pDLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDcEQsS0FBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQzt3QkFDekIsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUNsQixDQUFDO2dCQUNILENBQUM7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUNyQixDQUFDO0lBRUQsa0NBQVEsR0FBUjtRQUNFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDNUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7UUFDNUIsQ0FBQztJQUNILENBQUM7SUFFRCxxQ0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUF4Q1UsZUFBZTtRQUozQixXQUFJLENBQUM7WUFDSixJQUFJLEVBQUUsVUFBVTtZQUNoQixJQUFJLEVBQUUsS0FBSztTQUNaLENBQUM7eUNBTThCLHFDQUFrQixFQUFnQix3QkFBaUI7T0FMdEUsZUFBZSxDQXlDM0I7SUFBRCxzQkFBQztDQUFBLEFBekNELElBeUNDO0FBekNZLDBDQUFlO0FBMkM1QixzSUFBc0k7QUFJdEk7SUFHRSw2QkFBb0IsUUFBNEI7UUFBNUIsYUFBUSxHQUFSLFFBQVEsQ0FBb0I7SUFBSSxDQUFDO0lBRXJELHVDQUFTLEdBQVQsVUFBVSxTQUFpQixFQUFFLElBQVc7UUFDdEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxlQUFlLEdBQUcsaUJBQWlCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRTVELDZDQUE2QztRQUM3QyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pFLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDNUQsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sTUFBTSxDQUFDLEVBQUUsQ0FBQztRQUNaLENBQUM7SUFDSCxDQUFDO0lBZlUsbUJBQW1CO1FBSC9CLFdBQUksQ0FBQztZQUNKLElBQUksRUFBRSxjQUFjO1NBQ3JCLENBQUM7eUNBSThCLHFDQUFrQjtPQUhyQyxtQkFBbUIsQ0FnQi9CO0lBQUQsMEJBQUM7Q0FBQSxBQWhCRCxJQWdCQztBQWhCWSxrREFBbUI7QUFrQmhDLDJCQUEyQixTQUFpQixFQUFFLElBQVc7SUFDdkQsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDNUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNqQixDQUFDO0lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNwRCx3QkFBd0I7UUFDeEIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUFDLElBQUksQ0FBQyxDQUFDO1FBQ04sTUFBTSxDQUFDLEVBQUUsQ0FBQztJQUNaLENBQUM7QUFDSCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtQaXBlLCBQaXBlVHJhbnNmb3JtLCBPbkRlc3Ryb3ksIENoYW5nZURldGVjdG9yUmVmfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuaW1wb3J0IHtUTlNGb250SWNvblNlcnZpY2V9IGZyb20gJy4uL3NlcnZpY2VzL2ZvbnRpY29uLnNlcnZpY2UnO1xuXG5AUGlwZSh7XG4gIG5hbWU6ICdmb250aWNvbicsXG4gIHB1cmU6IGZhbHNlXG59KVxuZXhwb3J0IGNsYXNzIFROU0ZvbnRJY29uUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0sIE9uRGVzdHJveSB7XG4gIHByaXZhdGUgX2NvbGxlY3Rpb25OYW1lOiBzdHJpbmc7XG4gIHByaXZhdGUgX3ZhbHVlOiAnJztcbiAgcHJpdmF0ZSBfaWNvblN1YjogYW55O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZm9udGljb246IFROU0ZvbnRJY29uU2VydmljZSwgcHJpdmF0ZSBfcmVmOiBDaGFuZ2VEZXRlY3RvclJlZikgeyB9XG5cbiAgdHJhbnNmb3JtKGNsYXNzTmFtZTogc3RyaW5nLCBhcmdzOiBhbnlbXSkge1xuICAgIGlmICghdGhpcy5fY29sbGVjdGlvbk5hbWUpXG4gICAgICB0aGlzLl9jb2xsZWN0aW9uTmFtZSA9IGdldENvbGxlY3Rpb25OYW1lKGNsYXNzTmFtZSwgYXJncyk7XG5cbiAgICBpZiAoIXRoaXMuX3ZhbHVlIHx8ICh0aGlzLmZvbnRpY29uLmNzcyAmJiB0aGlzLmZvbnRpY29uLmNzc1t0aGlzLl9jb2xsZWN0aW9uTmFtZV0gJiYgdGhpcy5fdmFsdWUgIT09IHRoaXMuZm9udGljb24uY3NzW3RoaXMuX2NvbGxlY3Rpb25OYW1lXVtjbGFzc05hbWVdKSkge1xuICAgICAgLy8gb25seSBzdWJzY3JpYmUgaWYgdmFsdWUgaXMgY2hhbmdpbmdcbiAgICAgIC8vIGlmIHRoZXJlIGlzIGEgc3Vic2NyaXB0aW9uIHRvIGljb25TdWIsIGNsZWFuIGl0XG4gICAgICB0aGlzLl9kaXNwb3NlKCk7XG5cbiAgICAgIHRoaXMuX2ljb25TdWIgPSB0aGlzLmZvbnRpY29uLmZpbGVzTG9hZGVkLnN1YnNjcmliZSgoZGF0YTogYW55KSA9PiB7XG4gICAgICAgIGlmIChkYXRhICYmIGRhdGFbdGhpcy5fY29sbGVjdGlvbk5hbWVdICYmIGRhdGFbdGhpcy5fY29sbGVjdGlvbk5hbWVdW2NsYXNzTmFtZV0pIHtcbiAgICAgICAgICBpZiAodGhpcy5fdmFsdWUgIT09IGRhdGFbdGhpcy5fY29sbGVjdGlvbk5hbWVdW2NsYXNzTmFtZV0pIHtcbiAgICAgICAgICAgIC8vIG9ubHkgbWFya0ZvckNoZWNrIGlmIHZhbHVlIGhhcyBjaGFuZ2VkXG4gICAgICAgICAgICB0aGlzLl92YWx1ZSA9IGRhdGFbdGhpcy5fY29sbGVjdGlvbk5hbWVdW2NsYXNzTmFtZV07XG4gICAgICAgICAgICB0aGlzLl9yZWYubWFya0ZvckNoZWNrKCk7ICBcbiAgICAgICAgICAgIHRoaXMuX2Rpc3Bvc2UoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0pOyAgXG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuX3ZhbHVlO1xuICB9XG5cbiAgX2Rpc3Bvc2UoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMuX2ljb25TdWIpIHtcbiAgICAgIHRoaXMuX2ljb25TdWIudW5zdWJzY3JpYmUoKTtcbiAgICAgIHRoaXMuX2ljb25TdWIgPSB1bmRlZmluZWQ7XG4gICAgfVxuICB9ICBcblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICB0aGlzLl9kaXNwb3NlKCk7XG4gIH0gIFxufVxuXG4vLyBDYW4gYmUgdXNlZCBmb3Igb3B0aW1hbCBwZXJmb3JtYW5jZSwgaG93ZXZlciByZXF1aXJlcyB1c2FnZSBvZiBPYnNlcnZhYmxlIHZhbHVlcyB3aXRoIHRoZSBhc3luYyBwaXBlLCBzZWUgZGVtbyAoYXBwLnRzKSBmb3IgZXhhbXBsZVxuQFBpcGUoe1xuICBuYW1lOiAnZm9udGljb25QdXJlJ1xufSlcbmV4cG9ydCBjbGFzcyBUTlNGb250SWNvblB1cmVQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XG4gIHByaXZhdGUgX2NvbGxlY3Rpb25OYW1lOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmb250aWNvbjogVE5TRm9udEljb25TZXJ2aWNlKSB7IH1cblxuICB0cmFuc2Zvcm0oY2xhc3NOYW1lOiBzdHJpbmcsIGFyZ3M6IGFueVtdKSB7XG4gICAgaWYgKCF0aGlzLl9jb2xsZWN0aW9uTmFtZSlcbiAgICAgIHRoaXMuX2NvbGxlY3Rpb25OYW1lID0gZ2V0Q29sbGVjdGlvbk5hbWUoY2xhc3NOYW1lLCBhcmdzKTtcbiAgICBcbiAgICAvLyBjb25zb2xlLmxvZyhgZm9udGljb25QdXJlOiAke2NsYXNzTmFtZX1gKTtcbiAgICBpZiAodGhpcy5mb250aWNvbi5jc3MgJiYgdGhpcy5mb250aWNvbi5jc3NbdGhpcy5fY29sbGVjdGlvbk5hbWVdKSB7XG4gICAgICByZXR1cm4gdGhpcy5mb250aWNvbi5jc3NbdGhpcy5fY29sbGVjdGlvbk5hbWVdW2NsYXNzTmFtZV07XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAnJztcbiAgICB9IFxuICB9IFxufVxuXG5mdW5jdGlvbiBnZXRDb2xsZWN0aW9uTmFtZShjbGFzc05hbWU6IHN0cmluZywgYXJnczogYW55W10pOiBzdHJpbmcge1xuICBpZiAoYXJncyAmJiBhcmdzLmxlbmd0aCAmJiBhcmdzWzBdICE9PSBudWxsKSB7XG4gICAgcmV0dXJuIGFyZ3NbMF07XG4gIH0gZWxzZSBpZiAoY2xhc3NOYW1lICYmIGNsYXNzTmFtZS5pbmRleE9mKCctJykgPiAtMSkge1xuICAgIC8vIGRlcml2ZSBmcm9tIGNsYXNzbmFtZVxuICAgIHJldHVybiBjbGFzc05hbWUuc3BsaXQoJy0nKVswXTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cbn1cbiJdfQ==