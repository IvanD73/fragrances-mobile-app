"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// angular
var core_1 = require("@angular/core");
// nativescript
var file_system_1 = require("file-system");
// libs
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
exports.USE_STORE = new core_1.OpaqueToken('USE_STORE');
var TNSFontIconService = /** @class */ (function () {
    function TNSFontIconService(config) {
        this.config = config;
        this.css = {}; // font icon collections containing maps of classnames to unicode
        this.filesLoaded = new BehaviorSubject_1.BehaviorSubject(null);
        this.loadCss();
    }
    TNSFontIconService_1 = TNSFontIconService;
    TNSFontIconService.prototype.loadCss = function () {
        var _this = this;
        var cnt = 0;
        var fontIconCollections = Object.keys(this.config);
        if (TNSFontIconService_1.debug) {
            console.log("Collections to load: " + fontIconCollections);
        }
        var initCollection = function () {
            _this._currentName = fontIconCollections[cnt];
            _this.css[_this._currentName] = {};
        };
        var loadFiles = function () {
            initCollection();
            if (cnt === fontIconCollections.length) {
                _this.filesLoaded.next(_this.css);
            }
            else {
                var fonts = _this.config;
                _this.loadFile(fonts[_this._currentName]).then(function () {
                    cnt++;
                    loadFiles();
                });
            }
        };
        loadFiles();
    };
    TNSFontIconService.prototype.loadFile = function (path) {
        var _this = this;
        if (TNSFontIconService_1.debug) {
            console.log("----------");
            console.log("Loading collection '" + this._currentName + "' from file: " + path);
        }
        var cssFile = file_system_1.knownFolders.currentApp().getFile(path);
        return new Promise(function (resolve, reject) {
            cssFile.readText().then(function (data) {
                _this.mapCss(data);
                resolve();
            }, function (err) {
                reject(err);
            });
        });
    };
    TNSFontIconService.prototype.mapCss = function (data) {
        var sets = data.split('}');
        var cleanValue = function (val) {
            var v = val.split('content:')[1].toLowerCase().replace(/\\e/, '\\ue').replace(/\\f/, '\\uf').trim().replace(/\"/g, '').replace(/;/g, '');
            return v;
        };
        for (var _i = 0, sets_1 = sets; _i < sets_1.length; _i++) {
            var set = sets_1[_i];
            var pair = set.replace(/ /g, '').split(':before{');
            var keyGroups = pair[0];
            var keys = keyGroups.split(',');
            if (pair[1]) {
                var value = cleanValue(pair[1]);
                for (var _a = 0, keys_1 = keys; _a < keys_1.length; _a++) {
                    var key = keys_1[_a];
                    key = key.trim().slice(1).split(':before')[0];
                    this.css[this._currentName][key] = String.fromCharCode(parseInt(value.substring(2), 16));
                    if (TNSFontIconService_1.debug) {
                        console.log(key + ": " + value);
                    }
                }
            }
        }
    };
    TNSFontIconService.debug = false;
    TNSFontIconService = TNSFontIconService_1 = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Inject(exports.USE_STORE)),
        __metadata("design:paramtypes", [Object])
    ], TNSFontIconService);
    return TNSFontIconService;
    var TNSFontIconService_1;
}());
exports.TNSFontIconService = TNSFontIconService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9udGljb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZvbnRpY29uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxVQUFVO0FBQ1Ysc0NBQWdFO0FBRWhFLGVBQWU7QUFDZiwyQ0FBMkM7QUFFM0MsT0FBTztBQUNQLHdEQUF1RDtBQUUxQyxRQUFBLFNBQVMsR0FBRyxJQUFJLGtCQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7QUFHdEQ7SUFNRSw0QkFBd0MsTUFBVztRQUFYLFdBQU0sR0FBTixNQUFNLENBQUs7UUFINUMsUUFBRyxHQUFRLEVBQUUsQ0FBQyxDQUFDLGlFQUFpRTtRQUlyRixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksaUNBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDakIsQ0FBQzsyQkFUVSxrQkFBa0I7SUFXdEIsb0NBQU8sR0FBZDtRQUFBLGlCQTBCQztRQXpCQyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUM7UUFDWixJQUFJLG1CQUFtQixHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25ELEVBQUUsQ0FBQyxDQUFDLG9CQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBd0IsbUJBQXFCLENBQUMsQ0FBQztRQUM3RCxDQUFDO1FBRUQsSUFBSSxjQUFjLEdBQUc7WUFDbkIsS0FBSSxDQUFDLFlBQVksR0FBRyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM3QyxLQUFJLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDbkMsQ0FBQyxDQUFDO1FBRUYsSUFBSSxTQUFTLEdBQUc7WUFDZCxjQUFjLEVBQUUsQ0FBQztZQUNqQixFQUFFLENBQUMsQ0FBQyxHQUFHLEtBQUssbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDdkMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2xDLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDTixJQUFJLEtBQUssR0FBUSxLQUFJLENBQUMsTUFBTSxDQUFDO2dCQUM3QixLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQzNDLEdBQUcsRUFBRSxDQUFDO29CQUNOLFNBQVMsRUFBRSxDQUFDO2dCQUNkLENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQztRQUNILENBQUMsQ0FBQztRQUVGLFNBQVMsRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVPLHFDQUFRLEdBQWhCLFVBQWlCLElBQVk7UUFBN0IsaUJBY0M7UUFiQyxFQUFFLENBQUMsQ0FBQyxvQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBdUIsSUFBSSxDQUFDLFlBQVkscUJBQWdCLElBQU0sQ0FBQyxDQUFDO1FBQzlFLENBQUM7UUFDRCxJQUFJLE9BQU8sR0FBRywwQkFBWSxDQUFDLFVBQVUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0RCxNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUNqQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBUztnQkFDaEMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbEIsT0FBTyxFQUFFLENBQUM7WUFDWixDQUFDLEVBQUUsVUFBQyxHQUFRO2dCQUNWLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUNkLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sbUNBQU0sR0FBZCxVQUFlLElBQVM7UUFDdEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixJQUFJLFVBQVUsR0FBRyxVQUFDLEdBQVc7WUFDM0IsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3pJLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUM7UUFFRixHQUFHLENBQUMsQ0FBWSxVQUFJLEVBQUosYUFBSSxFQUFKLGtCQUFJLEVBQUosSUFBSTtZQUFmLElBQUksR0FBRyxhQUFBO1lBQ1YsSUFBSSxJQUFJLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ25ELElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLElBQUksR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1osSUFBSSxLQUFLLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoQyxHQUFHLENBQUMsQ0FBWSxVQUFJLEVBQUosYUFBSSxFQUFKLGtCQUFJLEVBQUosSUFBSTtvQkFBZixJQUFJLEdBQUcsYUFBQTtvQkFDVixHQUFHLEdBQUcsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzlDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDekYsRUFBRSxDQUFDLENBQUMsb0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzt3QkFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBSSxHQUFHLFVBQUssS0FBTyxDQUFDLENBQUM7b0JBQ2xDLENBQUM7aUJBQ0Y7WUFDSCxDQUFDO1NBQ0Y7SUFDSCxDQUFDO0lBNUVhLHdCQUFLLEdBQVksS0FBSyxDQUFDO0lBRDFCLGtCQUFrQjtRQUQ5QixpQkFBVSxFQUFFO1FBT0csV0FBQSxhQUFNLENBQUMsaUJBQVMsQ0FBQyxDQUFBOztPQU5wQixrQkFBa0IsQ0E4RTlCO0lBQUQseUJBQUM7O0NBQUEsQUE5RUQsSUE4RUM7QUE5RVksZ0RBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiLy8gYW5ndWxhclxuaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0LCBPcGFxdWVUb2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG4vLyBuYXRpdmVzY3JpcHRcbmltcG9ydCB7IGtub3duRm9sZGVycyB9IGZyb20gJ2ZpbGUtc3lzdGVtJztcblxuLy8gbGlic1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcy9CZWhhdmlvclN1YmplY3QnO1xuXG5leHBvcnQgY29uc3QgVVNFX1NUT1JFID0gbmV3IE9wYXF1ZVRva2VuKCdVU0VfU1RPUkUnKTtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFROU0ZvbnRJY29uU2VydmljZSB7XG4gIHB1YmxpYyBzdGF0aWMgZGVidWc6IGJvb2xlYW4gPSBmYWxzZTtcbiAgcHVibGljIGZpbGVzTG9hZGVkOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcbiAgcHVibGljIGNzczogYW55ID0ge307IC8vIGZvbnQgaWNvbiBjb2xsZWN0aW9ucyBjb250YWluaW5nIG1hcHMgb2YgY2xhc3NuYW1lcyB0byB1bmljb2RlXG4gIHByaXZhdGUgX2N1cnJlbnROYW1lOiBzdHJpbmc7IC8vIGN1cnJlbnQgY29sbGVjdGlvbiBuYW1lXG4gIFxuICBjb25zdHJ1Y3RvciggQEluamVjdChVU0VfU1RPUkUpIHByaXZhdGUgY29uZmlnOiBhbnkpIHtcbiAgICB0aGlzLmZpbGVzTG9hZGVkID0gbmV3IEJlaGF2aW9yU3ViamVjdChudWxsKTtcbiAgICB0aGlzLmxvYWRDc3MoKTtcbiAgfVxuXG4gIHB1YmxpYyBsb2FkQ3NzKCk6IHZvaWQge1xuICAgIGxldCBjbnQgPSAwO1xuICAgIGxldCBmb250SWNvbkNvbGxlY3Rpb25zID0gT2JqZWN0LmtleXModGhpcy5jb25maWcpO1xuICAgIGlmIChUTlNGb250SWNvblNlcnZpY2UuZGVidWcpIHtcbiAgICAgIGNvbnNvbGUubG9nKGBDb2xsZWN0aW9ucyB0byBsb2FkOiAke2ZvbnRJY29uQ29sbGVjdGlvbnN9YCk7XG4gICAgfVxuXG4gICAgbGV0IGluaXRDb2xsZWN0aW9uID0gKCkgPT4ge1xuICAgICAgdGhpcy5fY3VycmVudE5hbWUgPSBmb250SWNvbkNvbGxlY3Rpb25zW2NudF07XG4gICAgICB0aGlzLmNzc1t0aGlzLl9jdXJyZW50TmFtZV0gPSB7fTtcbiAgICB9O1xuXG4gICAgbGV0IGxvYWRGaWxlcyA9ICgpID0+IHtcbiAgICAgIGluaXRDb2xsZWN0aW9uKCk7XG4gICAgICBpZiAoY250ID09PSBmb250SWNvbkNvbGxlY3Rpb25zLmxlbmd0aCkge1xuICAgICAgICB0aGlzLmZpbGVzTG9hZGVkLm5leHQodGhpcy5jc3MpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgbGV0IGZvbnRzOiBhbnkgPSB0aGlzLmNvbmZpZztcbiAgICAgICAgdGhpcy5sb2FkRmlsZShmb250c1t0aGlzLl9jdXJyZW50TmFtZV0pLnRoZW4oKCkgPT4ge1xuICAgICAgICAgIGNudCsrO1xuICAgICAgICAgIGxvYWRGaWxlcygpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgbG9hZEZpbGVzKCk7XG4gIH1cblxuICBwcml2YXRlIGxvYWRGaWxlKHBhdGg6IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgaWYgKFROU0ZvbnRJY29uU2VydmljZS5kZWJ1Zykge1xuICAgICAgY29uc29sZS5sb2coYC0tLS0tLS0tLS1gKTtcbiAgICAgIGNvbnNvbGUubG9nKGBMb2FkaW5nIGNvbGxlY3Rpb24gJyR7dGhpcy5fY3VycmVudE5hbWV9JyBmcm9tIGZpbGU6ICR7cGF0aH1gKTtcbiAgICB9XG4gICAgbGV0IGNzc0ZpbGUgPSBrbm93bkZvbGRlcnMuY3VycmVudEFwcCgpLmdldEZpbGUocGF0aCk7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgIGNzc0ZpbGUucmVhZFRleHQoKS50aGVuKChkYXRhOiBhbnkpID0+IHtcbiAgICAgICAgdGhpcy5tYXBDc3MoZGF0YSk7XG4gICAgICAgIHJlc29sdmUoKTtcbiAgICAgIH0sIChlcnI6IGFueSkgPT4ge1xuICAgICAgICByZWplY3QoZXJyKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBtYXBDc3MoZGF0YTogYW55KTogdm9pZCB7XG4gICAgbGV0IHNldHMgPSBkYXRhLnNwbGl0KCd9Jyk7XG4gICAgbGV0IGNsZWFuVmFsdWUgPSAodmFsOiBzdHJpbmcpID0+IHtcbiAgICAgIGxldCB2ID0gdmFsLnNwbGl0KCdjb250ZW50OicpWzFdLnRvTG93ZXJDYXNlKCkucmVwbGFjZSgvXFxcXGUvLCAnXFxcXHVlJykucmVwbGFjZSgvXFxcXGYvLCAnXFxcXHVmJykudHJpbSgpLnJlcGxhY2UoL1xcXCIvZywgJycpLnJlcGxhY2UoLzsvZywgJycpO1xuICAgICAgcmV0dXJuIHY7XG4gICAgfTtcblxuICAgIGZvciAobGV0IHNldCBvZiBzZXRzKSB7XG4gICAgICBsZXQgcGFpciA9IHNldC5yZXBsYWNlKC8gL2csICcnKS5zcGxpdCgnOmJlZm9yZXsnKTtcbiAgICAgIGxldCBrZXlHcm91cHMgPSBwYWlyWzBdO1xuICAgICAgbGV0IGtleXMgPSBrZXlHcm91cHMuc3BsaXQoJywnKTtcbiAgICAgIGlmIChwYWlyWzFdKSB7XG4gICAgICAgIGxldCB2YWx1ZSA9IGNsZWFuVmFsdWUocGFpclsxXSk7XG4gICAgICAgIGZvciAobGV0IGtleSBvZiBrZXlzKSB7XG4gICAgICAgICAga2V5ID0ga2V5LnRyaW0oKS5zbGljZSgxKS5zcGxpdCgnOmJlZm9yZScpWzBdO1xuICAgICAgICAgIHRoaXMuY3NzW3RoaXMuX2N1cnJlbnROYW1lXVtrZXldID0gU3RyaW5nLmZyb21DaGFyQ29kZShwYXJzZUludCh2YWx1ZS5zdWJzdHJpbmcoMiksIDE2KSk7XG4gICAgICAgICAgaWYgKFROU0ZvbnRJY29uU2VydmljZS5kZWJ1Zykge1xuICAgICAgICAgICAgY29uc29sZS5sb2coYCR7a2V5fTogJHt2YWx1ZX1gKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==