"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
var core_1 = require("@angular/core");
var fonticon_pipe_1 = require("./src/app/pipes/fonticon.pipe");
var fonticon_service_1 = require("./src/app/services/fonticon.service");
// for manual imports
__export(require("./src/app/pipes/fonticon.pipe"));
__export(require("./src/app/services/fonticon.service"));
var config;
exports.iconConfig = function () {
    return new fonticon_service_1.TNSFontIconService(config);
};
var TNSFontIconModule = TNSFontIconModule_1 = (function () {
    function TNSFontIconModule() {
    }
    TNSFontIconModule.forRoot = function (providedConfig) {
        config = providedConfig;
        return {
            ngModule: TNSFontIconModule_1,
            providers: [
                { provide: fonticon_service_1.TNSFontIconService, useFactory: (exports.iconConfig) }
            ]
        };
    };
    return TNSFontIconModule;
}());
TNSFontIconModule = TNSFontIconModule_1 = __decorate([
    core_1.NgModule({
        declarations: [
            fonticon_pipe_1.TNSFontIconPipe,
            fonticon_pipe_1.TNSFontIconPurePipe
        ],
        exports: [
            fonticon_pipe_1.TNSFontIconPipe,
            fonticon_pipe_1.TNSFontIconPurePipe
        ]
    })
], TNSFontIconModule);
exports.TNSFontIconModule = TNSFontIconModule;
var TNSFontIconModule_1;
