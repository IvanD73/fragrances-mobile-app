import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppComponent } from "./app.component";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { TNSFontIconModule, TNSFontIconService } from 'nativescript-ngx-fonticon';
import { SlidesModule } from 'nativescript-ngx-slides';
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { TNSCheckBoxModule } from 'nativescript-checkbox/angular';
import { RadioButtonModule } from 'nativescript-radiobutton/angular'
import { NativeScriptHttpModule } from "nativescript-angular/http";

//Routes
import {routes} from './app.routes';

//Services
import { ProductsServices } from './services/products.services';
import { FilterService } from './services/filter.service';
import { AuthService } from './services/auth.service'
import { AuthGuardService } from './services/auth-guard.service'
import { ValidationService } from './services/validation.service'
import { AccountService } from './services/account.service'

//Components
import { DrawerComponent } from './components/drawer/drawer.component'
import { SearchModalComponent }  from './components/modal/search/search-modal.component';
import { FilterModalComponent } from './components/modal/filter/filter-modal.component';
import { AddressModalComponent } from './components/modal/address/address-modal.component';
import { ProductImageModalComponent } from './components/modal/productImage/productImage-modal.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component'
import { RegisterComponent } from './components/register/register.component';
import { SliderComponent } from './pages/home/slider/slider.component'
import { BoxesComponent } from './pages/home/boxes/boxes.component'

//Pages
import { HomePage } from "./pages/home/home.page";
import { AccountDashboardPage } from './pages/account/dashboard/account-dashboard.page';
import { AccountEditPage } from './pages/account/edit/account-edit.page'
import { AccountAddressesPage } from './pages/account/addresses/account-addresses.page'
import { AccountOrdersPage} from './pages/account/orders/account-orders.page'
import { SettingsPage } from "./pages/settings/settings.page";
import { CheckoutPage } from './pages/checkout/checkout.page';

import { CategoriesPage } from './pages/categories/categories.page';
import { CategoryList } from './pages/categories/category-list/categoryList.component';
import { SubcategoryList } from './pages/categories/subcategory-list/subcategoryList.component';
import { CategoryProducts } from './pages/categories/category-products/category-products.component';

import { ProductPage } from './pages/product/product.page';
import { SIDEDRAWER_DIRECTIVES } from "nativescript-pro-ui/sidedrawer/angular";
import { NativeScriptFormsModule } from "nativescript-angular";

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    HomePage,
    SliderComponent,
    BoxesComponent,
    AccountDashboardPage,
    AccountEditPage,
    AccountAddressesPage,
    AccountOrdersPage,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    SettingsPage,
    CheckoutPage,
    CategoriesPage,
    CategoryList,
    SubcategoryList,
    CategoryProducts,
    ProductPage,
    DrawerComponent,
    SIDEDRAWER_DIRECTIVES,
    SearchModalComponent,
    FilterModalComponent,
    AddressModalComponent,
    ProductImageModalComponent,
  ],
  entryComponents: [
      SearchModalComponent,
      FilterModalComponent,
      AddressModalComponent,
      ProductImageModalComponent,
  ],
  imports: [
      NativeScriptModule ,
      NativeScriptRouterModule,
      NativeScriptRouterModule.forRoot(routes),
      NativeScriptFormsModule,  
      TNSFontIconModule.forRoot({
        'fa': './assets/font-awesome.css',
        'ion': './assets/ionicons.css'
      }),
      SlidesModule,
      TNSCheckBoxModule,
      RadioButtonModule,
      NativeScriptHttpModule
  ],
  providers: [
      ProductsServices,
      ModalDialogService,
      AuthService,
      AuthGuardService,
      ValidationService,
<<<<<<< HEAD
      AccountAddressesService,
      FilterService
=======
      AccountService
>>>>>>> 79d97655f7b47797f695582b3c39918b3a653362
  ],
  schemas: [NO_ERRORS_SCHEMA],
})

export class AppModule {}
