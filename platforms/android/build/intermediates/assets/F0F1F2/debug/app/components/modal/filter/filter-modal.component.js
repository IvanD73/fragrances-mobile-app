"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var filter_service_1 = require("../../../services/filter.service");
var FilterModalComponent = (function () {
    function FilterModalComponent(params, filterService) {
        this.params = params;
        this.filterService = filterService;
        this.currentFilters = [
            { manufacturers: [] },
            { price: "10лв. - 25лв." },
            { aromaticGroups: [] },
            { moreOptions: [] }
        ];
        this.manufacturers = [];
        this.aromaticGroups = [];
        this.showFilterOptions = false;
        this.showingFilterHeading = 'Филтрирай';
        this.showingFilterName = '';
        this.manufacturers = this.filterService.getManufacturers();
        this.aromaticGroups = this.filterService.getАromaticGroups();
    }
    FilterModalComponent.prototype.close = function (res) {
        this.params.closeCallback(res);
    };
    FilterModalComponent.prototype.hideOptions = function () {
        this.showFilterOptions = false;
        this.showingFilterName = '';
    };
    FilterModalComponent.prototype.displayFilterOptions = function (filterName) {
        this.showFilterOptions = !this.showFilterOptions;
        this.showingFilterName = filterName;
        this.setFilterHeadingName(filterName);
    };
    FilterModalComponent.prototype.setFilterHeadingName = function (filterName) {
        switch (filterName) {
            case 'manufacturers': {
                this.showingFilterHeading = 'Марки';
                break;
            }
            case 'price': {
                this.showingFilterHeading = 'Цени';
                break;
            }
            case 'aromatic-groups': {
                this.showingFilterHeading = 'Ароматни групи';
                break;
            }
            case 'more': {
                this.showingFilterHeading = 'Още';
                break;
            }
            default: {
                this.showingFilterHeading = 'Филтрирай';
                break;
            }
        }
    };
    FilterModalComponent = __decorate([
        core_1.Component({
            selector: "filter-modal",
            templateUrl: "components/modal/filter/filter-modal.component.html",
        }),
        __metadata("design:paramtypes", [dialogs_1.ModalDialogParams,
            filter_service_1.FilterService])
    ], FilterModalComponent);
    return FilterModalComponent;
}());
exports.FilterModalComponent = FilterModalComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZpbHRlci1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFDMUMsbUVBQTRFO0FBQzVFLG1FQUFpRTtBQU1qRTtJQWNJLDhCQUNTLE1BQXlCLEVBQ3pCLGFBQTRCO1FBRDVCLFdBQU0sR0FBTixNQUFNLENBQW1CO1FBQ3pCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO1FBZnhDLG1CQUFjLEdBQUc7WUFDaEIsRUFBQyxhQUFhLEVBQUMsRUFBRSxFQUFDO1lBQ2xCLEVBQUMsS0FBSyxFQUFDLGVBQWUsRUFBQztZQUN2QixFQUFDLGNBQWMsRUFBQyxFQUFFLEVBQUM7WUFDbkIsRUFBQyxXQUFXLEVBQUMsRUFBRSxFQUFDO1NBQ2hCLENBQUE7UUFFRCxrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNwQixzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDMUIseUJBQW9CLEdBQUcsV0FBVyxDQUFDO1FBQ25DLHNCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUtoQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUMzRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUNqRSxDQUFDO0lBRU0sb0NBQUssR0FBWixVQUFhLEdBQVc7UUFDcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELDBDQUFXLEdBQVg7UUFDQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVELG1EQUFvQixHQUFwQixVQUFxQixVQUFVO1FBQzlCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNqRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsVUFBVSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsbURBQW9CLEdBQXBCLFVBQXFCLFVBQVU7UUFDOUIsTUFBTSxDQUFBLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNwQixLQUFLLGVBQWUsRUFBRSxDQUFDO2dCQUNwQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsT0FBTyxDQUFDO2dCQUNwQyxLQUFLLENBQUM7WUFDVCxDQUFDO1lBQ0QsS0FBSyxPQUFPLEVBQUUsQ0FBQztnQkFDWixJQUFJLENBQUMsb0JBQW9CLEdBQUcsTUFBTSxDQUFDO2dCQUNuQyxLQUFLLENBQUM7WUFDVCxDQUFDO1lBQ0QsS0FBSyxpQkFBaUIsRUFBRSxDQUFDO2dCQUN0QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsZ0JBQWdCLENBQUM7Z0JBQzdDLEtBQUssQ0FBQztZQUNULENBQUM7WUFDRCxLQUFLLE1BQU0sRUFBRSxDQUFDO2dCQUNYLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFLLENBQUM7Z0JBQ2xDLEtBQUssQ0FBQztZQUNULENBQUM7WUFDRCxTQUFTLENBQUM7Z0JBQ1AsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFdBQVcsQ0FBQztnQkFDeEMsS0FBSyxDQUFDO1lBQ1QsQ0FBQztRQUNKLENBQUM7SUFDQyxDQUFDO0lBM0RRLG9CQUFvQjtRQUpoQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLGNBQWM7WUFDeEIsV0FBVyxFQUFFLHFEQUFxRDtTQUNyRSxDQUFDO3lDQWdCbUIsMkJBQWlCO1lBQ1YsOEJBQWE7T0FoQjVCLG9CQUFvQixDQTZEaEM7SUFBRCwyQkFBQztDQUFBLEFBN0RELElBNkRDO0FBN0RZLG9EQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE1vZGFsRGlhbG9nUGFyYW1zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2RpcmVjdGl2ZXMvZGlhbG9nc1wiO1xyXG5pbXBvcnQgeyBGaWx0ZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvZmlsdGVyLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJmaWx0ZXItbW9kYWxcIixcclxuICAgIHRlbXBsYXRlVXJsOiBcImNvbXBvbmVudHMvbW9kYWwvZmlsdGVyL2ZpbHRlci1tb2RhbC5jb21wb25lbnQuaHRtbFwiLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmlsdGVyTW9kYWxDb21wb25lbnQge1xyXG5cdGN1cnJlbnRGaWx0ZXJzID0gW1xyXG5cdFx0e21hbnVmYWN0dXJlcnM6W119LFxyXG5cdFx0e3ByaWNlOlwiMTDQu9CyLiAtIDI10LvQsi5cIn0sXHJcblx0XHR7YXJvbWF0aWNHcm91cHM6W119LFxyXG5cdFx0e21vcmVPcHRpb25zOltdfVxyXG5cdF1cclxuXHJcblx0bWFudWZhY3R1cmVycyA9IFtdO1xyXG5cdGFyb21hdGljR3JvdXBzID0gW107XHJcblx0c2hvd0ZpbHRlck9wdGlvbnMgPSBmYWxzZTtcclxuXHRzaG93aW5nRmlsdGVySGVhZGluZyA9ICfQpNC40LvRgtGA0LjRgNCw0LknO1xyXG5cdHNob3dpbmdGaWx0ZXJOYW1lID0gJyc7XHJcblx0XHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IoXHJcbiAgICBcdHByaXZhdGUgcGFyYW1zOiBNb2RhbERpYWxvZ1BhcmFtcyxcclxuICAgIFx0cHJpdmF0ZSBmaWx0ZXJTZXJ2aWNlOiBGaWx0ZXJTZXJ2aWNlLCl7XHJcbiAgICAgICAgdGhpcy5tYW51ZmFjdHVyZXJzID0gdGhpcy5maWx0ZXJTZXJ2aWNlLmdldE1hbnVmYWN0dXJlcnMoKTtcclxuICAgICAgICB0aGlzLmFyb21hdGljR3JvdXBzID0gdGhpcy5maWx0ZXJTZXJ2aWNlLmdldNCQcm9tYXRpY0dyb3VwcygpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBjbG9zZShyZXM6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMucGFyYW1zLmNsb3NlQ2FsbGJhY2socmVzKTtcclxuICAgIH1cclxuXHJcbiAgICBoaWRlT3B0aW9ucygpe1xyXG4gICAgXHR0aGlzLnNob3dGaWx0ZXJPcHRpb25zID0gZmFsc2U7XHJcbiAgICBcdHRoaXMuc2hvd2luZ0ZpbHRlck5hbWUgPSAnJztcclxuICAgIH1cclxuXHJcbiAgIFx0ZGlzcGxheUZpbHRlck9wdGlvbnMoZmlsdGVyTmFtZSl7XHJcbiAgIFx0XHR0aGlzLnNob3dGaWx0ZXJPcHRpb25zID0gIXRoaXMuc2hvd0ZpbHRlck9wdGlvbnM7XHJcbiAgIFx0XHR0aGlzLnNob3dpbmdGaWx0ZXJOYW1lID0gZmlsdGVyTmFtZTtcclxuICAgXHRcdHRoaXMuc2V0RmlsdGVySGVhZGluZ05hbWUoZmlsdGVyTmFtZSk7XHRcclxuICAgXHR9XHJcblxyXG4gICBcdHNldEZpbHRlckhlYWRpbmdOYW1lKGZpbHRlck5hbWUpe1xyXG4gICBcdFx0c3dpdGNoKGZpbHRlck5hbWUpIHsgXHJcblx0XHQgICBjYXNlICdtYW51ZmFjdHVyZXJzJzoge1xyXG5cdFx0ICAgICAgdGhpcy5zaG93aW5nRmlsdGVySGVhZGluZyA9ICfQnNCw0YDQutC4JzsgXHJcblx0XHQgICAgICBicmVhazsgXHJcblx0XHQgICB9IFxyXG5cdFx0ICAgY2FzZSAncHJpY2UnOiB7IFxyXG5cdFx0ICAgICAgdGhpcy5zaG93aW5nRmlsdGVySGVhZGluZyA9ICfQptC10L3QuCc7IFxyXG5cdFx0ICAgICAgYnJlYWs7IFxyXG5cdFx0ICAgfVxyXG5cdFx0ICAgY2FzZSAnYXJvbWF0aWMtZ3JvdXBzJzogeyBcclxuXHRcdCAgICAgIHRoaXMuc2hvd2luZ0ZpbHRlckhlYWRpbmcgPSAn0JDRgNC+0LzQsNGC0L3QuCDQs9GA0YPQv9C4JzsgXHJcblx0XHQgICAgICBicmVhazsgXHJcblx0XHQgICB9XHJcblx0XHQgICBjYXNlICdtb3JlJzogeyBcclxuXHRcdCAgICAgIHRoaXMuc2hvd2luZ0ZpbHRlckhlYWRpbmcgPSAn0J7RidC1JzsgXHJcblx0XHQgICAgICBicmVhazsgXHJcblx0XHQgICB9XHJcblx0XHQgICBkZWZhdWx0OiB7IFxyXG5cdFx0ICAgICAgdGhpcy5zaG93aW5nRmlsdGVySGVhZGluZyA9ICfQpNC40LvRgtGA0LjRgNCw0LknOyBcclxuXHRcdCAgICAgIGJyZWFrOyBcclxuXHRcdCAgIH0gXHJcblx0XHR9IFxyXG4gICBcdH1cclxuXHJcbn0iXX0=