import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/directives/dialogs";
import { FilterService } from '../../../services/filter.service';

@Component({
    selector: "filter-modal",
    templateUrl: "components/modal/filter/filter-modal.component.html",
})
export class FilterModalComponent {
	currentFilters = [
		{manufacturers:[]},
		{price:"10лв. - 25лв."},
		{aromaticGroups:[]},
		{moreOptions:[]}
	]

	manufacturers = [];
	aromaticGroups = [];
	showFilterOptions = false;
	showingFilterHeading = 'Филтрирай';
	showingFilterName = '';
	
    public constructor(
    	private params: ModalDialogParams,
    	private filterService: FilterService,){
        this.manufacturers = this.filterService.getManufacturers();
        this.aromaticGroups = this.filterService.getАromaticGroups();
    }

    public close(res: string) {
        this.params.closeCallback(res);
    }

    hideOptions(){
    	this.showFilterOptions = false;
    	this.showingFilterName = '';
    }

   	displayFilterOptions(filterName){
   		this.showFilterOptions = !this.showFilterOptions;
   		this.showingFilterName = filterName;
   		this.setFilterHeadingName(filterName);	
   	}

   	setFilterHeadingName(filterName){
   		switch(filterName) { 
		   case 'manufacturers': {
		      this.showingFilterHeading = 'Марки'; 
		      break; 
		   } 
		   case 'price': { 
		      this.showingFilterHeading = 'Цени'; 
		      break; 
		   }
		   case 'aromatic-groups': { 
		      this.showingFilterHeading = 'Ароматни групи'; 
		      break; 
		   }
		   case 'more': { 
		      this.showingFilterHeading = 'Още'; 
		      break; 
		   }
		   default: { 
		      this.showingFilterHeading = 'Филтрирай'; 
		      break; 
		   } 
		} 
   	}

}