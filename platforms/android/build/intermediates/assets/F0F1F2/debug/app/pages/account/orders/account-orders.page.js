"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../page");
var common_1 = require("@angular/common");
var AccountOrdersPage = (function (_super) {
    __extends(AccountOrdersPage, _super);
    function AccountOrdersPage(location) {
        var _this = _super.call(this, location) || this;
        _this.location = location;
        return _this;
    }
    AccountOrdersPage.prototype.ngOnInit = function () {
    };
    AccountOrdersPage = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'account-orders',
            templateUrl: 'account-orders.page.html',
        }),
        __metadata("design:paramtypes", [common_1.Location])
    ], AccountOrdersPage);
    return AccountOrdersPage;
}(page_1.Page));
exports.AccountOrdersPage = AccountOrdersPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1vcmRlcnMucGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjY291bnQtb3JkZXJzLnBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBNkQ7QUFFN0QsbUNBQWtDO0FBR2xDLDBDQUEyQztBQVEzQztJQUF1QyxxQ0FBSTtJQUN2QywyQkFBb0IsUUFBa0I7UUFBdEMsWUFDSSxrQkFBTSxRQUFRLENBQUMsU0FDbEI7UUFGbUIsY0FBUSxHQUFSLFFBQVEsQ0FBVTs7SUFFdEMsQ0FBQztJQUVELG9DQUFRLEdBQVI7SUFFQSxDQUFDO0lBUFEsaUJBQWlCO1FBTjdCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixXQUFXLEVBQUUsMEJBQTBCO1NBQzFDLENBQUM7eUNBR2dDLGlCQUFRO09BRDdCLGlCQUFpQixDQVE3QjtJQUFELHdCQUFDO0NBQUEsQUFSRCxDQUF1QyxXQUFJLEdBUTFDO0FBUlksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBDaGFuZ2VEZXRlY3RvclJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcIi4uLy4uL3BhZ2VcIjtcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnXHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdhY2NvdW50LW9yZGVycycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2FjY291bnQtb3JkZXJzLnBhZ2UuaHRtbCcsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQWNjb3VudE9yZGVyc1BhZ2UgZXh0ZW5kcyBQYWdlIGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbG9jYXRpb246IExvY2F0aW9uKSB7XHJcbiAgICAgICAgc3VwZXIobG9jYXRpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG59Il19