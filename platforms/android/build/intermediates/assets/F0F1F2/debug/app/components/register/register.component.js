"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var validation_service_1 = require("../../services/validation.service");
var auth_service_1 = require("../../services/auth.service");
var router_1 = require("nativescript-angular/router");
var RegisterComponent = (function () {
    function RegisterComponent(authService, routerExtensions) {
        this.authService = authService;
        this.routerExtensions = routerExtensions;
        //fields
        this.firstname = '';
        this.lastname = '';
        this.password = '';
        this.confirm = '';
        this.email = '';
        this.telephone = '';
        this.address_1 = '';
        this.city = '';
        this.country_id = '55';
        this.zone_id = '1';
        this.newsletter = '1';
        //errors
        this.error_firstname = '';
        this.error_lastname = '';
        this.error_password = '';
        this.error_confirm = '';
        this.error_email = '';
        this.error_telephone = '';
        this.error_address_1 = '';
        this.error_city = '';
        this.error_country_id = '';
        this.error_zone_id = '';
        this.error_register = '';
    }
    RegisterComponent.prototype.register = function () {
        var _this = this;
        if (this.validateRegisterCredentials()) {
            //Create customer object
            var customer = {
                firstname: this.firstname,
                lastname: this.lastname,
                password: this.password,
                email: this.email,
                telephone: this.telephone,
                address_1: this.address_1,
                city: this.city,
                country_id: this.country_id,
                zone_id: this.zone_id,
                newsletter: this.newsletter
            };
            this.authService.register(customer, function () {
                _this.routerExtensions.navigate(['/account']);
            }, function (error) {
                _this.error_register = error;
            });
        }
    };
    RegisterComponent.prototype.validateRegisterCredentials = function () {
        this.resetErrors();
        if (!validation_service_1.ValidationService.isValidFirstname(this.firstname)) {
            this.error_firstname = 'Invalid firstname';
        }
        if (!validation_service_1.ValidationService.isValidLastname(this.lastname)) {
            this.error_lastname = 'Invalid lastname';
        }
        if (!validation_service_1.ValidationService.isValidEmail(this.email)) {
            this.error_email = 'Invalid email';
        }
        if (!validation_service_1.ValidationService.isValidTelephone(this.telephone)) {
            this.error_telephone = 'Invalid telehone';
        }
        if (!validation_service_1.ValidationService.isValidCity(this.city)) {
            this.error_city = 'Invalid city';
        }
        if (!validation_service_1.ValidationService.isValidCountryId(this.country_id)) {
            this.error_country_id = 'Invalid country';
        }
        if (!validation_service_1.ValidationService.isValidZoneId(this.zone_id)) {
            this.error_zone_id = 'invalid zone id';
        }
        if (!validation_service_1.ValidationService.isValidAddress(this.address_1)) {
            this.error_address_1 = 'Invalid address';
        }
        if (!validation_service_1.ValidationService.isValidPassword(this.password)) {
            this.error_password = 'Invalid password';
        }
        if (!validation_service_1.ValidationService.isValidConfirm(this.password, this.confirm)) {
            this.error_confirm = 'Invalid confirm';
        }
        return this.hasErrors();
    };
    RegisterComponent.prototype.resetErrors = function () {
        this.error_firstname = '';
        this.error_lastname = '';
        this.error_password = '';
        this.error_confirm = '';
        this.error_email = '';
        this.error_telephone = '';
        this.error_address_1 = '';
        this.error_city = '';
        this.error_country_id = '';
        this.error_zone_id = '';
        this.error_register = '';
    };
    RegisterComponent.prototype.hasErrors = function () {
        return !this.error_firstname &&
            !this.error_lastname &&
            !this.error_email &&
            !this.error_telephone &&
            !this.error_address_1 &&
            !this.error_city &&
            !this.error_country_id &&
            !this.error_zone_id &&
            !this.error_confirm;
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: 'register-component',
            templateUrl: 'components/register/register.component.html',
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService, router_1.RouterExtensions])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVnaXN0ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicmVnaXN0ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTBDO0FBQzFDLHdFQUFxRTtBQUNyRSw0REFBdUQ7QUFDdkQsc0RBQStEO0FBTy9EO0lBMkJJLDJCQUFvQixXQUF3QixFQUFVLGdCQUFrQztRQUFwRSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUExQnhGLFFBQVE7UUFDUixjQUFTLEdBQUcsRUFBRSxDQUFBO1FBQ2QsYUFBUSxHQUFHLEVBQUUsQ0FBQTtRQUNiLGFBQVEsR0FBRyxFQUFFLENBQUE7UUFDYixZQUFPLEdBQUcsRUFBRSxDQUFBO1FBQ1osVUFBSyxHQUFHLEVBQUUsQ0FBQTtRQUNWLGNBQVMsR0FBRyxFQUFFLENBQUE7UUFDZCxjQUFTLEdBQUcsRUFBRSxDQUFBO1FBQ2QsU0FBSSxHQUFHLEVBQUUsQ0FBQTtRQUNULGVBQVUsR0FBRyxJQUFJLENBQUE7UUFDakIsWUFBTyxHQUFHLEdBQUcsQ0FBQTtRQUNiLGVBQVUsR0FBRyxHQUFHLENBQUE7UUFFaEIsUUFBUTtRQUNSLG9CQUFlLEdBQUcsRUFBRSxDQUFBO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFBO1FBQ25CLG1CQUFjLEdBQUcsRUFBRSxDQUFBO1FBQ25CLGtCQUFhLEdBQUcsRUFBRSxDQUFBO1FBQ2xCLGdCQUFXLEdBQUcsRUFBRSxDQUFBO1FBQ2hCLG9CQUFlLEdBQUcsRUFBRSxDQUFBO1FBQ3BCLG9CQUFlLEdBQUcsRUFBRSxDQUFBO1FBQ3BCLGVBQVUsR0FBRyxFQUFFLENBQUE7UUFDZixxQkFBZ0IsR0FBRyxFQUFFLENBQUE7UUFDckIsa0JBQWEsR0FBRyxFQUFFLENBQUE7UUFDbEIsbUJBQWMsR0FBRyxFQUFFLENBQUE7SUFJbkIsQ0FBQztJQUVELG9DQUFRLEdBQVI7UUFBQSxpQkF3QkM7UUF2QkcsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUMsQ0FBQSxDQUFDO1lBQ25DLHdCQUF3QjtZQUN4QixJQUFJLFFBQVEsR0FBRztnQkFDWCxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7Z0JBQ3pCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtnQkFDdkIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO2dCQUN2QixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7Z0JBQ2pCLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztnQkFDekIsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTO2dCQUN6QixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7Z0JBQ2YsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVO2dCQUMzQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87Z0JBQ3JCLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTthQUM5QixDQUFBO1lBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUM5QjtnQkFDSSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNqRCxDQUFDLEVBQ0QsVUFBQSxLQUFLO2dCQUNELEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFBO1lBQy9CLENBQUMsQ0FDSixDQUFBO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCx1REFBMkIsR0FBM0I7UUFDSSxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7UUFFbEIsRUFBRSxDQUFBLENBQUMsQ0FBQyxzQ0FBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3BELElBQUksQ0FBQyxlQUFlLEdBQUcsbUJBQW1CLENBQUE7UUFDOUMsQ0FBQztRQUVELEVBQUUsQ0FBQSxDQUFDLENBQUMsc0NBQWlCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDbEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxrQkFBa0IsQ0FBQTtRQUM1QyxDQUFDO1FBRUQsRUFBRSxDQUFBLENBQUMsQ0FBQyxzQ0FBaUIsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUM1QyxJQUFJLENBQUMsV0FBVyxHQUFHLGVBQWUsQ0FBQTtRQUN0QyxDQUFDO1FBRUQsRUFBRSxDQUFBLENBQUMsQ0FBQyxzQ0FBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ3BELElBQUksQ0FBQyxlQUFlLEdBQUcsa0JBQWtCLENBQUE7UUFDN0MsQ0FBQztRQUVELEVBQUUsQ0FBQSxDQUFDLENBQUMsc0NBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDMUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxjQUFjLENBQUE7UUFDcEMsQ0FBQztRQUVELEVBQUUsQ0FBQSxDQUFDLENBQUMsc0NBQWlCLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNyRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsaUJBQWlCLENBQUE7UUFDN0MsQ0FBQztRQUVELEVBQUUsQ0FBQSxDQUFDLENBQUMsc0NBQWlCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBLENBQUM7WUFDL0MsSUFBSSxDQUFDLGFBQWEsR0FBRyxpQkFBaUIsQ0FBQTtRQUMxQyxDQUFDO1FBRUQsRUFBRSxDQUFBLENBQUMsQ0FBQyxzQ0FBaUIsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUNsRCxJQUFJLENBQUMsZUFBZSxHQUFHLGlCQUFpQixDQUFBO1FBQzVDLENBQUM7UUFFRCxFQUFFLENBQUEsQ0FBQyxDQUFDLHNDQUFpQixDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ2xELElBQUksQ0FBQyxjQUFjLEdBQUcsa0JBQWtCLENBQUE7UUFDNUMsQ0FBQztRQUVELEVBQUUsQ0FBQSxDQUFDLENBQUMsc0NBQWlCLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUEsQ0FBQztZQUMvRCxJQUFJLENBQUMsYUFBYSxHQUFHLGlCQUFpQixDQUFBO1FBQzFDLENBQUM7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFBO0lBQzNCLENBQUM7SUFFTyx1Q0FBVyxHQUFuQjtRQUNJLElBQUksQ0FBQyxlQUFlLEdBQUksRUFBRSxDQUFBO1FBQzFCLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFBO1FBQ3hCLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFBO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFBO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFBO1FBQ3JCLElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFBO1FBQ3pCLElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFBO1FBQ3pCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFBO1FBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUE7UUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUE7UUFDdkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUE7SUFDNUIsQ0FBQztJQUVPLHFDQUFTLEdBQWpCO1FBQ0ksTUFBTSxDQUFFLENBQUMsSUFBSSxDQUFDLGVBQWU7WUFDckIsQ0FBQyxJQUFJLENBQUMsY0FBYztZQUNwQixDQUFDLElBQUksQ0FBQyxXQUFXO1lBQ2pCLENBQUMsSUFBSSxDQUFDLGVBQWU7WUFDckIsQ0FBQyxJQUFJLENBQUMsZUFBZTtZQUNyQixDQUFDLElBQUksQ0FBQyxVQUFVO1lBQ2hCLENBQUMsSUFBSSxDQUFDLGdCQUFnQjtZQUN0QixDQUFDLElBQUksQ0FBQyxhQUFhO1lBQ25CLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQTtJQUMvQixDQUFDO0lBL0hRLGlCQUFpQjtRQUw3QixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixXQUFXLEVBQUUsNkNBQTZDO1NBQzdELENBQUM7eUNBNkJtQywwQkFBVyxFQUE0Qix5QkFBZ0I7T0EzQi9FLGlCQUFpQixDQWdJN0I7SUFBRCx3QkFBQztDQUFBLEFBaElELElBZ0lDO0FBaElZLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFZhbGlkYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvdmFsaWRhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQge0F1dGhTZXJ2aWNlfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnXHJcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAncmVnaXN0ZXItY29tcG9uZW50JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnY29tcG9uZW50cy9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuaHRtbCcsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUmVnaXN0ZXJDb21wb25lbnR7XHJcbiAgICAvL2ZpZWxkc1xyXG4gICAgZmlyc3RuYW1lID0gJydcclxuICAgIGxhc3RuYW1lID0gJydcclxuICAgIHBhc3N3b3JkID0gJydcclxuICAgIGNvbmZpcm0gPSAnJ1xyXG4gICAgZW1haWwgPSAnJ1xyXG4gICAgdGVsZXBob25lID0gJydcclxuICAgIGFkZHJlc3NfMSA9ICcnXHJcbiAgICBjaXR5ID0gJydcclxuICAgIGNvdW50cnlfaWQgPSAnNTUnXHJcbiAgICB6b25lX2lkID0gJzEnXHJcbiAgICBuZXdzbGV0dGVyID0gJzEnXHJcblxyXG4gICAgLy9lcnJvcnNcclxuICAgIGVycm9yX2ZpcnN0bmFtZSA9ICcnXHJcbiAgICBlcnJvcl9sYXN0bmFtZSA9ICcnXHJcbiAgICBlcnJvcl9wYXNzd29yZCA9ICcnXHJcbiAgICBlcnJvcl9jb25maXJtID0gJydcclxuICAgIGVycm9yX2VtYWlsID0gJydcclxuICAgIGVycm9yX3RlbGVwaG9uZSA9ICcnXHJcbiAgICBlcnJvcl9hZGRyZXNzXzEgPSAnJ1xyXG4gICAgZXJyb3JfY2l0eSA9ICcnXHJcbiAgICBlcnJvcl9jb3VudHJ5X2lkID0gJydcclxuICAgIGVycm9yX3pvbmVfaWQgPSAnJ1xyXG4gICAgZXJyb3JfcmVnaXN0ZXIgPSAnJ1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXV0aFNlcnZpY2U6IEF1dGhTZXJ2aWNlLCBwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpe1xyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgIHJlZ2lzdGVyKCl7XHJcbiAgICAgICAgaWYodGhpcy52YWxpZGF0ZVJlZ2lzdGVyQ3JlZGVudGlhbHMoKSl7XHJcbiAgICAgICAgICAgIC8vQ3JlYXRlIGN1c3RvbWVyIG9iamVjdFxyXG4gICAgICAgICAgICBsZXQgY3VzdG9tZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBmaXJzdG5hbWU6IHRoaXMuZmlyc3RuYW1lLFxyXG4gICAgICAgICAgICAgICAgbGFzdG5hbWU6IHRoaXMubGFzdG5hbWUsXHJcbiAgICAgICAgICAgICAgICBwYXNzd29yZDogdGhpcy5wYXNzd29yZCxcclxuICAgICAgICAgICAgICAgIGVtYWlsOiB0aGlzLmVtYWlsLFxyXG4gICAgICAgICAgICAgICAgdGVsZXBob25lOiB0aGlzLnRlbGVwaG9uZSxcclxuICAgICAgICAgICAgICAgIGFkZHJlc3NfMTogdGhpcy5hZGRyZXNzXzEsXHJcbiAgICAgICAgICAgICAgICBjaXR5OiB0aGlzLmNpdHksXHJcbiAgICAgICAgICAgICAgICBjb3VudHJ5X2lkOiB0aGlzLmNvdW50cnlfaWQsXHJcbiAgICAgICAgICAgICAgICB6b25lX2lkOiB0aGlzLnpvbmVfaWQsXHJcbiAgICAgICAgICAgICAgICBuZXdzbGV0dGVyOiB0aGlzLm5ld3NsZXR0ZXJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLnJlZ2lzdGVyKGN1c3RvbWVyLFxyXG4gICAgICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbJy9hY2NvdW50J10pO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVycm9yX3JlZ2lzdGVyID0gZXJyb3JcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZVJlZ2lzdGVyQ3JlZGVudGlhbHMoKXtcclxuICAgICAgICB0aGlzLnJlc2V0RXJyb3JzKClcclxuXHJcbiAgICAgICAgaWYoIVZhbGlkYXRpb25TZXJ2aWNlLmlzVmFsaWRGaXJzdG5hbWUodGhpcy5maXJzdG5hbWUpKXtcclxuICAgICAgICAgICAgdGhpcy5lcnJvcl9maXJzdG5hbWUgPSAnSW52YWxpZCBmaXJzdG5hbWUnXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZighVmFsaWRhdGlvblNlcnZpY2UuaXNWYWxpZExhc3RuYW1lKHRoaXMubGFzdG5hbWUpKXtcclxuICAgICAgICAgICAgdGhpcy5lcnJvcl9sYXN0bmFtZSA9ICdJbnZhbGlkIGxhc3RuYW1lJ1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYoIVZhbGlkYXRpb25TZXJ2aWNlLmlzVmFsaWRFbWFpbCh0aGlzLmVtYWlsKSl7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JfZW1haWwgPSAnSW52YWxpZCBlbWFpbCdcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKCFWYWxpZGF0aW9uU2VydmljZS5pc1ZhbGlkVGVsZXBob25lKHRoaXMudGVsZXBob25lKSl7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JfdGVsZXBob25lID0gJ0ludmFsaWQgdGVsZWhvbmUnXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZighVmFsaWRhdGlvblNlcnZpY2UuaXNWYWxpZENpdHkodGhpcy5jaXR5KSl7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JfY2l0eSA9ICdJbnZhbGlkIGNpdHknXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZighVmFsaWRhdGlvblNlcnZpY2UuaXNWYWxpZENvdW50cnlJZCh0aGlzLmNvdW50cnlfaWQpKXtcclxuICAgICAgICAgICAgdGhpcy5lcnJvcl9jb3VudHJ5X2lkID0gJ0ludmFsaWQgY291bnRyeSdcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKCFWYWxpZGF0aW9uU2VydmljZS5pc1ZhbGlkWm9uZUlkKHRoaXMuem9uZV9pZCkpe1xyXG4gICAgICAgICAgICB0aGlzLmVycm9yX3pvbmVfaWQgPSAnaW52YWxpZCB6b25lIGlkJ1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYoIVZhbGlkYXRpb25TZXJ2aWNlLmlzVmFsaWRBZGRyZXNzKHRoaXMuYWRkcmVzc18xKSl7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JfYWRkcmVzc18xID0gJ0ludmFsaWQgYWRkcmVzcydcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKCFWYWxpZGF0aW9uU2VydmljZS5pc1ZhbGlkUGFzc3dvcmQodGhpcy5wYXNzd29yZCkpe1xyXG4gICAgICAgICAgICB0aGlzLmVycm9yX3Bhc3N3b3JkID0gJ0ludmFsaWQgcGFzc3dvcmQnXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZighVmFsaWRhdGlvblNlcnZpY2UuaXNWYWxpZENvbmZpcm0odGhpcy5wYXNzd29yZCwgdGhpcy5jb25maXJtKSl7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JfY29uZmlybSA9ICdJbnZhbGlkIGNvbmZpcm0nXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5oYXNFcnJvcnMoKVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcmVzZXRFcnJvcnMoKXtcclxuICAgICAgICB0aGlzLmVycm9yX2ZpcnN0bmFtZSAgPSAnJ1xyXG4gICAgICAgIHRoaXMuZXJyb3JfbGFzdG5hbWUgPSAnJ1xyXG4gICAgICAgIHRoaXMuZXJyb3JfcGFzc3dvcmQgPSAnJ1xyXG4gICAgICAgIHRoaXMuZXJyb3JfY29uZmlybSA9ICcnXHJcbiAgICAgICAgdGhpcy5lcnJvcl9lbWFpbCA9ICcnXHJcbiAgICAgICAgdGhpcy5lcnJvcl90ZWxlcGhvbmUgPSAnJ1xyXG4gICAgICAgIHRoaXMuZXJyb3JfYWRkcmVzc18xID0gJydcclxuICAgICAgICB0aGlzLmVycm9yX2NpdHkgPSAnJ1xyXG4gICAgICAgIHRoaXMuZXJyb3JfY291bnRyeV9pZCA9ICcnXHJcbiAgICAgICAgdGhpcy5lcnJvcl96b25lX2lkID0gJydcclxuICAgICAgICB0aGlzLmVycm9yX3JlZ2lzdGVyID0gJycgXHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBoYXNFcnJvcnMoKXtcclxuICAgICAgICByZXR1cm4gICF0aGlzLmVycm9yX2ZpcnN0bmFtZSAmJlxyXG4gICAgICAgICAgICAgICAgIXRoaXMuZXJyb3JfbGFzdG5hbWUgJiZcclxuICAgICAgICAgICAgICAgICF0aGlzLmVycm9yX2VtYWlsICYmXHJcbiAgICAgICAgICAgICAgICAhdGhpcy5lcnJvcl90ZWxlcGhvbmUgJiZcclxuICAgICAgICAgICAgICAgICF0aGlzLmVycm9yX2FkZHJlc3NfMSAmJlxyXG4gICAgICAgICAgICAgICAgIXRoaXMuZXJyb3JfY2l0eSAmJlxyXG4gICAgICAgICAgICAgICAgIXRoaXMuZXJyb3JfY291bnRyeV9pZCAmJlxyXG4gICAgICAgICAgICAgICAgIXRoaXMuZXJyb3Jfem9uZV9pZCAmJlxyXG4gICAgICAgICAgICAgICAgIXRoaXMuZXJyb3JfY29uZmlybVxyXG4gICAgfVxyXG59XHJcbiJdfQ==