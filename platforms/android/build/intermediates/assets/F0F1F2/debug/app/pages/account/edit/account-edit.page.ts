import { Component, ChangeDetectorRef } from "@angular/core";
import { OnInit } from '@angular/core';
import { Page } from "../../page";
import { Location } from "@angular/common";
import { AuthService } from '../../../services/auth.service'
import { RouterExtensions } from "nativescript-angular/router";
import { ValidationService } from '../../../services/validation.service'

@Component({
    moduleId: module.id,
    selector: 'account-edit',
    templateUrl: 'account-edit.page.html',
})

export class AccountEditPage extends Page implements OnInit {
    customer = {
        firstname: '',
        lastname: '',
        email: '',
        telephone: '',
        fax: '',
        newsletter: ''
    }

    error = {
        firstname: '',
        lastname: '',
        email: '',
        telephone: '',
        fax: ''
    }

    constructor(private location: Location, private authService: AuthService) {
        super(location);
    }

    ngOnInit() {
        this.authService.getCustomer(
            customer => {
                this.customer = customer
            },
            error => {
                console.log(error)
            }
        )
    }

    editCustomer() {
        if (this.validateCustomer()) {
            this.authService.editCustomer(this.customer,
                customer => { 
                    /* User has been succesfully updated */
                    /* todo: notification */
                },
                error => {
                    console.log(error)
                }
            )
        }
    }


    validateCustomer() {
        this.clearErrors()

        if (!ValidationService.isValidFirstname(this.customer.firstname)) {
            this.error.firstname = 'Invalid firstname'
        }

        if (!ValidationService.isValidLastname(this.customer.lastname)) {
            this.error.lastname = 'Invalid lastname'
        }

        if (!ValidationService.isValidEmail(this.customer.email)) {
            this.error.email = 'Invalid email'
        }

        if (!ValidationService.isValidTelephone(this.customer.telephone)) {
            this.error.telephone = 'Invalid telephone'
        }

        return !this.hasErrors()
    }

    hasErrors() {
        for (var key in this.error) {
            if (this.error[key]) {
                return true
            }
        }
        return false
    }

    clearErrors() {
        for (var key in this.error) {
            this.error[key] = ''
        }
    }
}