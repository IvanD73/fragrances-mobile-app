import { Component, ChangeDetectorRef } from "@angular/core";
import { OnInit } from '@angular/core';
import { Page } from "../../page";
import { AuthService } from '../../../services/auth.service'
import { RouterExtensions } from "nativescript-angular/router";
import { Location } from "@angular/common";

@Component({
    moduleId: module.id,
    selector: 'account-orders',
    templateUrl: 'account-orders.page.html',
})

export class AccountOrdersPage extends Page implements OnInit {
    constructor(private location: Location) {
        super(location);
    }

    ngOnInit() {
        
    }
}