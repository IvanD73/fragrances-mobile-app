"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ValidationService = (function () {
    function ValidationService() {
    }
    ValidationService.isValidEmail = function (string) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(string);
    };
    ValidationService.isValidPassword = function (string) {
        return string.length >= 6;
    };
    ValidationService.isValidConfirm = function (string1, string2) {
        return string1 == string2;
    };
    ValidationService.isValidFirstname = function (string) {
        return string.length >= 3;
    };
    ValidationService.isValidLastname = function (string) {
        return string.length >= 3;
    };
    ValidationService.isValidAddress = function (string) {
        return string.length >= 3;
    };
    ValidationService.isValidCity = function (string) {
        return string.length >= 3;
    };
    ValidationService.isValidCountryId = function (country_id) {
        return !!country_id;
    };
    ValidationService.isValidZoneId = function (zone_id) {
        return !!zone_id;
    };
    ValidationService.isValidTelephone = function (string) {
        return true;
        /*
        //https://stackoverflow.com/questions/4338267/validate-phone-number-with-javascript
        //Maybe change it, hasnt been tested much
        var phoneRe = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
        var digits = string.replace(/\D/g, "");
        return phoneRe.test(digits);
        */
    };
    ValidationService = __decorate([
        core_1.Injectable()
    ], ValidationService);
    return ValidationService;
}());
exports.ValidationService = ValidationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmFsaWRhdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBRzNDO0lBQUE7SUFrREEsQ0FBQztJQWpEVSw4QkFBWSxHQUFuQixVQUFvQixNQUFNO1FBQ3RCLElBQUksRUFBRSxHQUFHLHlKQUF5SixDQUFDO1FBQ25LLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFTSxpQ0FBZSxHQUF0QixVQUF1QixNQUFjO1FBQ2pDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQTtJQUM3QixDQUFDO0lBRU0sZ0NBQWMsR0FBckIsVUFBc0IsT0FBYyxFQUFFLE9BQWU7UUFDakQsTUFBTSxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUE7SUFDN0IsQ0FBQztJQUVNLGtDQUFnQixHQUF2QixVQUF3QixNQUFNO1FBQzFCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQTtJQUM3QixDQUFDO0lBRU0saUNBQWUsR0FBdEIsVUFBdUIsTUFBTTtRQUN6QixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUE7SUFDN0IsQ0FBQztJQUVNLGdDQUFjLEdBQXJCLFVBQXNCLE1BQU07UUFDeEIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFBO0lBQzdCLENBQUM7SUFFTSw2QkFBVyxHQUFsQixVQUFtQixNQUFNO1FBQ3JCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQTtJQUM3QixDQUFDO0lBRU0sa0NBQWdCLEdBQXZCLFVBQXdCLFVBQVU7UUFDOUIsTUFBTSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUE7SUFDdkIsQ0FBQztJQUVNLCtCQUFhLEdBQXBCLFVBQXFCLE9BQU87UUFDeEIsTUFBTSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUE7SUFDcEIsQ0FBQztJQUVNLGtDQUFnQixHQUF2QixVQUF3QixNQUFNO1FBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUE7UUFDWDs7Ozs7O1VBTUU7SUFDTixDQUFDO0lBL0NRLGlCQUFpQjtRQUQ3QixpQkFBVSxFQUFFO09BQ0EsaUJBQWlCLENBa0Q3QjtJQUFELHdCQUFDO0NBQUEsQUFsREQsSUFrREM7QUFsRFksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgVmFsaWRhdGlvblNlcnZpY2V7XHJcbiAgICBzdGF0aWMgaXNWYWxpZEVtYWlsKHN0cmluZyl7XHJcbiAgICAgICAgdmFyIHJlID0gL14oKFtePD4oKVxcW1xcXVxcXFwuLDs6XFxzQFwiXSsoXFwuW148PigpXFxbXFxdXFxcXC4sOzpcXHNAXCJdKykqKXwoXCIuK1wiKSlAKChcXFtbMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFxdKXwoKFthLXpBLVpcXC0wLTldK1xcLikrW2EtekEtWl17Mix9KSkkLztcclxuICAgICAgICByZXR1cm4gcmUudGVzdChzdHJpbmcpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpc1ZhbGlkUGFzc3dvcmQoc3RyaW5nOiBTdHJpbmcpe1xyXG4gICAgICAgIHJldHVybiBzdHJpbmcubGVuZ3RoID49IDZcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaXNWYWxpZENvbmZpcm0oc3RyaW5nMTpTdHJpbmcsIHN0cmluZzI6IFN0cmluZyl7XHJcbiAgICAgICAgcmV0dXJuIHN0cmluZzEgPT0gc3RyaW5nMlxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpc1ZhbGlkRmlyc3RuYW1lKHN0cmluZyl7XHJcbiAgICAgICAgcmV0dXJuIHN0cmluZy5sZW5ndGggPj0gM1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpc1ZhbGlkTGFzdG5hbWUoc3RyaW5nKXtcclxuICAgICAgICByZXR1cm4gc3RyaW5nLmxlbmd0aCA+PSAzXHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGlzVmFsaWRBZGRyZXNzKHN0cmluZyl7XHJcbiAgICAgICAgcmV0dXJuIHN0cmluZy5sZW5ndGggPj0gM1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpc1ZhbGlkQ2l0eShzdHJpbmcpe1xyXG4gICAgICAgIHJldHVybiBzdHJpbmcubGVuZ3RoID49IDNcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaXNWYWxpZENvdW50cnlJZChjb3VudHJ5X2lkKXtcclxuICAgICAgICByZXR1cm4gISFjb3VudHJ5X2lkXHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGlzVmFsaWRab25lSWQoem9uZV9pZCl7XHJcbiAgICAgICAgcmV0dXJuICEhem9uZV9pZFxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpc1ZhbGlkVGVsZXBob25lKHN0cmluZyl7XHJcbiAgICAgICAgcmV0dXJuIHRydWVcclxuICAgICAgICAvKlxyXG4gICAgICAgIC8vaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvNDMzODI2Ny92YWxpZGF0ZS1waG9uZS1udW1iZXItd2l0aC1qYXZhc2NyaXB0XHJcbiAgICAgICAgLy9NYXliZSBjaGFuZ2UgaXQsIGhhc250IGJlZW4gdGVzdGVkIG11Y2hcclxuICAgICAgICB2YXIgcGhvbmVSZSA9IC9eWzItOV1cXGR7Mn1bMi05XVxcZHsyfVxcZHs0fSQvO1xyXG4gICAgICAgIHZhciBkaWdpdHMgPSBzdHJpbmcucmVwbGFjZSgvXFxEL2csIFwiXCIpO1xyXG4gICAgICAgIHJldHVybiBwaG9uZVJlLnRlc3QoZGlnaXRzKTtcclxuICAgICAgICAqL1xyXG4gICAgfVxyXG5cclxuICAgIFxyXG59Il19