"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("../../page");
var common_1 = require("@angular/common");
var account_addresses_service_1 = require("../../../services/account-addresses.service");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var address_modal_component_1 = require("../../../components/modal/address/address-modal.component");
var AccountAddressesPage = (function (_super) {
    __extends(AccountAddressesPage, _super);
    function AccountAddressesPage(location, accountAddressesService, modal, vcRef) {
        var _this = _super.call(this, location) || this;
        _this.location = location;
        _this.accountAddressesService = accountAddressesService;
        _this.modal = modal;
        _this.vcRef = vcRef;
        _this.addresses = [];
        return _this;
    }
    AccountAddressesPage.prototype.ngOnInit = function () {
        var _this = this;
        this.accountAddressesService.getAddresses(function (addresses) {
            _this.addresses = addresses;
        }, function (error) {
            console.log(error);
        });
    };
    AccountAddressesPage.prototype.editExistingAddress = function (address) {
        var options = {
            context: {},
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(address_modal_component_1.AddressModalComponent, options).then(function (res) {
            //close callback
        });
    };
    AccountAddressesPage = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'account-addresses',
            templateUrl: 'account-addresses.page.html',
        }),
        __metadata("design:paramtypes", [common_1.Location,
            account_addresses_service_1.AccountAddressesService,
            dialogs_1.ModalDialogService,
            core_1.ViewContainerRef])
    ], AccountAddressesPage);
    return AccountAddressesPage;
}(page_1.Page));
exports.AccountAddressesPage = AccountAddressesPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1hZGRyZXNzZXMucGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjY291bnQtYWRkcmVzc2VzLnBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBK0U7QUFFL0UsbUNBQWtDO0FBRWxDLDBDQUEyQztBQUUzQyx5RkFBc0Y7QUFDdEYsbUVBQTZFO0FBQzdFLHFHQUFpRztBQVFqRztJQUEwQyx3Q0FBSTtJQUcxQyw4QkFBcUIsUUFBa0IsRUFDbEIsdUJBQWdELEVBQ2hELEtBQXlCLEVBQ3pCLEtBQXVCO1FBSDVDLFlBS2dCLGtCQUFNLFFBQVEsQ0FBQyxTQUM5QjtRQU5vQixjQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLDZCQUF1QixHQUF2Qix1QkFBdUIsQ0FBeUI7UUFDaEQsV0FBSyxHQUFMLEtBQUssQ0FBb0I7UUFDekIsV0FBSyxHQUFMLEtBQUssQ0FBa0I7UUFMNUMsZUFBUyxHQUFHLEVBQUUsQ0FBQzs7SUFRZixDQUFDO0lBRUQsdUNBQVEsR0FBUjtRQUFBLGlCQVNDO1FBUkcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FDckMsVUFBQSxTQUFTO1lBQ04sS0FBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7UUFDN0IsQ0FBQyxFQUNELFVBQUEsS0FBSztZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDdEIsQ0FBQyxDQUNKLENBQUE7SUFDTCxDQUFDO0lBRUQsa0RBQW1CLEdBQW5CLFVBQW9CLE9BQU87UUFDdkIsSUFBSSxPQUFPLEdBQUc7WUFDVixPQUFPLEVBQUUsRUFBRTtZQUNYLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxLQUFLO1NBQy9CLENBQUM7UUFDRixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQywrQ0FBcUIsRUFBRSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO1lBQzFELGdCQUFnQjtRQUNuQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUEvQlEsb0JBQW9CO1FBTmhDLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixXQUFXLEVBQUUsNkJBQTZCO1NBQzdDLENBQUM7eUNBS2lDLGlCQUFRO1lBQ08sbURBQXVCO1lBQ3pDLDRCQUFrQjtZQUNsQix1QkFBZ0I7T0FObkMsb0JBQW9CLENBZ0NoQztJQUFELDJCQUFDO0NBQUEsQUFoQ0QsQ0FBMEMsV0FBSSxHQWdDN0M7QUFoQ1ksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LENoYW5nZURldGVjdG9yUmVmLCBWaWV3Q29udGFpbmVyUmVmICB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcIi4uLy4uL3BhZ2VcIjtcclxuaW1wb3J0IHsgQXV0aFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9hdXRoLnNlcnZpY2UnXHJcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBBY2NvdW50QWRkcmVzc2VzU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9zZXJ2aWNlcy9hY2NvdW50LWFkZHJlc3Nlcy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE1vZGFsRGlhbG9nU2VydmljZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9kaXJlY3RpdmVzL2RpYWxvZ3NcIjtcclxuaW1wb3J0IHsgQWRkcmVzc01vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9tb2RhbC9hZGRyZXNzL2FkZHJlc3MtbW9kYWwuY29tcG9uZW50J1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6ICdhY2NvdW50LWFkZHJlc3NlcycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2FjY291bnQtYWRkcmVzc2VzLnBhZ2UuaHRtbCcsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQWNjb3VudEFkZHJlc3Nlc1BhZ2UgZXh0ZW5kcyBQYWdlIGltcGxlbWVudHMgT25Jbml0e1xyXG4gICAgYWRkcmVzc2VzID0gW107XHJcbiAgICBcclxuICAgIGNvbnN0cnVjdG9yKCBwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbixcclxuICAgICAgICAgICAgICAgICBwcml2YXRlIGFjY291bnRBZGRyZXNzZXNTZXJ2aWNlOiBBY2NvdW50QWRkcmVzc2VzU2VydmljZSxcclxuICAgICAgICAgICAgICAgICBwcml2YXRlIG1vZGFsOiBNb2RhbERpYWxvZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICAgcHJpdmF0ZSB2Y1JlZjogVmlld0NvbnRhaW5lclJlZlxyXG4gICAgICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3VwZXIobG9jYXRpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCl7XHJcbiAgICAgICAgdGhpcy5hY2NvdW50QWRkcmVzc2VzU2VydmljZS5nZXRBZGRyZXNzZXMoXHJcbiAgICAgICAgICAgIGFkZHJlc3NlcyA9PiB7XHJcbiAgICAgICAgICAgICAgIHRoaXMuYWRkcmVzc2VzID0gYWRkcmVzc2VzXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKVxyXG4gICAgfVxyXG5cclxuICAgIGVkaXRFeGlzdGluZ0FkZHJlc3MoYWRkcmVzcyl7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGNvbnRleHQ6IHt9LFxyXG4gICAgICAgICAgICBmdWxsc2NyZWVuOiB0cnVlLFxyXG4gICAgICAgICAgICB2aWV3Q29udGFpbmVyUmVmOiB0aGlzLnZjUmVmXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLm1vZGFsLnNob3dNb2RhbChBZGRyZXNzTW9kYWxDb21wb25lbnQsIG9wdGlvbnMpLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgICAvL2Nsb3NlIGNhbGxiYWNrXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn0iXX0=