"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CheckoutPage = (function () {
    function CheckoutPage() {
        this.cartProducts = [
            { name: 'Продукт', price: '32.89', quantity: '1' },
            { name: 'Продукт 2', price: '23.47', quantity: '2' },
        ];
    }
    __decorate([
        core_1.ViewChild("Terms"),
        __metadata("design:type", core_1.ElementRef)
    ], CheckoutPage.prototype, "Terms", void 0);
    __decorate([
        core_1.ViewChild("DeliveryMethod"),
        __metadata("design:type", core_1.ElementRef)
    ], CheckoutPage.prototype, "DeliveryMethod", void 0);
    CheckoutPage = __decorate([
        core_1.Component({
            selector: 'checkout-page',
            templateUrl: 'pages/checkout/checkout.page.html',
        }),
        __metadata("design:paramtypes", [])
    ], CheckoutPage);
    return CheckoutPage;
}());
exports.CheckoutPage = CheckoutPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tvdXQucGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNoZWNrb3V0LnBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBaUU7QUFRakU7SUFTQztRQUxBLGlCQUFZLEdBQUc7WUFDZCxFQUFDLElBQUksRUFBQyxTQUFTLEVBQUUsS0FBSyxFQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUMsR0FBRyxFQUFDO1lBQzdDLEVBQUMsSUFBSSxFQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUMsT0FBTyxFQUFFLFFBQVEsRUFBQyxHQUFHLEVBQUM7U0FDL0MsQ0FBQTtJQUVjLENBQUM7SUFSSTtRQUFuQixnQkFBUyxDQUFDLE9BQU8sQ0FBQztrQ0FBUSxpQkFBVTsrQ0FBQztJQUNUO1FBQTVCLGdCQUFTLENBQUMsZ0JBQWdCLENBQUM7a0NBQWlCLGlCQUFVO3dEQUFDO0lBRjVDLFlBQVk7UUFMeEIsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxlQUFlO1lBQ3pCLFdBQVcsRUFBRSxtQ0FBbUM7U0FDbkQsQ0FBQzs7T0FFVyxZQUFZLENBVXhCO0lBQUQsbUJBQUM7Q0FBQSxBQVZELElBVUM7QUFWWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBFbGVtZW50UmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY2hlY2tvdXQtcGFnZScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ3BhZ2VzL2NoZWNrb3V0L2NoZWNrb3V0LnBhZ2UuaHRtbCcsXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ2hlY2tvdXRQYWdlIHtcclxuXHRAVmlld0NoaWxkKFwiVGVybXNcIikgVGVybXM6IEVsZW1lbnRSZWY7XHJcblx0QFZpZXdDaGlsZChcIkRlbGl2ZXJ5TWV0aG9kXCIpIERlbGl2ZXJ5TWV0aG9kOiBFbGVtZW50UmVmO1xyXG5cclxuXHRjYXJ0UHJvZHVjdHMgPSBbXHJcblx0XHR7bmFtZTon0J/RgNC+0LTRg9C60YInLCBwcmljZTonMzIuODknLCBxdWFudGl0eTonMSd9LFxyXG5cdFx0e25hbWU6J9Cf0YDQvtC00YPQutGCIDInLCBwcmljZTonMjMuNDcnLCBxdWFudGl0eTonMid9LFxyXG5cdF1cclxuXHJcblx0Y29uc3RydWN0b3IoKSB7fVxyXG59XHJcbiJdfQ==