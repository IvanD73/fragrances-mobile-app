"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var base_service_1 = require("../services/base.service");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var applicationSettings = require("application-settings");
var AuthService = (function (_super) {
    __extends(AuthService, _super);
    function AuthService(http) {
        return _super.call(this, http) || this;
    }
    /* Used in another service for obtaining the user token*/
    AuthService.prototype.getToken = function () {
        return applicationSettings.getString('token', '');
    };
    AuthService.prototype.setToken = function (token) {
        applicationSettings.setString('token', token);
    };
    AuthService.prototype.isLogged = function () {
        return !!this.getToken();
    };
    AuthService.prototype.logout = function () {
        this.setToken('');
    };
    AuthService.prototype.getCustomer = function (success, error) {
        if (!this.isLogged()) {
            error('User is not logged');
        }
        this.executePostRequest('/account/customer', { token: this.getToken() })
            .subscribe(function (response) {
            if (!response.error) {
                success(response.customer);
            }
            else {
                error(response.message);
            }
        });
    };
    AuthService.prototype.editCustomer = function (customer, success, error) {
        if (!this.isLogged()) {
            error('User is not logged');
        }
        this.executePostRequest('/account/edit', { token: this.getToken(), customer: customer })
            .subscribe(function (response) {
            if (!response.error) {
                success(response.customer);
            }
            else {
                error(response.message);
            }
        }, function (error) {
        });
    };
    AuthService.prototype.loginWithEmailAndPassword = function (email, password, success, error) {
        var _this = this;
        var credentials = {
            email: email,
            password: password
        };
        this.executePostRequest('/account/login', credentials)
            .subscribe(function (response) {
            if (!response.error) {
                //Logs the customer
                _this.setToken(response.token);
                success();
            }
            else {
                error(error.message);
            }
        });
    };
    AuthService.prototype.register = function (customer, success, error) {
        var _this = this;
        this.executePostRequest('/account/register', customer)
            .subscribe(function (response) {
            if (!response.error) {
                //Logs the customer
                _this.setToken(response.token);
                success();
            }
            else {
                error(error.message);
            }
        });
    };
    AuthService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], AuthService);
    return AuthService;
}(base_service_1.BaseService));
exports.AuthService = AuthService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXV0aC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEseURBQXNEO0FBQ3RELHNDQUEyQztBQUMzQyxzQ0FBcUM7QUFDckMsMERBQTREO0FBRzVEO0lBQWlDLCtCQUFXO0lBQ3hDLHFCQUFZLElBQVU7ZUFDbEIsa0JBQU0sSUFBSSxDQUFDO0lBQ2YsQ0FBQztJQUVELHlEQUF5RDtJQUNsRCw4QkFBUSxHQUFmO1FBQ0ksTUFBTSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUE7SUFDckQsQ0FBQztJQUVPLDhCQUFRLEdBQWhCLFVBQWlCLEtBQUs7UUFDbEIsbUJBQW1CLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQTtJQUNqRCxDQUFDO0lBRUQsOEJBQVEsR0FBUjtRQUNJLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO0lBQzVCLENBQUM7SUFHRCw0QkFBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQTtJQUNyQixDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLE9BQU8sRUFBRSxLQUFLO1FBQ3RCLEVBQUUsQ0FBQSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUEsQ0FBQztZQUNqQixLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNoQyxDQUFDO1FBRUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixFQUFFLEVBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBQyxDQUFDO2FBQ2pFLFNBQVMsQ0FBRSxVQUFBLFFBQVE7WUFDaEIsRUFBRSxDQUFBLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUEsQ0FBQztnQkFDaEIsT0FBTyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtZQUM5QixDQUFDO1lBQUEsSUFBSSxDQUFBLENBQUM7Z0JBQ0YsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQTtZQUMzQixDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUE7SUFDVixDQUFDO0lBRUQsa0NBQVksR0FBWixVQUFhLFFBQVEsRUFBRSxPQUFPLEVBQUUsS0FBSztRQUNqQyxFQUFFLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFBLENBQUM7WUFDakIsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDaEMsQ0FBQztRQUVELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLEVBQUUsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUMsQ0FBQzthQUNqRixTQUFTLENBQUUsVUFBQSxRQUFRO1lBQ2hCLEVBQUUsQ0FBQSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFBLENBQUM7Z0JBQ2hCLE9BQU8sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUE7WUFDOUIsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNGLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDM0IsQ0FBQztRQUNMLENBQUMsRUFDRCxVQUFBLEtBQUs7UUFFTCxDQUFDLENBQUMsQ0FBQTtJQUNWLENBQUM7SUFFRCwrQ0FBeUIsR0FBekIsVUFBMEIsS0FBSyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsS0FBSztRQUF6RCxpQkFtQkM7UUFsQkcsSUFBSSxXQUFXLEdBQUc7WUFDZCxLQUFLLEVBQUUsS0FBSztZQUNaLFFBQVEsRUFBRSxRQUFRO1NBQ3JCLENBQUE7UUFFRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLEVBQUUsV0FBVyxDQUFDO2FBQ2pELFNBQVMsQ0FDTixVQUFBLFFBQVE7WUFDSixFQUFFLENBQUEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDO2dCQUNoQixtQkFBbUI7Z0JBQ25CLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFBO2dCQUM3QixPQUFPLEVBQUUsQ0FBQTtZQUNiLENBQUM7WUFBQSxJQUFJLENBQUEsQ0FBQztnQkFDRixLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1lBQ3hCLENBQUM7UUFFTCxDQUFDLENBQ0osQ0FBQTtJQUNULENBQUM7SUFFRCw4QkFBUSxHQUFSLFVBQVMsUUFBUSxFQUFFLE9BQU8sRUFBRSxLQUFLO1FBQWpDLGlCQWFDO1FBWkcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixFQUFFLFFBQVEsQ0FBQzthQUNyRCxTQUFTLENBQ04sVUFBQSxRQUFRO1lBQ0osRUFBRSxDQUFBLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUEsQ0FBQztnQkFDaEIsbUJBQW1CO2dCQUNuQixLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQTtnQkFDN0IsT0FBTyxFQUFFLENBQUE7WUFDYixDQUFDO1lBQUEsSUFBSSxDQUFBLENBQUM7Z0JBQ0YsS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQTtZQUN4QixDQUFDO1FBQ0wsQ0FBQyxDQUNKLENBQUE7SUFDTCxDQUFDO0lBMUZRLFdBQVc7UUFEdkIsaUJBQVUsRUFBRTt5Q0FFUyxXQUFJO09BRGIsV0FBVyxDQTJGdkI7SUFBRCxrQkFBQztDQUFBLEFBM0ZELENBQWlDLDBCQUFXLEdBMkYzQztBQTNGWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEJhc2VTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYmFzZS5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEh0dHAgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xyXG5pbXBvcnQgKiBhcyBhcHBsaWNhdGlvblNldHRpbmdzIGZyb20gXCJhcHBsaWNhdGlvbi1zZXR0aW5nc1wiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZpY2UgZXh0ZW5kcyBCYXNlU2VydmljZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihodHRwOiBIdHRwKSB7XHJcbiAgICAgICAgc3VwZXIoaHR0cClcclxuICAgIH1cclxuXHJcbiAgICAvKiBVc2VkIGluIGFub3RoZXIgc2VydmljZSBmb3Igb2J0YWluaW5nIHRoZSB1c2VyIHRva2VuKi9cclxuICAgIHB1YmxpYyBnZXRUb2tlbigpe1xyXG4gICAgICAgIHJldHVybiBhcHBsaWNhdGlvblNldHRpbmdzLmdldFN0cmluZygndG9rZW4nLCAnJylcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHNldFRva2VuKHRva2VuKXtcclxuICAgICAgICBhcHBsaWNhdGlvblNldHRpbmdzLnNldFN0cmluZygndG9rZW4nLCB0b2tlbilcclxuICAgIH1cclxuXHJcbiAgICBpc0xvZ2dlZCgpIHtcclxuICAgICAgICByZXR1cm4gISF0aGlzLmdldFRva2VuKClcclxuICAgIH1cclxuXHJcbiAgICBcclxuICAgIGxvZ291dCgpIHtcclxuICAgICAgICB0aGlzLnNldFRva2VuKCcnKVxyXG4gICAgfVxyXG5cclxuICAgIGdldEN1c3RvbWVyKHN1Y2Nlc3MsIGVycm9yKXtcclxuICAgICAgICBpZighdGhpcy5pc0xvZ2dlZCgpKXtcclxuICAgICAgICAgICAgZXJyb3IoJ1VzZXIgaXMgbm90IGxvZ2dlZCcpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5leGVjdXRlUG9zdFJlcXVlc3QoJy9hY2NvdW50L2N1c3RvbWVyJywge3Rva2VuOiB0aGlzLmdldFRva2VuKCl9KVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZighcmVzcG9uc2UuZXJyb3Ipe1xyXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UuY3VzdG9tZXIpXHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvcihyZXNwb25zZS5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIGVkaXRDdXN0b21lcihjdXN0b21lciwgc3VjY2VzcywgZXJyb3Ipe1xyXG4gICAgICAgIGlmKCF0aGlzLmlzTG9nZ2VkKCkpe1xyXG4gICAgICAgICAgICBlcnJvcignVXNlciBpcyBub3QgbG9nZ2VkJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmV4ZWN1dGVQb3N0UmVxdWVzdCgnL2FjY291bnQvZWRpdCcsIHt0b2tlbjogdGhpcy5nZXRUb2tlbigpLCBjdXN0b21lcjogY3VzdG9tZXJ9KVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZighcmVzcG9uc2UuZXJyb3Ipe1xyXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UuY3VzdG9tZXIpXHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvcihyZXNwb25zZS5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBsb2dpbldpdGhFbWFpbEFuZFBhc3N3b3JkKGVtYWlsLCBwYXNzd29yZCwgc3VjY2VzcywgZXJyb3IpIHtcclxuICAgICAgICBsZXQgY3JlZGVudGlhbHMgPSB7XHJcbiAgICAgICAgICAgIGVtYWlsOiBlbWFpbCxcclxuICAgICAgICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkXHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuZXhlY3V0ZVBvc3RSZXF1ZXN0KCcvYWNjb3VudC9sb2dpbicsIGNyZWRlbnRpYWxzKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKCFyZXNwb25zZS5lcnJvcil7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vTG9ncyB0aGUgY3VzdG9tZXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRUb2tlbihyZXNwb25zZS50b2tlbilcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzcygpXHJcbiAgICAgICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yKGVycm9yLm1lc3NhZ2UpXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApXHJcbiAgICB9XHJcblxyXG4gICAgcmVnaXN0ZXIoY3VzdG9tZXIsIHN1Y2Nlc3MsIGVycm9yKXtcclxuICAgICAgICB0aGlzLmV4ZWN1dGVQb3N0UmVxdWVzdCgnL2FjY291bnQvcmVnaXN0ZXInLCBjdXN0b21lcilcclxuICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZighcmVzcG9uc2UuZXJyb3Ipe1xyXG4gICAgICAgICAgICAgICAgICAgIC8vTG9ncyB0aGUgY3VzdG9tZXJcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFRva2VuKHJlc3BvbnNlLnRva2VuKVxyXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MoKVxyXG4gICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IoZXJyb3IubWVzc2FnZSlcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIClcclxuICAgIH1cclxufSJdfQ==