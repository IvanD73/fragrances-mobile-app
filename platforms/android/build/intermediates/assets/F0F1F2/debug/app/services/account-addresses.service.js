"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var base_service_1 = require("../services/base.service");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var auth_service_1 = require("../services/auth.service");
var AccountAddressesService = (function (_super) {
    __extends(AccountAddressesService, _super);
    function AccountAddressesService(http, authService) {
        var _this = _super.call(this, http) || this;
        _this.authService = authService;
        _this.token = '';
        //Set token
        _this.token = _this.authService.getToken();
        return _this;
    }
    AccountAddressesService.prototype.getAddresses = function (success, error) {
        this.executePostRequest('/account/addresses', { token: this.token })
            .subscribe(function (response) {
            if (response.error) {
                error(response.message);
            }
            else {
                success(response.addresses);
            }
        }, function (err) {
            error(err);
        });
    };
    AccountAddressesService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http, auth_service_1.AuthService])
    ], AccountAddressesService);
    return AccountAddressesService;
}(base_service_1.BaseService));
exports.AccountAddressesService = AccountAddressesService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC1hZGRyZXNzZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFjY291bnQtYWRkcmVzc2VzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSx5REFBc0Q7QUFDdEQsc0NBQTJDO0FBQzNDLHNDQUFxQztBQUVyQyx5REFBdUQ7QUFHdkQ7SUFBNkMsMkNBQVc7SUFFcEQsaUNBQVksSUFBVSxFQUFVLFdBQXdCO1FBQXhELFlBQ0ksa0JBQU0sSUFBSSxDQUFDLFNBSWQ7UUFMK0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7UUFEaEQsV0FBSyxHQUFHLEVBQUUsQ0FBQztRQUlmLFdBQVc7UUFDWCxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUE7O0lBQzVDLENBQUM7SUFFRCw4Q0FBWSxHQUFaLFVBQWEsT0FBTyxFQUFFLEtBQUs7UUFDdkIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG9CQUFvQixFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUMvRCxTQUFTLENBQ04sVUFBQSxRQUFRO1lBQ0osRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ2pCLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDM0IsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLE9BQU8sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUE7WUFDL0IsQ0FBQztRQUNMLENBQUMsRUFDRCxVQUFBLEdBQUc7WUFDQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUE7UUFDZCxDQUFDLENBQ0osQ0FBQTtJQUNULENBQUM7SUF2QlEsdUJBQXVCO1FBRG5DLGlCQUFVLEVBQUU7eUNBR1MsV0FBSSxFQUF1QiwwQkFBVztPQUYvQyx1QkFBdUIsQ0F3Qm5DO0lBQUQsOEJBQUM7Q0FBQSxBQXhCRCxDQUE2QywwQkFBVyxHQXdCdkQ7QUF4QlksMERBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQmFzZVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9iYXNlLnNlcnZpY2UnXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cCB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XHJcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uU2V0dGluZ3MgZnJvbSBcImFwcGxpY2F0aW9uLXNldHRpbmdzXCI7XHJcbmltcG9ydCB7IEF1dGhTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYXV0aC5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFjY291bnRBZGRyZXNzZXNTZXJ2aWNlIGV4dGVuZHMgQmFzZVNlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSB0b2tlbiA9ICcnO1xyXG4gICAgY29uc3RydWN0b3IoaHR0cDogSHR0cCwgcHJpdmF0ZSBhdXRoU2VydmljZTogQXV0aFNlcnZpY2UpIHtcclxuICAgICAgICBzdXBlcihodHRwKVxyXG5cclxuICAgICAgICAvL1NldCB0b2tlblxyXG4gICAgICAgIHRoaXMudG9rZW4gPSB0aGlzLmF1dGhTZXJ2aWNlLmdldFRva2VuKClcclxuICAgIH1cclxuXHJcbiAgICBnZXRBZGRyZXNzZXMoc3VjY2VzcywgZXJyb3IpIHtcclxuICAgICAgICB0aGlzLmV4ZWN1dGVQb3N0UmVxdWVzdCgnL2FjY291bnQvYWRkcmVzc2VzJywgeyB0b2tlbjogdGhpcy50b2tlbiB9KVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5lcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcihyZXNwb25zZS5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UuYWRkcmVzc2VzKVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yKGVycilcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKVxyXG4gICAgfVxyXG59Il19