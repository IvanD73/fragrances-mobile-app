"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var drawer_page_1 = require("../drawer.page");
var categories_service_1 = require("../../services/categories.service");
var CategoriesPage = (function (_super) {
    __extends(CategoriesPage, _super);
    function CategoriesPage(changeDetectorRef, categoriesServices) {
        var _this = _super.call(this, changeDetectorRef) || this;
        _this.changeDetectorRef = changeDetectorRef;
        _this.categoriesServices = categoriesServices;
        return _this;
    }
    CategoriesPage.prototype.ngOnInit = function () { };
    CategoriesPage = __decorate([
        core_1.Component({
            selector: 'categories-page',
            templateUrl: 'pages/categories/categories.page.html',
            providers: [categories_service_1.CategoriesService]
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef,
            categories_service_1.CategoriesService])
    ], CategoriesPage);
    return CategoriesPage;
}(drawer_page_1.DrawerPage));
exports.CategoriesPage = CategoriesPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0ZWdvcmllcy5wYWdlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY2F0ZWdvcmllcy5wYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXFFO0FBQ3JFLDhDQUE0QztBQUM1Qyx3RUFBc0U7QUFVdEU7SUFBb0Msa0NBQVU7SUFDMUMsd0JBQ1MsaUJBQW9DLEVBQ3BDLGtCQUFxQztRQUY5QyxZQUdJLGtCQUFNLGlCQUFpQixDQUFDLFNBQzNCO1FBSFEsdUJBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyx3QkFBa0IsR0FBbEIsa0JBQWtCLENBQW1COztJQUU5QyxDQUFDO0lBRUQsaUNBQVEsR0FBUixjQUFXLENBQUM7SUFQSCxjQUFjO1FBTjFCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsaUJBQWlCO1lBQzNCLFdBQVcsRUFBRSx1Q0FBdUM7WUFDcEQsU0FBUyxFQUFFLENBQUMsc0NBQWlCLENBQUM7U0FDakMsQ0FBQzt5Q0FJOEIsd0JBQWlCO1lBQ2hCLHNDQUFpQjtPQUhyQyxjQUFjLENBVzFCO0lBQUQscUJBQUM7Q0FBQSxBQVhELENBQW9DLHdCQUFVLEdBVzdDO0FBWFksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBEcmF3ZXJQYWdlIH0gZnJvbSBcIi4uL2RyYXdlci5wYWdlXCI7XHJcbmltcG9ydCB7IENhdGVnb3JpZXNTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY2F0ZWdvcmllcy5zZXJ2aWNlJztcclxuXHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NhdGVnb3JpZXMtcGFnZScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ3BhZ2VzL2NhdGVnb3JpZXMvY2F0ZWdvcmllcy5wYWdlLmh0bWwnLFxyXG4gICAgcHJvdmlkZXJzOiBbQ2F0ZWdvcmllc1NlcnZpY2VdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ2F0ZWdvcmllc1BhZ2UgZXh0ZW5kcyBEcmF3ZXJQYWdlIGltcGxlbWVudHMgT25Jbml0ICB7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgIFx0cHJpdmF0ZSBjaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICBcdHByaXZhdGUgY2F0ZWdvcmllc1NlcnZpY2VzOiBDYXRlZ29yaWVzU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKGNoYW5nZURldGVjdG9yUmVmKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpe31cclxuXHJcblxyXG5cclxufVxyXG4iXX0=