"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var ProductImageModalComponent = (function () {
    function ProductImageModalComponent(params, modal, vcRef) {
        this.params = params;
        this.modal = modal;
        this.vcRef = vcRef;
    }
    ProductImageModalComponent.prototype.close = function (res) {
        this.params.closeCallback(res);
    };
    ProductImageModalComponent = __decorate([
        core_1.Component({
            selector: "productImage-modal",
            templateUrl: "components/modal/productImage/productImage-modal.component.html",
        }),
        __metadata("design:paramtypes", [dialogs_1.ModalDialogParams,
            dialogs_1.ModalDialogService,
            core_1.ViewContainerRef])
    ], ProductImageModalComponent);
    return ProductImageModalComponent;
}());
exports.ProductImageModalComponent = ProductImageModalComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdEltYWdlLW1vZGFsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInByb2R1Y3RJbWFnZS1tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFDM0QsbUVBQWdHO0FBTWhHO0lBRUksb0NBQ1MsTUFBeUIsRUFDekIsS0FBd0IsRUFDeEIsS0FBc0I7UUFGdEIsV0FBTSxHQUFOLE1BQU0sQ0FBbUI7UUFDekIsVUFBSyxHQUFMLEtBQUssQ0FBbUI7UUFDeEIsVUFBSyxHQUFMLEtBQUssQ0FBaUI7SUFBRyxDQUFDO0lBRTVCLDBDQUFLLEdBQVosVUFBYSxHQUFXO1FBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFUUSwwQkFBMEI7UUFKdEMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsV0FBVyxFQUFFLGlFQUFpRTtTQUNqRixDQUFDO3lDQUltQiwyQkFBaUI7WUFDbkIsNEJBQWtCO1lBQ2xCLHVCQUFnQjtPQUx0QiwwQkFBMEIsQ0FXdEM7SUFBRCxpQ0FBQztDQUFBLEFBWEQsSUFXQztBQVhZLGdFQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCxWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgTW9kYWxEaWFsb2dQYXJhbXMsIE1vZGFsRGlhbG9nU2VydmljZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9kaXJlY3RpdmVzL2RpYWxvZ3NcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwicHJvZHVjdEltYWdlLW1vZGFsXCIsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCJjb21wb25lbnRzL21vZGFsL3Byb2R1Y3RJbWFnZS9wcm9kdWN0SW1hZ2UtbW9kYWwuY29tcG9uZW50Lmh0bWxcIixcclxufSlcclxuZXhwb3J0IGNsYXNzIFByb2R1Y3RJbWFnZU1vZGFsQ29tcG9uZW50IHtcclxuXHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IoXHJcbiAgICBcdHByaXZhdGUgcGFyYW1zOiBNb2RhbERpYWxvZ1BhcmFtcyxcclxuICAgIFx0cHJpdmF0ZSBtb2RhbDpNb2RhbERpYWxvZ1NlcnZpY2UsXHJcbiAgICBcdHByaXZhdGUgdmNSZWY6Vmlld0NvbnRhaW5lclJlZikge31cclxuXHJcbiAgICBwdWJsaWMgY2xvc2UocmVzOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLnBhcmFtcy5jbG9zZUNhbGxiYWNrKHJlcyk7XHJcbiAgICB9XHJcblxyXG59Il19