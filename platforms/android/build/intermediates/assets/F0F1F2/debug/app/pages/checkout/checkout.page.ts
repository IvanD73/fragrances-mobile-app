import { Component, ViewChild, ElementRef } from "@angular/core";


@Component({
    selector: 'checkout-page',
    templateUrl: 'pages/checkout/checkout.page.html',
})

export class CheckoutPage {
	@ViewChild("Terms") Terms: ElementRef;
	@ViewChild("DeliveryMethod") DeliveryMethod: ElementRef;

	cartProducts = [
		{name:'Продукт', price:'32.89', quantity:'1'},
		{name:'Продукт 2', price:'23.47', quantity:'2'},
	]

	constructor() {}
}
