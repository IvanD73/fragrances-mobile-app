"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var drawer_page_1 = require("../../drawer.page");
var router_1 = require("@angular/router");
var router_2 = require("nativescript-angular/router");
var categories_service_1 = require("../../../services/categories.service");
var SubcategoryList = (function (_super) {
    __extends(SubcategoryList, _super);
    function SubcategoryList(changeDetectorRef, routerExtensions, route, categoriesServices) {
        var _this = _super.call(this, changeDetectorRef) || this;
        _this.changeDetectorRef = changeDetectorRef;
        _this.routerExtensions = routerExtensions;
        _this.route = route;
        _this.categoriesServices = categoriesServices;
        return _this;
    }
    SubcategoryList.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (id) { return _this.parentId = id.id; });
        this.selectedCategoryChildrens = this.categoriesServices.getSubcategoriesByParentId(this.parentId);
        console.dir(this.selectedCategoryChildrens);
    };
    SubcategoryList.prototype.onCategoryTap = function (category) {
        this.routerExtensions.navigate(["/category", category.id]);
    };
    SubcategoryList = __decorate([
        core_1.Component({
            selector: 'subcategoriesList',
            templateUrl: './pages/categories/subcategory-list/subcategoryList.component.html',
            providers: [categories_service_1.CategoriesService]
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef,
            router_2.RouterExtensions,
            router_1.ActivatedRoute,
            categories_service_1.CategoriesService])
    ], SubcategoryList);
    return SubcategoryList;
}(drawer_page_1.DrawerPage));
exports.SubcategoryList = SubcategoryList;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3ViY2F0ZWdvcnlMaXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInN1YmNhdGVnb3J5TGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBcUU7QUFDckUsaURBQStDO0FBQy9DLDBDQUFpRDtBQUNqRCxzREFBK0Q7QUFDL0QsMkVBQXlFO0FBUXpFO0lBQXFDLG1DQUFVO0lBSTNDLHlCQUNTLGlCQUFvQyxFQUNqQyxnQkFBa0MsRUFDckMsS0FBcUIsRUFDbEIsa0JBQXFDO1FBSmpELFlBS0ksa0JBQU0saUJBQWlCLENBQUMsU0FDM0I7UUFMUSx1QkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ2pDLHNCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDckMsV0FBSyxHQUFMLEtBQUssQ0FBZ0I7UUFDbEIsd0JBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjs7SUFFakQsQ0FBQztJQUVELGtDQUFRLEdBQVI7UUFBQSxpQkFPQztRQU5HLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FDdkIsVUFBQyxFQUFFLElBQUssT0FBQSxLQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQXJCLENBQXFCLENBQ2hDLENBQUE7UUFFSixJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNoRyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCx1Q0FBYSxHQUFiLFVBQWMsUUFBUTtRQUNsQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxFQUFFLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUF2QlEsZUFBZTtRQU4zQixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixXQUFXLEVBQUMsb0VBQW9FO1lBQ2hGLFNBQVMsRUFBQyxDQUFDLHNDQUFpQixDQUFDO1NBQ2hDLENBQUM7eUNBTzhCLHdCQUFpQjtZQUNmLHlCQUFnQjtZQUM5Qix1QkFBYztZQUNFLHNDQUFpQjtPQVJ4QyxlQUFlLENBeUIzQjtJQUFELHNCQUFDO0NBQUEsQUF6QkQsQ0FBcUMsd0JBQVUsR0F5QjlDO0FBekJZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgRHJhd2VyUGFnZSB9IGZyb20gXCIuLi8uLi9kcmF3ZXIucGFnZVwiO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgQ2F0ZWdvcmllc1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9jYXRlZ29yaWVzLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ3N1YmNhdGVnb3JpZXNMaXN0JyxcclxuICAgIHRlbXBsYXRlVXJsOicuL3BhZ2VzL2NhdGVnb3JpZXMvc3ViY2F0ZWdvcnktbGlzdC9zdWJjYXRlZ29yeUxpc3QuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgcHJvdmlkZXJzOltDYXRlZ29yaWVzU2VydmljZV1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTdWJjYXRlZ29yeUxpc3QgZXh0ZW5kcyBEcmF3ZXJQYWdlIGltcGxlbWVudHMgT25Jbml0ICB7XHJcblx0c2VsZWN0ZWRDYXRlZ29yeUNoaWxkcmVuczpBcnJheTxPYmplY3Q+O1xyXG4gICAgcGFyZW50SWQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICBcdHByaXZhdGUgY2hhbmdlRGV0ZWN0b3JSZWY6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgICAgIHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgIFx0cHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICAgICAgcHJpdmF0ZSBjYXRlZ29yaWVzU2VydmljZXM6IENhdGVnb3JpZXNTZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoY2hhbmdlRGV0ZWN0b3JSZWYpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCl7XHJcbiAgICAgICAgdGhpcy5yb3V0ZS5wYXJhbXMuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAoaWQpID0+IHRoaXMucGFyZW50SWQgPSBpZC5pZFxyXG4gICAgICAgIClcclxuXHJcbiAgICBcdHRoaXMuc2VsZWN0ZWRDYXRlZ29yeUNoaWxkcmVucyA9IHRoaXMuY2F0ZWdvcmllc1NlcnZpY2VzLmdldFN1YmNhdGVnb3JpZXNCeVBhcmVudElkKHRoaXMucGFyZW50SWQpO1xyXG4gICAgICAgIGNvbnNvbGUuZGlyKHRoaXMuc2VsZWN0ZWRDYXRlZ29yeUNoaWxkcmVucyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25DYXRlZ29yeVRhcChjYXRlZ29yeSl7XHJcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9jYXRlZ29yeVwiLCBjYXRlZ29yeS5pZF0pO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=