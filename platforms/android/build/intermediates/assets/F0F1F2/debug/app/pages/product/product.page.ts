import { Component, ChangeDetectorRef, ViewContainerRef } from "@angular/core";
import { DrawerPage } from "../drawer.page";
import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { ProductImageModalComponent } from '../../components/modal/productImage/productImage-modal.component';

@Component({
    selector: 'product-page',
    templateUrl: 'pages/product/product.page.html',
})

export class ProductPage extends DrawerPage {

    constructor(
    	private changeDetectorRef: ChangeDetectorRef,
    	private modal:ModalDialogService,
    	private vcRef:ViewContainerRef
    ){
        super(changeDetectorRef);
    }

    showProductImageModal() {
    	
        let options = {
            context: {},
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(ProductImageModalComponent, options).then(res => {
            console.log(res);
        });
    }
}
