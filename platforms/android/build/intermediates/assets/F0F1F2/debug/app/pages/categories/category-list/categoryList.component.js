"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var categories_service_1 = require("../../../services/categories.service");
var CategoryList = (function () {
    function CategoryList(categoriesServices, routerExtensions) {
        this.categoriesServices = categoriesServices;
        this.routerExtensions = routerExtensions;
    }
    CategoryList.prototype.ngOnInit = function () {
        this.categories = this.categoriesServices.getCategories();
    };
    CategoryList.prototype.onCategoryTap = function (category) {
        if (category.subcategories) {
            this.routerExtensions.navigate(["/category/subcategories", category.id]);
        }
        else {
            this.routerExtensions.navigate(["/category", category.id]);
        }
    };
    __decorate([
        core_1.ViewChild("CategoryList"),
        __metadata("design:type", core_1.ElementRef)
    ], CategoryList.prototype, "categoryList", void 0);
    CategoryList = __decorate([
        core_1.Component({
            selector: 'categoriesList',
            templateUrl: './pages/categories/category-list/categoryList.component.html',
        }),
        __metadata("design:paramtypes", [categories_service_1.CategoriesService,
            router_1.RouterExtensions])
    ], CategoryList);
    return CategoryList;
}());
exports.CategoryList = CategoryList;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2F0ZWdvcnlMaXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImNhdGVnb3J5TGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUU7QUFHekUsc0RBQStEO0FBQy9ELDJFQUF5RTtBQU96RTtJQUlJLHNCQUNTLGtCQUFxQyxFQUNyQyxnQkFBa0M7UUFEbEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQUNyQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO0lBQzNDLENBQUM7SUFFRCwrQkFBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDM0QsQ0FBQztJQUVELG9DQUFhLEdBQWIsVUFBYyxRQUFRO1FBQ3JCLEVBQUUsQ0FBQSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyx5QkFBeUIsRUFBRSxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNoRixDQUFDO1FBQUEsSUFBSSxDQUFBLENBQUM7WUFDTCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxFQUFHLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdELENBQUM7SUFDTCxDQUFDO0lBbEIwQjtRQUExQixnQkFBUyxDQUFDLGNBQWMsQ0FBQztrQ0FBZSxpQkFBVTtzREFBQztJQUR4QyxZQUFZO1FBTHhCLGdCQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsZ0JBQWdCO1lBQzFCLFdBQVcsRUFBQyw4REFBOEQ7U0FDN0UsQ0FBQzt5Q0FPK0Isc0NBQWlCO1lBQ25CLHlCQUFnQjtPQU5sQyxZQUFZLENBb0J4QjtJQUFELG1CQUFDO0NBQUEsQUFwQkQsSUFvQkM7QUFwQlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgRWxlbWVudFJlZiwgVmlld0NoaWxkIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQ29sb3IgfSBmcm9tIFwiY29sb3JcIjtcclxuaW1wb3J0IHsgVmlldyB9IGZyb20gXCJ1aS9jb3JlL3ZpZXdcIjtcclxuaW1wb3J0IHsgUm91dGVyRXh0ZW5zaW9ucyB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgQ2F0ZWdvcmllc1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9jYXRlZ29yaWVzLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2NhdGVnb3JpZXNMaXN0JyxcclxuICAgIHRlbXBsYXRlVXJsOicuL3BhZ2VzL2NhdGVnb3JpZXMvY2F0ZWdvcnktbGlzdC9jYXRlZ29yeUxpc3QuY29tcG9uZW50Lmh0bWwnLFxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENhdGVnb3J5TGlzdCBpbXBsZW1lbnRzIE9uSW5pdCAge1xyXG5cdEBWaWV3Q2hpbGQoXCJDYXRlZ29yeUxpc3RcIikgY2F0ZWdvcnlMaXN0OiBFbGVtZW50UmVmO1xyXG4gICAgY2F0ZWdvcmllczpBcnJheTxPYmplY3Q+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgXHRwcml2YXRlIGNhdGVnb3JpZXNTZXJ2aWNlczogQ2F0ZWdvcmllc1NlcnZpY2UsXHJcbiAgICBcdHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucykge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCl7XHJcbiAgICBcdHRoaXMuY2F0ZWdvcmllcyA9IHRoaXMuY2F0ZWdvcmllc1NlcnZpY2VzLmdldENhdGVnb3JpZXMoKTsgICAgXHJcbiAgICB9XHJcblxyXG4gICAgb25DYXRlZ29yeVRhcChjYXRlZ29yeSl7XHJcbiAgICBcdGlmKGNhdGVnb3J5LnN1YmNhdGVnb3JpZXMpe1xyXG4gICAgICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2NhdGVnb3J5L3N1YmNhdGVnb3JpZXNcIiwgY2F0ZWdvcnkuaWRdKTtcclxuICAgIFx0fWVsc2V7XHJcbiAgICBcdFx0dGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9jYXRlZ29yeVwiLCAgY2F0ZWdvcnkuaWRdKTtcclxuICAgIFx0fVxyXG5cdH1cclxufVxyXG4iXX0=