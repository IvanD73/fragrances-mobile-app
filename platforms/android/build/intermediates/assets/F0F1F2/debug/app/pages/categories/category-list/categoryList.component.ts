import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { Color } from "color";
import { View } from "ui/core/view";
import { RouterExtensions } from "nativescript-angular/router";
import { CategoriesService } from '../../../services/categories.service';

@Component({
    selector: 'categoriesList',
    templateUrl:'./pages/categories/category-list/categoryList.component.html',
})

export class CategoryList implements OnInit  {
	@ViewChild("CategoryList") categoryList: ElementRef;
    categories:Array<Object>;

    constructor(
    	private categoriesServices: CategoriesService,
    	private routerExtensions: RouterExtensions) {
    }

    ngOnInit(){
    	this.categories = this.categoriesServices.getCategories();    
    }

    onCategoryTap(category){
    	if(category.subcategories){
            this.routerExtensions.navigate(["/category/subcategories", category.id]);
    	}else{
    		this.routerExtensions.navigate(["/category",  category.id]);
    	}
	}
}
