import { BaseService } from '../services/base.service'
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import * as applicationSettings from "application-settings";
import { AuthService } from '../services/auth.service';

@Injectable()
export class AccountAddressesService extends BaseService {
    private token = '';
    constructor(http: Http, private authService: AuthService) {
        super(http)

        //Set token
        this.token = this.authService.getToken()
    }

    getAddresses(success, error) {
        this.executePostRequest('/account/addresses', { token: this.token })
            .subscribe(
                response => {
                    if (response.error) {
                        error(response.message)
                    } else {
                        success(response.addresses)
                    }
                },
                err => {
                    error(err)
                }
            )
    }
}